//
//  CustomActionSheet.swift
//  manup
//
//  Created by Tom Swindell on 10/12/2015.
//  Copyright © 2015 The App Developers. All rights reserved.
//

import UIKit

class CustomActionSheet: UIView {
    
    // MARK: - Enums
    enum ButtonPressed {
        case camera, chooseFromLibrary, cancel
    }
    
   
    // MARK: - Outlets
    @IBOutlet weak var sheetBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var actionSheet: UIView!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnLibrary: UIButton!
    @IBOutlet weak var btnCancel: UIButton!

    // MARK: - Completion Handler
    typealias ButtonCompletion = (_ buttonPressed: ButtonPressed) -> ()
    var buttonCompletion: ButtonCompletion?
  
    // MARK: - Actions
    @IBAction func didTapCloseButton(_ sender: AnyObject) {
        close { () -> () in
            DispatchQueue.main.async(execute: { () -> Void in
                self.buttonCompletion?(.cancel)
            })
        }
    }
    
    @IBAction func didTapChooseFromLibrary(_ sender: AnyObject) {
        close { () -> () in
            DispatchQueue.main.async(execute: { () -> Void in
                self.buttonCompletion?(.chooseFromLibrary)
            })
        }
    }

    @IBAction func didTapCamera(_ sender: AnyObject) {
        close { () -> () in
            DispatchQueue.main.async(execute: { () -> Void in
                self.buttonCompletion?(.camera)
            })
        }
    }
    
    // MARK: - Initialisers
    class func show( _ btnTitles: [String]? = [] ,completion: @escaping (_ buttonPressed: ButtonPressed) -> ()) -> CustomActionSheet {
        let sheet = UINib(nibName: "ChooseImageActionSheet", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomActionSheet
        if let titles = btnTitles , titles.count > 2{
            sheet.btnCamera.setTitle(titles[0], for: UIControlState())
            sheet.btnLibrary.setTitle(titles[1], for: .selected)
            sheet.btnCancel.setTitle(titles[2], for: UIControlState())
        }
        sheet.buttonCompletion = completion
        let window = UIApplication.shared.keyWindow
        window?.addSubview(sheet)
        var f = sheet.frame
        f.origin = CGPoint(x: 0, y: 0)
        f.size.width = UIScreen.main.bounds.width
        f.size.height = UIScreen.main.bounds.height
        sheet.frame = f
        
        sheet.sheetBottomConstraint.constant = -sheet.actionSheet.frame.size.height
        sheet.layoutIfNeeded()
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            sheet.alpha = 1.0
            }, completion: { (completed) -> Void in
                sheet.sheetBottomConstraint.constant = 0
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    sheet.layoutIfNeeded()
                    //animate in
                    }, completion: { (completed) -> Void in
                })
        }) 
        
        return sheet
    }
    
    
 
    func close(_ completion: (() -> ())?) {
        sheetBottomConstraint.constant = -actionSheet.frame.size.height
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.layoutIfNeeded()
            }, completion: { (completed) -> Void in
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    self.alpha = 0.0
                    }, completion: { (completed) -> Void in
                        self.removeFromSuperview()
                        completion?()
                })
        }) 

    }

}
