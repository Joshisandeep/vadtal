//
//  PlayerCell.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 19/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class PlayerCell: ConstrainedTableViewCell {
    @IBOutlet var playerView : YTPlayerView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Method
    
    func prepareToPlay(_ videoID:String){
        var playerVars = [String:AnyObject]()
        playerVars["controls"]          = 1 as AnyObject?
        playerVars["playsinline"]       = 1 as AnyObject?
        playerVars["autohide"]          = 1 as AnyObject?
        playerVars["modestbranding"]    = 1 as AnyObject?
        playerVars["iv_load_policy"]    = 3 as AnyObject?
        playerVars["rel"]               = 0 as AnyObject?
        playerView.setPlaybackQuality(YTPlaybackQuality.auto)
        playerView.load(withVideoId: videoID, playerVars: playerVars)
    }
    
    
    
}

// MARK: - YTPlayerView Delegate
extension PlayerCell :YTPlayerViewDelegate{
    
    func playerView(_ playerView: YTPlayerView, receivedError error: YTPlayerError) {
        jprint("error:\(error.rawValue)")
        self.playerView.isHidden = false
    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
        jprint("state:\(state.rawValue)")
        
    }
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        self.playerView.isHidden = false
    }
}
