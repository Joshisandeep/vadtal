//
//  VideoCell.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 21/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class VideoCell: ConstrainedCollectionViewCell {
    @IBOutlet var playerView : YTPlayerView!
    @IBOutlet var lblTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Method
    
    func prepareToPlay(_ videoID:String){
        var playerVars = [String:AnyObject]()
        playerVars["controls"]          = 0 as AnyObject?
        playerVars["playsinline"]       = 1 as AnyObject?
//        playerVars["autohide"]          = 1
        playerVars["modestbranding"]    = 0 as AnyObject?
//        playerVars["iv_load_policy"]    = 3
//        playerVars["rel"]               = 0
        playerView.setPlaybackQuality(YTPlaybackQuality.small)
        playerView.load(withVideoId: videoID, playerVars: playerVars)
    }
}
