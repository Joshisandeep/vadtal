//
//  ToolBar.swift
//  VoteMe
//
//  Created by Yudiz Solutions Pvt.Ltd. on 30/06/16.
//  Copyright © 2016 Yudiz Solutions Pvt.Ltd. All rights reserved.
//

import UIKit

class ToolBarView: ConstrainedView {
    @IBOutlet var btnOK: UIButton!
   
    // MARK: Variables
    enum Tapped {
        case ok, discard
    }
    var actionBlock: ((_ tapped: Tapped,_ alert: ToolBarView) -> ())?
    
    class func instantiateViewFromNib() -> ToolBarView {
        let obj = Bundle.main.loadNibNamed("ToolBarView", owner: nil, options: nil)![0] as! ToolBarView
        return obj
    }
    
    // MARK: Init
    // This will work only for two buttons (limitation)
    class func show(_ btnTitle: String? = nil) -> ToolBarView {
        let intance = instantiateViewFromNib()

        if let title = btnTitle{
            intance.btnOK.setTitle(title, for: UIControlState())
        }
        
        return intance
    }
    
    func handleTappedAction(_ block: @escaping (_ tapped: Tapped, _ alert: ToolBarView) -> ()){
        actionBlock = block
    }
}
//MARK:-
extension ToolBarView{
    @IBAction func btnOKTapped(_ sender: UIButton!) {
        actionBlock?(Tapped.ok,self)
    }

}
