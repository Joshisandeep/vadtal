// MVCarouselCollectionView.swift
//
// Copyright (c) 2015 Andrea Bizzotto (bizz84@gmail.com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import UIKit
import Foundation

/*
 * TODO: Would be nice to support spacing between pages. The link below explains how to do this but
 * the code sample needs to be converted to Auto Layout
 * http://stackoverflow.com/questions/13228600/uicollectionview-align-logic-missing-in-horizontal-paging-scrollview
 */
@objc public protocol MVCarouselCollectionViewDelegate {
    // method to provide a custom loader for a cell
    @objc optional func imageLoaderForCell(atIndexPath indexPath: IndexPath, imagePath: String) -> MVImageLoaderClosure
    func carousel(_ carousel: MVCarouselCollectionView, didSelectCellAtIndexPath indexPath: IndexPath)
    func carousel(_ carousel: MVCarouselCollectionView, didScrollToCellAtIndex cellIndex : NSInteger)
}

open class MVCarouselCollectionView: UICollectionView, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    fileprivate let reuseID = "SomeReuseID"

    // MARK: Variables
    open var imagePaths : [String] = []
    open var selectDelegate : MVCarouselCollectionViewDelegate?
    open var currentPageIndex : Int = 0
    open var maximumZoom : Double = 0.0

    // Default clousure used to load images
    open var commonImageLoader: MVImageLoaderClosure?

    // Trick to avoid updating the page index more than necessary
    fileprivate var clientDidRequestScroll : Bool = false

    // MARK: Initialisation
    override open func awakeFromNib() {
        super.awakeFromNib()
        prepareUI()
    }
    
    
}
//MARK:- Private
extension MVCarouselCollectionView{
    func prepareUI() {
        self.delegate = self
        self.dataSource = self
        
        // Loading bundle from class, see: http://stackoverflow.com/questions/25138989/uicollectionview-nib-from-a-framework-target-registered-as-a-cell-fails-at-runt
        let bundle = Bundle(for: MVCarouselCell.self)
        let nib = UINib(nibName : "MVCarouselCell", bundle: bundle)
        self.register(nib, forCellWithReuseIdentifier: self.reuseID)

    }
    public func updatePageIndex() {
        let pageIndex = self.getPageNumber()
        if currentPageIndex != pageIndex {
            //            println("old page: \(currentPageIndex), new page: \(pageIndex)")
            currentPageIndex = pageIndex
            self.selectDelegate?.carousel(self, didScrollToCellAtIndex: pageIndex)
        }
    }
    
    public func getPageNumber() -> NSInteger {
        
        // http://stackoverflow.com/questions/4132993/getting-the-current-page
        let width : CGFloat = self.frame.size.width
        var page : NSInteger = NSInteger((self.contentOffset.x + (CGFloat(0.5) * width)) / width)
        let numPages = self.numberOfItems(inSection: 0)
        if page < 0 {
            page = 0
        }
        else if page >= numPages {
            page = numPages - 1
        }
        return page
    }
    
    public func setCurrentPageIndex(_ pageIndex: Int, animated: Bool) {
        self.currentPageIndex = pageIndex
        self.clientDidRequestScroll = true;
        
        let indexPath = IndexPath(row: currentPageIndex, section: 0)
        self.scrollToItem(at: indexPath, at:UICollectionViewScrollPosition.centeredHorizontally, animated: animated)
    }
    
    
    public func resetZoom() {
        for cell in self.visibleCells as! [MVCarouselCell] {
            cell.resetZoom()
        }
    }
}
//MARK:- UIScrollViewDelegate
extension MVCarouselCollectionView{
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == self {
            if !self.clientDidRequestScroll {
                self.updatePageIndex()
            }
        }
    }
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == self {
            self.clientDidRequestScroll = false
            self.updatePageIndex()
        }
    }
    public func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
        if scrollView == self {
            self.clientDidRequestScroll = false
            self.updatePageIndex()
        }
    }

}

//MARK:- UICollectionViewDataSource
extension MVCarouselCollectionView{
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imagePaths.count
    }
    
    @objc(collectionView:cellForItemAtIndexPath:) public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // Should be set at this point
        assert(commonImageLoader != nil)
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.reuseID, for: indexPath) as! MVCarouselCell
        cell.cellSize =         CGSize(width: self.bounds.size.width-20, height: self.bounds.size.height)

        
        // Pass the closure to the cell
        let imagePath = self.imagePaths[(indexPath as NSIndexPath).row]
        let loader = self.selectDelegate?.imageLoaderForCell?(atIndexPath: indexPath, imagePath: imagePath)
        cell.imageLoader = loader != nil ? loader : self.commonImageLoader
        // Set image path, which will call closure
        cell.imagePath = imagePath
        cell.maximumZoom = maximumZoom
        cell.startImageLoading()
        // http://stackoverflow.com/questions/16960556/how-to-zoom-a-uiscrollview-inside-of-a-uicollectionviewcell
        if let gestureRecognizer = cell.scrollView.pinchGestureRecognizer {
            self.addGestureRecognizer(gestureRecognizer)
        }
        if let gestureRecognizer = cell.scrollView?.panGestureRecognizer {
            self.addGestureRecognizer(gestureRecognizer)
        }
        
        return cell
    }
    
    @objc(collectionView:didEndDisplayingCell:forItemAtIndexPath:) public func collectionView(_ collectionView : UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if let cell = cell as? MVCarouselCell {
            // http://stackoverflow.com/questions/16960556/how-to-zoom-a-uiscrollview-inside-of-a-uicollectionviewcell
            if let gestureRecognizer = cell.scrollView?.pinchGestureRecognizer {
                self.removeGestureRecognizer(gestureRecognizer)
            }
            if let gestureRecognizer = cell.scrollView?.panGestureRecognizer {
                self.removeGestureRecognizer(gestureRecognizer)
            }
        }
    }
    
    // MARK: UICollectionViewDelegateFlowLayout
    @objc(collectionView:layout:sizeForItemAtIndexPath:) public func collectionView(_ collectionView : UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.bounds.size.width-20, height: self.bounds.size.height)
    }
    
    @objc(collectionView:didSelectItemAtIndexPath:) public func collectionView(_ collectionView : UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectDelegate?.carousel(self, didSelectCellAtIndexPath: indexPath)
    }
}
