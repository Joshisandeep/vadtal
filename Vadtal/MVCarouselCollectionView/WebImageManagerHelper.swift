//
//  WebImageManagerHelper.swift
//  Checkin
//
//  Created by Yudiz Solutions Pvt.Ltd. on 02/06/16.
//  Copyright © 2016 Yudiz Solutions Pvt.Ltd. All rights reserved.
//

import Foundation
import UIKit


var imageViewLoadCached : ((imageView: UIImageView, imagePath : String, completion: (newImage: Bool) -> ()) -> ()) = {
    (imageView: UIImageView, imagePath : String, completion: (newImage: Bool) -> ()) in
    
    imageView.image = UIImage(named:imagePath)
    completion(newImage: imageView.image != nil)
}

var imageViewLoadFromPath: ((imageView: UIImageView, imagePath : String, completion: (newImage: Bool) -> ()) -> ()) = {
    (imageView: UIImageView, imagePath : String, completion: (newImage: Bool) -> ()) in
    
    var url = NSURL(string: imagePath)
    
    imageView.sd_setImageWithURL(url, completed: {
        (image : UIImage?, error: NSError?, cacheType: SDImageCacheType, imageURL: NSURL?) in
        
        completion(newImage: image != nil)
    })
}

