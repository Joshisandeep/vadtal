//
//  Location.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 13/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//


import UIKit
import CoreLocation
import MapKit

// MARK:- String For Location Message or Title
let LMNoPermission                          =  "No permission";
let LMLocationSettingsMsg                   =  "Please go to settings and allow us to use your location.";
let LMLocationOff                           =  "Location services are off";
let LMLocationOffMsg                        =  "Location services are off,Please go to settings and allow us to use your location.";

let LMLocationWhenInUse                     =  "To use location you must turn on 'WhenInUse' in the location services settings";
let LMLocationAlways                        =  "To use background location you must turn on 'Always' in the location services settings";
let LMBackgroundLocationOff                 =  "Background location is not enabled";

// MARK:- Enum For LocationPermission

enum LocationPermission: Int {
    case accepted;
    case denied;
    case error;
}

// MARK:- Class
class UserLocation: NSObject  {
    
    static var obj = UserLocation()
    
    // MARK: - Variables
    var locationManger: CLLocationManager = {
        let lm = CLLocationManager()
        lm.activityType = .other
        lm.desiredAccuracy = kCLLocationAccuracyBest
        return lm
    }()
    
    // Will be assigned by host controller. If not set can throw Exception.
    typealias LocationBlock = (CLLocation?, String?)->()
    var completionBlock : LocationBlock? = nil
    weak var controller: UIViewController!
    var latitude : Double = 0.0
    var longitude : Double =  0.0

    
    // MARk: - Func
    func fetchUserLocationForOnce(_ controller: UIViewController, block: LocationBlock?) {
        self.controller = controller
        locationManger.delegate = self
        completionBlock = block
        if checkAuthorizationStatus() {
            locationManger.startUpdatingLocation()
        }else{
            self.completionBlock?(nil,LMLocationWhenInUse)
        }
    }
    
    func checkAuthorizationStatus() -> Bool {
        let status = CLLocationManager.authorizationStatus()
        // If status is denied or only granted for when in use
        if status == CLAuthorizationStatus.denied || status == CLAuthorizationStatus.restricted {
            let title = LMLocationOff
            let msg = LMLocationWhenInUse
            UIAlertController.actionWithMessage(msg, title: title, type: .alert, buttons: ["Settings"], controller: controller, block: { (tapped) -> () in
                if tapped == "Settings" {
                    UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                }
            })
            
            return false
        } else if status == CLAuthorizationStatus.notDetermined {
            locationManger.requestWhenInUseAuthorization()
            return false
        } else if status == CLAuthorizationStatus.authorizedAlways || status == CLAuthorizationStatus.authorizedWhenInUse {
            return true
        }
        return false
    }
 
}

// MARK: - Location manager Delegation
extension UserLocation: CLLocationManagerDelegate {
  
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let firstLocation = locations.first!
        if let loc = locations.first{
            latitude    = loc.coordinate.latitude
            longitude   = loc.coordinate.longitude
        }
        jprint(":: Location object got ::")
        self.completionBlock?(firstLocation,nil)
        locationManger.delegate = nil
        self.controller = nil
        locationManger.stopUpdatingLocation()
        completionBlock = nil
    }
    
    func addressFromlocation(_ location: CLLocation, block: @escaping (String)->()){
        let geoLocation = CLGeocoder()
        geoLocation.reverseGeocodeLocation(location, completionHandler: { (placeMarks, error) -> Void in
            if let pmark = placeMarks , pmark.count > 0 {
                let place :CLPlacemark = pmark.last! as CLPlacemark
                if let addr = place.addressDictionary {
                    jprint("The address dictionary : \(place.addressDictionary)")
                    let cityName = addr["City"] as! NSString?
                    let countryName = addr["Country"] as! NSString?
                    var locationString :String = ""
                    if let cityN = cityName {
                        locationString += cityN as String
                    }
                    if let countN = countryName {
                        locationString += ", "
                        locationString += countN as String
                    }
                    block(locationString)
                }
            }
        })
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.completionBlock?(nil,kFailToFetchLocation)
        locationManger.delegate = nil
        self.controller = nil
        locationManger.stopUpdatingLocation()
        completionBlock = nil
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if checkAuthorizationStatus() {
            locationManger.startUpdatingLocation()
        }else{
            self.completionBlock?(nil,LMLocationWhenInUse)
        }
    }
}

