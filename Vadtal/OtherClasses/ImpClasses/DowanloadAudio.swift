//
//  DowanloadAudio.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 20/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import Foundation
import AFNetworking

class DowanloadAudio: NSObject{
    
    fileprivate var fileManager: FileManager {
        return FileManager.default
    }
    
    fileprivate func createDir(_ foldername: String) -> String? {
        var paths =  NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let docDirectory = paths[0] as NSString
        let cPath = docDirectory.appendingPathComponent(foldername)
        if !fileManager.fileExists(atPath: cPath) {
            do {
                try fileManager.createDirectory(atPath: cPath, withIntermediateDirectories: true, attributes: nil)
                return cPath
            } catch  let error as NSError {
                jprint("Error in creating temporary path: \(error.localizedDescription)")
                return nil
            }
        }
        return cPath
    }
    
    
    func downloadFileFromUrl(_ url:URL, block:@escaping (_ filepath:URL?,_ success:Bool)->()){
        let cpath = self.createDir("AudioAlbum")
        if let path = cpath{
            
            if !fileManager.fileExists(atPath: path){
                block(URL(fileURLWithPath: path), true)
                return
            }
            
            let configuration = URLSessionConfiguration.default
            let manager = AFURLSessionManager(sessionConfiguration: configuration)
            let request = URLRequest(url: url)
            let downloadTask = manager.downloadTask(with: request, progress: nil, destination: { (targetPath, response) -> URL in
                return URL(fileURLWithPath: path)
                }, completionHandler: { (response, filePath, error) in
                    jprint("File downloaded to: \(filePath)")
                    if error == nil{
                        block(filePath, true)
                    }else{
                        do {
                            try self.fileManager.removeItem(atPath: path)
                            block(nil, false)

                        } catch  let error as NSError {
                            jprint("Error in creating temporary path: \(error.localizedDescription)")
                            block(nil, false)
                        }
                    }
                    
            })
            downloadTask.resume()
        }else{
            block(nil, false)
            return
        }
    }
}
