//
//  Extensions.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 13/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//


import UIKit


extension IndexPath {
    // Return IndexPath
    func indexPathForCellContainingView(_ view: UIView, inTableView tableView:UITableView) -> IndexPath? {
        let viewCenterRelativeToTableview = tableView.convert(CGPoint(x: view.bounds.midX, y: view.bounds.midY), from:view)
        return tableView.indexPathForRow(at: viewCenterRelativeToTableview)
    }
}


// MARK: Important Extensions
extension UIApplication {
    
    class func dismissPresentedController(_ viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let presented = viewController?.presentedViewController {
            print("\n\npresented : \(presented)" )
            presented.dismiss(animated: false, completion: { () -> Void in
                return _ = dismissPresentedController()
            })
        }
        
        return viewController
    }
}

// MARK: - Text Display
extension UITextField {
    
    func setAttributedPlaceholder(_ text: String, font: UIFont, color: UIColor) {
        let mutatingAttributedString = NSMutableAttributedString(string: text)
        mutatingAttributedString.addAttribute(NSFontAttributeName, value: font, range: NSMakeRange(0, text.characters.count))
        mutatingAttributedString.addAttribute(NSForegroundColorAttributeName, value: color, range: NSMakeRange(0, text.characters.count))
        attributedPlaceholder = mutatingAttributedString
    }
    
    // This will give combined string with respective attributes
    func setAttributedPlaceholder(_ texts: [String], attributes: [[String : AnyObject]]) {
        let attbStr = NSMutableAttributedString()
        for (index,element) in texts.enumerated() {
            attbStr.append(NSAttributedString(string: element, attributes: attributes[index]))
        }
        attributedPlaceholder = attbStr
    }
}




extension NSMutableAttributedString {
    func setAttributedText(_ text: String, font: UIFont, color: UIColor) -> NSMutableAttributedString {
        let attribute = NSMutableAttributedString(string: text)
        attribute.addAttribute(NSFontAttributeName, value: font, range: NSMakeRange(0, text.characters.count))
        attribute.addAttribute(NSForegroundColorAttributeName, value: color, range: NSMakeRange(0, text.characters.count))
        return attribute
    }
}

// To calculate text rect
extension UIFont {
    // Will return size to fit rect for given string.
    func sizeOfString(_ string: String, constrainedToWidth width: Double) -> CGSize {
        return NSString(string: string).boundingRect(with: CGSize(width: width, height: DBL_MAX),
            options: NSStringDrawingOptions.usesLineFragmentOrigin,
            attributes: [NSFontAttributeName: self],
            context: nil).size
    }
}

// MARK: - Alerts
// Inline Alert message pop up with controller
extension UIAlertController {
    
    class func actionWithMessage(_ message: String? = nil, title: String?  = nil, type: UIAlertControllerStyle, buttons: [String], buttonStyles: [UIAlertActionStyle] = [],controller: UIViewController ,block:@escaping (_ tapped: String)->()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: type)
        for (idx,btn) in buttons.enumerated() {
            var style = UIAlertActionStyle.default
            if !buttonStyles.isEmpty && idx < buttonStyles.count{
                style = buttonStyles[idx]
            }
            alert.addAction(UIAlertAction(title: btn, style: style, handler: { (action) -> Void in
                block(btn)
            }))
        }
        alert.addAction(UIAlertAction(title: kCancel, style: UIAlertActionStyle.cancel, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
    
        
    class func tappedAction(_ message: String? = nil, title: String?  = nil, type: UIAlertControllerStyle,controller: UIViewController ,block:@escaping (_ tapped: Tapped)->()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle:type)
        
        alert.addAction(UIAlertAction(title: kOK, style: UIAlertActionStyle.default, handler: { (action) -> Void in
            block(Tapped.okay)
        }))
        
        alert.addAction(UIAlertAction(title: kCancel, style: UIAlertActionStyle.cancel, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
    class func tappedActionYesNo(_ message: String? = nil, title: String?  = nil, type: UIAlertControllerStyle,controller: UIViewController, style: UIAlertActionStyle = UIAlertActionStyle.default ,block:@escaping (_ tapped: Tapped)->()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle:type)
        
        alert.addAction(UIAlertAction(title: kYes, style: style, handler: { (action) -> Void in
            block(Tapped.okay)
        }))
        
        alert.addAction(UIAlertAction(title: kNo, style: UIAlertActionStyle.cancel, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
}

// MARK: - Devices
// To identify devices and its Family
extension UIDevice {
    
    class func isiPhone4() -> Bool {
        return _screenSize.height == 480.0 && UIDevice.current.userInterfaceIdiom == .phone
    }
    class func isiPhone5() -> Bool {
        return _screenSize.height == 568.0 && UIDevice.current.userInterfaceIdiom == .phone
    }
    class func isiPhone6() -> Bool {
        return _screenSize.height == 667.0 && UIDevice.current.userInterfaceIdiom == .phone
    }
    class func isiPhone6plus() -> Bool {
        return _screenSize.height == 736.0 && UIDevice.current.userInterfaceIdiom == .phone
    }
    class func isiPad() -> Bool {
        return _screenSize.height == 1024 && UIDevice.current.userInterfaceIdiom == .pad
    }
    class func isiPadPro() -> Bool {
        return _screenSize.width == 1024 && UIDevice.current.userInterfaceIdiom == .pad
    }
    class func isPad() -> Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    class func isPhone() -> Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    class func isAtLeastiOSVersion(_ ver: String) -> Bool {
        return self.current.systemVersion.compare(ver, options: NSString.CompareOptions.numeric, range: nil, locale: nil) != ComparisonResult.orderedAscending
    }
    
    class func configureFor(i6p b1:()->(), i6 b2:()->(), i5 b3:()->(), i4 b4:()->()) {
        if self.isiPhone6plus() {
            b1()
        } else if self.isiPhone6() {
            b2()
        } else if self.isiPhone5() {
            b3()
        } else if self.isiPhone4() {
            b4()
        }
    }
    
}
