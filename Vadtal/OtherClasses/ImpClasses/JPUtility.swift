//
//  JPUtility.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 13/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//


import UIKit


class JPUtility: NSObject {
    
    static let shared = JPUtility()
    
    func agoStringFromTime(_ date: Date) -> String {
        var timeScale = [String : Int64]()
        timeScale["sec"] = 1
        timeScale["min"] = 60
        timeScale["hour"] = 3600
        timeScale["day"] = 86400
        timeScale["week"] = 605800
        timeScale["month"] = 2629743
        timeScale["year"] = 31556926
        var scale : String!
        let timeAgo = -1 * date.timeIntervalSinceNow
        if timeAgo < 60 {
            scale = "sec"
            return "Just now"
        } else if timeAgo < 3600 {
            scale = "min"
        } else if timeAgo < 86400 {
            scale = "hour"
        } else if timeAgo < 605800 {
            scale = "day"
        } else if timeAgo < 2629743 {
            scale = "week"
        } else if timeAgo < 31556926 {
            scale = "month"
        } else {
            scale = "year"
        }
        let ago = Int64(timeAgo) / timeScale[scale]!
        var s = ""
        if ago > 1 {
            s = "s"
        }
        
        return "\(ago) \(scale)\(s) ago"
    }
    
    func sjCount(_ cnt: Double) -> String {
        var float : Double = 0.0
        var integer : Double = 0.0
        var count = "\(Int(cnt))"
        if cnt > 999 && cnt < 999999 {
            float = cnt/1000.0
            let fr = Int(modf(float, &integer) * 100)
            if fr > 0 {
                count = String(format: "%.02f", float)
            } else {
                count = "\(Int(integer))"
            }
            count  += "K"
        } else if cnt > 999999  && cnt<999999999{
            float = cnt/1000000.0
            let fr = Int(modf(float, &integer) * 100)
            if fr > 0 {
                count = String(format: "%.02f", float)
            } else {
                count = "\(Int(integer))"
            }
            count  += "M"
        } else if cnt > 999999999  {
            float = cnt/1000000000.0
            let fr = Int(modf(float, &integer) * 100)
            if fr > 0 {
                count = String(format: "%.02f", float)
            } else {
                count = "\(Int(integer))"
            }
            count  += "B"
            
        }
        
        return count
    }
}





