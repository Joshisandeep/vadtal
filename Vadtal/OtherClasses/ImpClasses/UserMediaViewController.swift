//
//  SJUserMedia.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 13/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//


import UIKit
import Foundation
import AVFoundation
import AVKit

/**
 This Enum Represents relationship user MediaOption at any time in application. It is bound with
 
 */
enum MediaOption: String {
    case TakeVideo                       = "Take Video"
    case TakePhoto                       = "Take Photo"
    case ChooseFromLibrary               = "Choose From Library"
}

/**
 This Enum Represents relationship user media type at any time in application. It is bound with
 
 - Audio:     Media type Audio
 - Video:     Media type Video.
 - Photo:     Media type Photo
 */

enum MediaType: String {
    case Audio  = "Audio"
    case Video  = "Video"
    case Image  = "Image"
}

class UserMediaViewController: ParentVC {
    // MARK: - Enums
    enum ButtonPressed {
        case done, cancel
    }
    // MARK: - Completion Handler
    
    typealias MediaBlock = ((_ buttonPressed: ButtonPressed,_ type: MediaType,_ image:UIImage?,_ path:String?) -> ())
    var mediaCompletion:MediaBlock?
    var outputMediaName : String?
}


// MARK: - User Media Open
extension UserMediaViewController {
   
    func mediaActionSheet(_ block:@escaping (_ tapped: MediaType)->()){
        self.view.endEditing(true)
        let mediaOption: [MediaType] = [MediaType.Audio ,MediaType.Video ,MediaType.Image]
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for option in mediaOption {
            alert.addAction(UIAlertAction(title: option.rawValue, style: .default, handler: { (action) -> Void in
                block(option)
            }))
        }
        alert.addAction(UIAlertAction(title: kCancel, style: UIAlertActionStyle.cancel, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    func videoActionSheet(_ outputName : String? = nil,block: @escaping MediaBlock){
        outputMediaName = outputName
        self.view.endEditing(true)
        let mediaOption: [MediaOption] = [MediaOption.TakeVideo ,MediaOption.ChooseFromLibrary]
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for option in mediaOption {
            
            alert.addAction(UIAlertAction(title: option.rawValue, style: .default, handler: { (action) -> Void in
                if option == .TakeVideo{
                    self.takeVideo()
                }else if option == .ChooseFromLibrary{
                    self.openVideoGallary()
                }
                
            }))
        }
        alert.addAction(UIAlertAction(title: kCancel, style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
            self.outputMediaName = nil
            block(.cancel, .Video, nil, "")
            
        }))
        
        present(alert, animated: true, completion: nil)
        
        mediaCompletion = block
        
    }
    
    func photoActionSheet(_ block: @escaping MediaBlock){
        outputMediaName = nil
        self.view.endEditing(true)
        
        self.view.addSubview(CustomActionSheet.show(completion: { (buttonPressed) in
            if  buttonPressed == .camera{
                self.takePhoto()
            }else if  buttonPressed == .chooseFromLibrary{
                self.openPhotoGallary()

            }
        }))
        mediaCompletion = block
    }
    
   
    // MARK:-  Share Activity
    
    func openShareKit(_ urlPath:String,compilation: @escaping (_ success: Bool) -> ()){
        let activityVC = UIActivityViewController(activityItems: [urlPath], applicationActivities: nil)
        activityVC .setValue("\(_appName) Recommendation", forKey: "subject")
        activityVC.completionWithItemsHandler = {
            (activity, success, items, error) in
            jprint("Activity: \(activity) Success: \(success) Items: \(items) Error: \(error)")
            if success
            {
                compilation(true)
                jprint("success")
            }else{
                compilation(false)
                jprint("user cancel")
            }
        }
        self.present(activityVC, animated: true, completion: nil)
    }
    // MARK:- Video Camera/Gallary Open Code Here
    
    fileprivate  func takeVideo() {
        if UIImagePickerController.isSourceTypeAvailable(.camera)
        {
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            imagePicker.mediaTypes = ["public.movie"]
            //            imagePicker.videoQuality = .TypeMedium
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            cameraNotAvailable()
        }
    }
    
    fileprivate  func openVideoGallary()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        imagePicker.mediaTypes = ["public.movie"]
        //        imagePicker.videoQuality = .TypeMedium
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK:- Photo Camera/Gallary Open Code Here
    
    fileprivate  func takePhoto()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera)
        {
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            cameraNotAvailable()
        }
    }
    
    fileprivate  func cameraNotAvailable() {
        Popups.instance.ShowPopup(self,title: kCameraNotAvailable, message: kAllowCameraAccess)
    }
    
    fileprivate  func openPhotoGallary()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // This method is using for video export .mov to .mp4
    
    fileprivate  func compressVideo(_ inputURL: URL, outputURL: URL, completion:  @escaping ((AVAssetExportSession) -> ())) {
        // code here
        jprint("\ninputURL :\(inputURL) \noutputURL :\(outputURL)")
        let asset: AVURLAsset = AVURLAsset(url: inputURL, options: nil)
        let exportSession : AVAssetExportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetMediumQuality)!
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileTypeMPEG4
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously {
            completion(exportSession)
        }
        
    }
    
    // This method is using for getting video asset first frame image
    func getAssetFirstFrameImage(_ url:URL) -> UIImage?{
        do {
            let asset = AVURLAsset(url: url, options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            let imag = UIImage(cgImage: cgImage)
            return imag
            // lay out this image view, or if it already exists, set its image property to uiImage
        } catch let error as NSError {
            jprint("Error generating thumbnail: \(error)")
            return nil
        }
    }
    
}

// MARK: - UIImagePickerController Delegate
extension UserMediaViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        jprint("info:\(info)")
        if let mediaType = info[UIImagePickerControllerMediaType]  as? String{
            if mediaType == "public.movie"{
                if let _ = info[UIImagePickerControllerMediaURL] as? URL{
                    _ = dismiss(animated: true, completion: nil)
                    
                }else{
                   _ =  dismiss(animated: true, completion: nil)
                }
            }else{
                let image = info[UIImagePickerControllerEditedImage] as? UIImage
                mediaCompletion?(.done, .Image, image, "")
                dismiss(animated: true, completion: nil)
            }
        }else{
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
}
