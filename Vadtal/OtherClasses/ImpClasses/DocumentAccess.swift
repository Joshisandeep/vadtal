//
//  DocumentAccess.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 13/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//


import UIKit


class DocumentAccess: NSObject {
    
    static var obj = DocumentAccess()
    
    fileprivate lazy var formatter1: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddhhmmssSSSS"
        return formatter
    }()
    fileprivate var fileManager: FileManager {
        return FileManager.default
    }
    
    fileprivate lazy var media: NSString = {
        return self.cachePath("media")!
    }() as NSString
    
    fileprivate lazy var video: NSString = {
        return self.cachePath("video")!
    }() as NSString
    
    fileprivate lazy var audio: NSString = {
        return self.cachePath("audio")!
    }() as NSString
    
    fileprivate lazy var document: NSString = {
        return self.documentPath("pdf_files")!
    }() as NSString
    
    fileprivate lazy var temporaryMedia: NSString = {
        return self.temporaryPath("temp_media")!
    }() as NSString
    
    fileprivate lazy var temporaryAudio: NSString = {
        return self.temporaryPath("temp_audio")!
    }() as NSString
    fileprivate lazy var temporaryVideo: NSString = {
        return self.temporaryPath("temp_video")!
    }() as NSString
    
    // MARK: Paths For Directory
    fileprivate func documentPath(_ foldername: String) -> String? {
        var paths =  NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let docDirectory = paths[0] as NSString
        let cPath = docDirectory.appendingPathComponent(foldername)
        if !fileManager.fileExists(atPath: cPath) {
            do {
                try fileManager.createDirectory(atPath: cPath, withIntermediateDirectories: true, attributes: nil)
                return cPath
            } catch  let error as NSError {
                jprint("Error in creating temporary path: \(error.localizedDescription)")
                return nil
            }
        }
        return cPath
    }
    fileprivate func cachePath(_ foldername: String) -> String? {
        var paths =  NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let docDirectory = paths[0] as NSString
        let cPath = docDirectory.appendingPathComponent(foldername)
        if !fileManager.fileExists(atPath: cPath) {
            do {
                try fileManager.createDirectory(atPath: cPath, withIntermediateDirectories: true, attributes: nil)
                return cPath
            } catch  let error as NSError {
                jprint("Error in creating temporary path: \(error.localizedDescription)")
                return nil
            }
        }
        return cPath
    }
    
    fileprivate func temporaryPath(_ foldername: String) -> String? {
        let tempDirectory = NSTemporaryDirectory() as NSString
        let tPath = tempDirectory.appendingPathComponent(foldername)
        if !fileManager.fileExists(atPath: tPath) {
            do {
                try fileManager.createDirectory(atPath: tPath, withIntermediateDirectories: true, attributes: nil)
                return tPath
            } catch  let error as NSError {
                jprint("Error in creating temporary path: \(error.localizedDescription)")
                return nil
            }
        } else {
            return tPath
        }
    }
    
    
    // This will check if media exist at path and return if true.
    func mediaForName(_ filename: String) -> URL? {
        let fullpath = media.appendingPathComponent(filename)
        if fileManager.fileExists(atPath: fullpath) {
            return URL(fileURLWithPath: fullpath)
        } else {
            return nil
        }
    }
    
    func documentForName(_ filename: String) -> URL? {
        let fullpath = document.appendingPathComponent(filename)
        if fileManager.fileExists(atPath: fullpath) {
            return URL(fileURLWithPath: fullpath)
        } else {
            return nil
        }
    }
    
    // MARK : Audio File
    // It will remove old file will give new file path
    func createAudioFileUrl(_ fileName: String) -> URL {
        let fullPath = audio.appendingPathComponent(fileName) as String
        if fileManager.fileExists(atPath: fullPath) {
            do {
                try fileManager.removeItem(atPath: fullPath)
            } catch _ {
            }
        }
        return  URL(fileURLWithPath: fullPath)
    }
    // This will check if audio exist at path and return if true.
    func audioForName(_ filename: String) -> URL? {
        let fullpath = audio.appendingPathComponent(filename)
        if fileManager.fileExists(atPath: fullpath) {
            return URL(fileURLWithPath: fullpath)
        } else {
            return nil
        }
    }
    
    
    // This will check if video exist at path and return if true.
    func videoForName(_ filename: String) -> URL? {
        let fullpath = video.appendingPathComponent(filename)
        if fileManager.fileExists(atPath: fullpath) {
            return URL(fileURLWithPath: fullpath)
        } else {
            return nil
        }
    }
    
    
    // This will give media url with filename given.
    func mediaUrlForName(_ filename: String) -> URL{
        let path = media.appendingPathComponent(filename)
        return URL(fileURLWithPath: path)
        
    }
    
    func documentUrlForName(_ filename: String) ->URL{
        let path = document.appendingPathComponent(filename)
        return URL(fileURLWithPath: path)
    }
    
    
    func removeMediaForName(_ filename: String) -> Bool {
        let fullpath = media.appendingPathComponent(filename)
        do { try fileManager.removeItem(atPath: fullpath)
            return true
        } catch let error as NSError {
            jprint("Error in removing media: \(error.localizedDescription)")
            return false
        }
    }
    
    // This will give audio url with filename given.
    func audioUrlForName(_ filename: String) -> URL{
        let path = audio.appendingPathComponent(filename)
        return URL(fileURLWithPath: path)
    }
    
    func getAudioForName(_ name: String) -> URL? {
        let fullPath = audio.appendingPathComponent(name) as String
        if FileManager.default.fileExists(atPath: fullPath) {
            return  URL(fileURLWithPath: fullPath)
        } else {
            return nil
        }
    }
    func removeAudioForName(_ filename: String) -> Bool {
        let fullpath = audio.appendingPathComponent(filename)
        do { try fileManager.removeItem(atPath: fullpath)
            return true
        } catch let error as NSError {
            jprint("Error in removing media: \(error.localizedDescription)")
            return false
        }
    }
    // This will give video url with filename given.
    func videoUrlForName(_ filename: String) -> URL {
        let path = video.appendingPathComponent(filename)
        return URL(fileURLWithPath: path)
    }
    
    
    func removeVideoForName(_ filename: String) -> Bool {
        let fullpath = video.appendingPathComponent(filename)
        do { try fileManager.removeItem(atPath: fullpath)
            return true
        } catch let error as NSError {
            jprint("Error in removing media: \(error.localizedDescription)")
            return false
        }
    }
    
    
    // MARK: Store Image to Cache
    /* we will append _thumb next to its name that way we can store both thumb and full image */
    func imageForName(_ filename: String, isthumb: Bool) -> UIImage? {
        let fullpath = media.appendingPathComponent(filename + (isthumb ? "_thumb" : ""))
        return UIImage(contentsOfFile: fullpath)
    }
    
    func setImage(_ img: UIImage, isthumb: Bool, forName name: String) -> Bool {
        let fullpath = media.appendingPathComponent(name + (isthumb ? "_thumb" : ""))
        return fileManager.createFile(atPath: fullpath, contents: UIImageJPEGRepresentation(img, 1), attributes: nil)
    }
    
    // MARK: Store Media To Cache
    func setMedia(_ medData: Data, forName filename: String) -> Bool {
        let fullpath = media.appendingPathComponent(filename)
        return fileManager.createFile(atPath: fullpath, contents: medData, attributes: nil)
    }
    
    
    
    // MARK : Randome Names
    func randomMediaName(_ exten: String? = nil) -> String {
        var strDate = formatter1.string(from: Date())
        if let extensn = exten {
            strDate.append(".\(extensn)")
        }
        return strDate
    }
    
    
    func randomAudioName(_ exten: String? = nil) -> String {
        var strDate = formatter1.string(from: Date())
        if let extensn = exten {
            strDate.append(".\(extensn)")
        }else{
            strDate.append(".mp4")
        }
        return strDate
    }
    
    func randomVideoName(_ exten: String? = nil) -> String {
        var strDate = formatter1.string(from: Date())
        if let extensn = exten {
            strDate.append(".\(extensn)")
        }else{
            strDate.append(".mp4")
        }
        return strDate
    }
    
}

