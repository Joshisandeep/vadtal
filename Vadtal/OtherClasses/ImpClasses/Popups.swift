//
//  Popups.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 13/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import Foundation
import UIKit

class Popups {
    
    static var instance = Popups()
    
    // MARK: - Variables
    var alertComletion : ((String) -> Void)!
    var alertButtons : [String]!
    
    // MARK - func
    func ShowAlert(_ sender: UIViewController, title: String, message: String, buttons : [String], completion: ((_ buttonPressed: String) -> Void)?) {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for b in buttons {
            
            alertView.addAction(UIAlertAction(title: b, style: UIAlertActionStyle.default, handler: {
                (action : UIAlertAction) -> Void in
                completion!(action.title!)
            }))
        }
        sender.present(alertView, animated: true, completion: nil)

    }
    
    func ShowPopup(_ sender: UIViewController, title : String, message : String) {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: kOK, style: UIAlertActionStyle.default, handler: nil))
        sender.present(alertView, animated: true, completion: nil)
    }
}
