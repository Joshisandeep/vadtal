//
//  AudioPlayer.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 20/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

enum PlayerState: Int{
    case readyToPlay,fail,unKnown,complete,terminate
}
typealias ActionOfPlayer = (_ state:PlayerState) -> Void

import UIKit
import AVFoundation

class AudioPlayer: NSObject{
    
    static var sharedInstance = AudioPlayer()
    var audioPlayer: AVPlayer!
    var actionBlock: ActionOfPlayer!
    var blockTemp : ((Int)->Void)?
    var currentIndex: NSInteger!
    var isPlaying: Bool = false
    var isAudioStart: Bool = false
    
    fileprivate override init() {
        super.init()
        
        currentIndex = -1
    }
    
    func prepareForPlayingAudio(withURL audioURL: URL!) {
        
        let recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(AVAudioSessionCategorySoloAmbient)
            try recordingSession.setActive(true)
        }catch let err {
            jprint("error \(err)")
        }
        
        if audioPlayer == nil {
            
            audioPlayer = AVPlayer(url:audioURL)
            audioPlayer.volume = 1.0
            audioPlayer.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions.new, context: nil)
            _defaultCenter.addObserver(self, selector: #selector(AudioPlayer.playerItemDidReachEnd(_:)), name:NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: audioPlayer.currentItem)
        }else{
            
            audioPlayer.pause()
            audioPlayer.removeObserver(self, forKeyPath: "status")
            _defaultCenter.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
            audioPlayer = AVPlayer(url:audioURL)
            audioPlayer.volume = 1.0
            audioPlayer.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions.new, context: nil)
            _defaultCenter.addObserver(self, selector: #selector(AudioPlayer.playerItemDidReachEnd(_:)), name:NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: audioPlayer.currentItem)
        }
    }
    
    func prepareForPlayingAudio(withUrl audioURL:URL, block:@escaping ActionOfPlayer){
        
        isPlaying = true
        isAudioStart = false
        if self.actionBlock != nil
        {
            self.actionBlock = nil
        }
        self.actionBlock = block
        
        if FileManager.default.fileExists(atPath: audioURL.lastPathComponent) {
            self.prepareForPlayingAudio(withURL: audioURL)
        }else{
            DowanloadAudio().downloadFileFromUrl(audioURL, block: { (filepath, state) -> Void in
                
                if let path = filepath , state == true{
                    if let url = DocumentAccess.obj.getAudioForName(path.lastPathComponent){
                        self.prepareForPlayingAudio(withURL: url)
                    }else{
                        self.prepareForPlayingAudio(withURL: path)
                    }
                }else{
                    self.prepareForPlayingAudio(withURL: audioURL)
                }
            })
        }
    }
    
    func playerItemDidReachEnd(_ notification:Foundation.Notification){
        currentIndex = -1
        isPlaying = false
        self.actionBlock?(PlayerState.complete)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        if (keyPath == "status")
        {
            if isPlaying == false{
                currentIndex = -1
                self.actionBlock?(PlayerState.complete)
            }else{
                if(audioPlayer.status == AVPlayerStatus.readyToPlay)
                {
                    jprint("ready to play")
                    self.actionBlock?(PlayerState.readyToPlay)
                    isAudioStart = true
                }
                else if (audioPlayer.status == AVPlayerStatus.failed)
                {
                    jprint("failed")
                    currentIndex = -1
                    isPlaying = false
                    self.actionBlock?(PlayerState.fail)
                }
                else if (audioPlayer.status == AVPlayerStatus.unknown)
                {
                    jprint("unknown")
                    currentIndex = -1
                    isPlaying = false
                    self.actionBlock?(PlayerState.unKnown)
                }
            }
        }
    }
    
    //    func setCurrentAudioTime(value: Float){
    //        audioPlayer.currentTime()
    //    }
    
    func timeFormat(_ value: Float) -> String{
        let minutes = floor(value/60)
        
        let seconds = roundf(value) - (minutes * 60)
        let roundedSeconds = ilogbf(seconds)
        
        return String(format: "0:%.2d",roundedSeconds)
    }
    /*
     * Get the whole length of the audio file
     */
    func getAudioDuration() -> Float{
        let duration = CMTimeGetSeconds(audioPlayer.currentItem!.asset.duration);
        return Float(duration)
    }
    
    /*
     * Get the time where audio is playing right now
     */
    func getCurrentAudioTime() -> Float{
        
        let currTime : TimeInterval = CMTimeGetSeconds(audioPlayer.currentTime());
        return Float(currTime)
    }
    
    func starPlayingAudio(){
        audioPlayer?.play()
    }
    
    func stopPlayingAudio() {
        currentIndex = -1
        isPlaying = false
        if isAudioStart == true{
            audioPlayer.pause()
        }
    }
    
    func seekPlayer(_ value:Int64){
        let time = CMTime.init(value: CMTimeValue(value), timescale: CMTimeScale(1.0))
        if audioPlayer != nil{
            if audioPlayer.status == AVPlayerStatus.readyToPlay{
                audioPlayer.seek(to: time)
            }
        }
    }
    
    func pausePlayingAudio(){
        audioPlayer.pause()
    }
    
    func resumePlayingAudio(){
        audioPlayer.play()
    }
}
