//
//  Subclasses.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 13/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//


import UIKit

// MARK: - Useful Classes
class GenericCollectionViewCell: ConstrainedCollectionViewCell {
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var lblSubtitle2: UILabel!
    @IBOutlet var imgv: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}


class GenericTableViewCell: ConstrainedTableViewCell {
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var lblSubtitle2: UILabel!
    @IBOutlet var imgv: UIImageView!
    @IBOutlet var imgBg: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}

class GenericTableViewCell1: ConstrainedTableViewCell {
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var imgv: UIImageView!
    @IBOutlet var imgBg: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            backgroundColor    = UIColor.white.withAlphaComponent(0.45)
        } else {
            backgroundColor = UIColor.clear
        }
    }
    
}

class PushWithoutAnimationSegue: UIStoryboardSegue {
    override func perform() {
        let svc =  source
        svc.navigationController?.pushViewController(destination , animated: false)
    }
    
}

class RoundedImageView: UIImageView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.bounds.size.height/2
        self.layer.masksToBounds = true
    }
}
class WhiteRoundedImageView: UIImageView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.bounds.size.height/2
        self.layer.masksToBounds = true
        self.layer.borderWidth  = 1
        self.layer.borderColor  = UIColor.white.cgColor
    }
}
class  WhiteBorderImageView: UIImageView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.masksToBounds = true
        self.layer.borderWidth  = 1
        self.layer.borderColor  = UIColor.white.cgColor
    }
}
class  GaryBorderImageView: UIImageView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.masksToBounds = true
        self.layer.borderWidth  = 1
        self.layer.borderColor  = UIColor.colorWithGray(49).cgColor
    }
}

class  BlackBorderView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.masksToBounds = true
        self.layer.borderWidth  = 3
        self.layer.borderColor  = UIColor.black.cgColor
    }
}
class RoundedCornerImageView: UIImageView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
    }
}
class RoundedButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.bounds.size.height/2
        self.layer.masksToBounds = true
        self.imageView?.contentMode  = .scaleToFill
    }
}

class RoundedLabel: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.bounds.size.height/2
        self.layer.masksToBounds = true
    }
}
class RoundedCountLabel: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.bounds.size.height/2
        self.layer.masksToBounds = true
    }
}
class RoundedCornerButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.cornerRadius = 3
        self.layer.masksToBounds = true
    }
}


class DubleLineButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.titleLabel?.numberOfLines = 3
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.titleLabel?.textAlignment = .center
    }
}

class WhiteBorderRoundedCornerButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        self.layer.borderWidth  = 1
        self.layer.borderColor  = UIColor.white.withAlphaComponent(0.5).cgColor
    }
}



class RoundedView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.bounds.size.height/2
        self.layer.masksToBounds = true
    }
}
class RoundedCornerView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.cornerRadius = 3
        self.layer.masksToBounds = true
    }
}

class WhiteBorder10RoundedCornerView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        self.layer.borderWidth  = 1
        self.layer.borderColor  = UIColor.white.cgColor
    }
}
class RoundedCornerInputView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.cornerRadius = 3
        self.layer.masksToBounds = true
        self.layer.borderWidth  = 1
        self.layer.borderColor  = UIColor.inputBorderThemeColor().cgColor
    }
}
class YellowBorderView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.masksToBounds = true
        self.layer.borderWidth  = 1
        self.layer.borderColor  = UIColor.yellowThemeColor().cgColor
    }
}
class WhiteBorderView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.masksToBounds = true
        self.layer.borderWidth  = 1
        self.layer.borderColor  = UIColor.white.cgColor
    }
}
class OrangeBorderView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.masksToBounds = true
        self.layer.borderWidth  = 1
        self.layer.borderColor  = UIColor.orangeThemeColor().cgColor
    }
}

class ShadowRoundCornerView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.borderWidth  = 1
        self.layer.borderColor  = UIColor.white.withAlphaComponent(0.5).cgColor
        self.layer.cornerRadius = 20

        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 5)
        
    }
}
class FullRoundedCornerInputView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.bounds.size.height/2
        self.layer.masksToBounds = true
        self.layer.borderWidth  = 1
        self.layer.borderColor  = UIColor.white.withAlphaComponent(0.3).cgColor
    }
}

class FullRoundedCornerWhiteBorderView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.bounds.size.height/2
        self.layer.masksToBounds = true
        self.layer.borderWidth  = 1
        self.layer.borderColor  = UIColor.white.cgColor
    }
}
class FullRoundedCornerView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.bounds.size.height/2
        self.layer.masksToBounds = true
    }
}

class WhitePlaceHolderTextFiled: UITextField {
    override func awakeFromNib() {
        super.awakeFromNib()
        //        self.tintAdjustmentMode = .Automatic
        self.layoutIfNeeded()
        self.tintColor =  UIColor.white
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes: [NSForegroundColorAttributeName :  UIColor.white])
    }
}

class InputTextFiled: UITextField {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.tintColor =  UIColor.white
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes: [NSForegroundColorAttributeName :  UIColor.white.withAlphaComponent(0.35)])
    }
}

