//
//  AppMessages.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 13/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import Foundation

// MARK: Permission
let kCameraAccessTitle   = "No camera access"
let kCameraAccessMsg     = "Please go to settings and switch on your Camera. Settings -> \(_appName) -> Switch on camera"
let kPhotosAccessTitle   = "No photos access"
let kPhotosAccessMsg     = "Please go to settings and switch on your Photos. Settings -> \(_appName) -> Switch on photos"

let kVideoAccessTitle   = "No video access"
let kVideoAccessMsg     = "Please go to settings and switch on your Videos. Settings -> \(_appName) -> Switch on photos"

let kAudioAccessTitle   = "No Recording Access"
let kAudioAccessMsg     = "Please go to settings and switch on your Microphone. Settings -> \(_appName) -> Switch on microphone"

let kCameraNotAvailable         = "Camera not Available"
let kAllowCameraAccess          = "Please allow us access to your camera"


// MARK: Web Operation
let kNoInternet              = "No internet"
let kInternetDown            = "Your internet connection seems to be down"
let kConnectionLost          = "The network connection was lost"
let kHostDown                = "Your host seems to be down"
let kTimeOut                 = "The request timed out"
let kWrongCredential         = "Wrong Credentials"
let kInternalError           = "Internal Error"
let kUnableSaveAccount       = "Unable to save account"
let kSomethingWentWrong      = "Something went wrong,Please try later"

// MARK: Profile

let kEnterName                  = "Please enter name"
let kEnterEmail                 = "Please enter an email address"
let kValidEmail                 = "Please use a valid email address"
let kEnterMobile                = "Please enter your contact number"
let kValidMobile                = "Please enter a valid contact number"
let kPleaseWait                 = "Please wait"
let kEnterMessage               = "Please enter your message"


// MARK: Contact Us
let kEnterContactSubject           = "Please enter your subject"
let kEnterContactComment           = "Please enter your comment"


// MARK: App Logic base
let kAreYouSure                  = "Are you sure?"
let kImageSave                   = "Photo save successfully"
let kImageUnloaded               = "Photo could not be loaded"

// MARK: Other
let kFailToSaveData              = "Failed to initialize the application's saved data"
let kErrorDomain                 = "com.phoenix.vadtaldham.error"
let kFetchLocation               = "Fetching location"
let kFailToFetchLocation         = "Couldn't fetch location"
let kSomeError                   = "Some error occurred"

let kSettings                    = "Settings"
let kCancel                      = "Cancel"
let kOK                          = "OK"
let kNext                        = "Next"
let kDone                        = "Done"
let kYes                         = "Yes"
let kNo                          = "No"
let kUnderConstruction           = "Under construction"

let kShare_APP_Message            = "Let me recommend you Vadtaldham Darshan https://itunes.apple.com/in/app/vadtaldham-darshan/id1070700979?mt=8"


let Facebook_Page_URL           = "https://www.facebook.com/vadtalmandiro"
let Instagram_Page_URL          = "https://www.instagram.com/vadtalmandir/"
let Youtube_Page_URL            = "https://www.youtube.com/channel/UC2oaqitUiwcbYPnh9zLR50g"




