//
//  WebServices.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 13/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//


import Foundation
import AFNetworking
// MARK:  Base URL
let kDevelopmentURL             = "https://www.vadtalmandir.org/"
let kProductionURL              = "https://www.vadtalmandir.org/"

let kSiteURL                    = kProductionURL
let kBasePath                   = kSiteURL + "webservice/"


// MARK:  Block
typealias WSBlock       = (_ json: Any?, _ flag: Int) -> ()
typealias WSFileBlock   = (_ path: URL, _ success: Bool) -> ()
typealias WSUploadBlock = (_ success: Bool) -> ()

// MARK:-  User Youtube image path get
extension String{
    func fetchYoutubeVideoUrl(_ isDefault:Bool = true) -> String {
        //Default Thumbnail Image, Full-Size (480x360)
        if isDefault{
            return "http://img.youtube.com/vi/" + "\(self)" + "/0.jpg"
        }
        //2nd | Default Thumbnail Image, Small (120x90)
        //3rd Thumbnail Image, Small (120x90)
        //1st Thumbnail Image, Small (120x90)

        return "http://img.youtube.com/vi/" + "\(self)" + "/2.jpg"
    }
}

class WebService: NSObject {
    
    // MARK: Variable
    static var wsCall: WebService = WebService()

    fileprivate let manager: AFHTTPSessionManager;
    fileprivate var flinntSuccessBlock: (String, URLSessionDataTask, Any?,@escaping WSBlock) -> Void
    fileprivate var flinntErrorBlock: (String, URLSessionDataTask?, NSError,@escaping WSBlock) -> Void
    
    
    // MARK: Init
    
    override init() {
        manager = AFHTTPSessionManager(baseURL: URL(string: kBasePath))
        manager.requestSerializer.timeoutInterval = 45.0
        manager.reachabilityManager.setReachabilityStatusChange { (status: AFNetworkReachabilityStatus) -> Void in
            if status == AFNetworkReachabilityStatus.notReachable {
            } else if status == AFNetworkReachabilityStatus.reachableViaWiFi ||
                status == AFNetworkReachabilityStatus.reachableViaWWAN {
                // Internet connection resumed.
            }
        }
        manager.reachabilityManager.startMonitoring()
        
        
        // Will be called on success of web service calls.
        flinntSuccessBlock = { (relativePath, dataTask, respObj, block) -> Void in
            // Check for response it should be there as it had come in success block
            if !WebService.wsCall.isInternetAvailable() {
                block(nil, _offline)
                return
            }
            if let response = dataTask.response as? HTTPURLResponse {
                jprint("Response Code: \(response.statusCode)")
                jprint("Response(\(relativePath)): \(respObj)")
                if response.statusCode == 200 {
                    block(respObj, response.statusCode)
                } else {
                    block(respObj, response.statusCode)
                }
            } else {
                // There might me no case this can get execute
                block(["message": kSomethingWentWrong], 404)
            }
        }
        
        // Will be called on Error during web service call
        flinntErrorBlock = { (relativePath, dataTask, error, block) -> Void in
            // First check for the response if found check code and make decision
            if !WebService.wsCall.isInternetAvailable() {
                block(nil, _offline)
                return
            }
            if let response = dataTask?.response as? HTTPURLResponse {
                jprint("Response Code: \(response.statusCode)")
                jprint("Error Code: \(error.code)")
                if let data = error.userInfo["com.alamofire.serialization.response.error.data"] as? Data {
                    let errorDict = (try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)) as? NSDictionary
                    if errorDict != nil {
                        jprint("Error(\(relativePath)): \(errorDict!)")
                        block(errorDict!, response.statusCode)
                    } else {
                        let code = response.statusCode
                        block(["message": kSomethingWentWrong], code)
                    }
                } else {
                    block(["message": kSomethingWentWrong], response.statusCode)
                }
                // If response not found rely on error code to find the issue
            } else if error.code == -2102  {
                jprint("Error(\(relativePath)): \(error)")
                block(["message": kTimeOut], error.code)
                return
            }else if error.code == -1009  {
                jprint("Error(\(relativePath)): \(error)")
                block(["message": kInternetDown], error.code)
                return
            } else if error.code == -1005  {
                jprint("Error(\(relativePath)): \(error)")
                block(["message": kConnectionLost], error.code)
                return
            } else if error.code == -1003  {
                jprint("Error(\(relativePath)): \(error)")
                block(["message": kHostDown], error.code)
                return
            } else if error.code == -1001  {
                jprint("Error(\(relativePath)): \(error)")
                block(["message": kTimeOut], error.code)
                return
            } else {
                jprint("Error(\(relativePath)): \(error)")
                block(["message": kSomethingWentWrong], error.code)
            }
        }
        
        
        super.init()
    }
    
    func isInternetAvailable() -> Bool {
        let bl = manager.reachabilityManager.networkReachabilityStatus != AFNetworkReachabilityStatus.notReachable
        
        return bl
    }
    
    // This will show error message from given error dictionary
    class func showErrorMessage(_ view: UIView?, fromJson json: Any?) {
        var confirmView: UIView!
        if let aview = view {
            confirmView = aview
        } else if let winView = _appDelegator.window {
            confirmView = winView
        } else {
            jprint("Can't find view to show this message")
            return
        }
        if let jsonDict = json as? NSDictionary,
            let errMsg = jsonDict["Message"] as? String {
            _ = ValidationToast.showBarMessage(errMsg, inView: confirmView)
        } else if let jsonDict = json as? NSDictionary,
            let errMsg = jsonDict["error_description"] as? String  {
            _ = ValidationToast.showBarMessage(errMsg, inView: confirmView)
        } else if let jsonDict = json as? NSDictionary,
            let errMsg = jsonDict[_appName] as? String  {
            _ = ValidationToast.showBarMessage(errMsg, inView: confirmView)
        } else {
            _ = ValidationToast.showBarMessage(kSomeError, inView: confirmView)
        }
    }
    
    // Sign manager with access token
    func addAccesTokenToHeader(_ token: String){
        manager.requestSerializer.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
    }
    
    // unSign manager with access token
    func removeAccessTokenFromHeader() {
        manager.requestSerializer.setValue(nil, forHTTPHeaderField: "Authorization")
    }
    
}
// MARK: - Private Methods
extension WebService {
    
    fileprivate func postRequest(_ relativePath: String, param: Any?, block: @escaping WSBlock) -> URLSessionDataTask? {
        jprint("RelativePath: \(relativePath)")
        jprint("Parameter: \(param)")
    
        return manager.post(relativePath,
                            parameters: param,
                            progress: nil,
                            success: { (task, resObj) -> Void in
                                self.flinntSuccessBlock(relativePath,task,resObj,block)
        }) { (task, error) -> Void in
            self.flinntErrorBlock(relativePath,task,error as NSError,block)
        }
    }
    
    fileprivate func deleteRequest(_ relativePath: String, param: Any?, block: @escaping WSBlock) -> URLSessionDataTask?{
        jprint("RelativePath: \(relativePath)")
        jprint("Parameter: \(param)")
        return manager.delete(relativePath,
                              parameters:
            param, success: { (task, resObj) -> Void in
                self.flinntSuccessBlock(relativePath,task,resObj,block)
        }) { (task, error) -> Void in
            self.flinntErrorBlock(relativePath,task,error as NSError,block)
        }
    }
    
    func getRequest(_ relativePath: String, param: Any?, block: @escaping WSBlock) -> URLSessionDataTask? {
        jprint("RelativePath: \(relativePath)")
        jprint("Parameter: \(param)")
      
        return manager.get(relativePath,
                           parameters: param,
                           progress: nil,
                           success: { (task, resObj) -> Void in
                            self.flinntSuccessBlock(relativePath,task,resObj,block)
        }) { (task, error) -> Void in
            self.flinntErrorBlock(relativePath,task,error as NSError,block)
        }
    }
    
    fileprivate func uploadImage(_ relativePath: String, progress: ((Progress) -> ())?, imageData: Data?,imageName: String, param: Any?, block: @escaping WSBlock) -> URLSessionDataTask?{
        jprint("RelativePath: \(relativePath)")
        jprint("Parameter: \(param)")
      
        return  manager.post(relativePath, parameters: param, constructingBodyWith: { (multipart) -> Void in
            if let imgDt = imageData {
                multipart.appendPart(withFileData: imgDt, name: imageName, fileName: "image.jpeg", mimeType: "image/jpeg")
            }
            }, progress: progress, success: { (task, resObj) -> Void in
                self.flinntSuccessBlock(relativePath,task,resObj,block)
        }) { (task, error) -> Void in
            self.flinntErrorBlock(relativePath,task,error as NSError,block)
        }
    }
    
    fileprivate func uploadFile(_ relativePath: String, progress: ((Progress) -> ())?, fileUrl: URL, param: Any?, block: @escaping WSBlock) -> URLSessionDataTask?{
        jprint("RelativePath: \(relativePath)")
        jprint("Parameter: \(param)")
       
        return manager.post(relativePath, parameters: param, constructingBodyWith: { (multipart) -> Void in
            do { try multipart.appendPart(withFileURL: fileUrl, name: "upload_file")
            } catch let error as NSError { jprint("Error got: \(error.localizedDescription)") }
            }, progress: progress, success: { (task, resObj) -> Void in
                self.flinntSuccessBlock(relativePath,task,resObj,block)
        }) { (task, error) -> Void in
            self.flinntErrorBlock(relativePath,task,error as NSError,block)
        }
    }
    
    fileprivate func downloadFile(_ url: URL, progress: ((Progress) -> ())? ,block: @escaping WSFileBlock){
       
        let cachUrl = DocumentAccess.obj.documentUrlForName(url.lastPathComponent)
        let path = cachUrl.path
        if FileManager.default.fileExists(atPath: path) {
            block(cachUrl, true)
            return
        }
        let task = manager.downloadTask(with: URLRequest(url: url),
                                                   progress: progress,
                                                   destination: { (url, response) -> URL in
                                                    return cachUrl
        }) { (response, url, error) -> Void in
            if let err = error {
                jprint("Error occure while downloading file from: \(url!.absoluteString) \n error: \(err.localizedDescription)")
            }else{
                jprint("File downloaded to: \(path)")
            }
            if let _ = url{
                block(url!, error == nil)
            }else{
                block(cachUrl, error == nil)
            }
        }
        
        
        task.resume()
    }
    
}

//MARK:- API List

extension WebService{
     //MARK:- Feedback
    func addFeedback(_ param: Any, block: @escaping WSBlock){
        jprint("-------------- Add Feedback ------------------")
        let relPath = "?mode=setFeedback"
        _ = postRequest(relPath, param: param, block: block)
    }
    
     //MARK:- Notification
    func getNotification(_ param: NSDictionary, block: @escaping WSBlock){
        jprint("-------------- Get Notification ------------------")
        let relPath = "?mode=getNotification"
        _ =  postRequest(relPath, param: param, block: block)
    }
    func sendDeviceToken(_ token: String, block: @escaping WSBlock){
        jprint("-------------- Save Device Token ------------------")
        let relPath = "ios.php?mode=saveData"
        var param = [String: String]()
        param["regid_ios"] = token
        _ =  postRequest(relPath, param: param, block: block)
    }
    
     //MARK:- Dail Darshan
    func getDailyDarshan(_ date:String,isShangar:Bool = true, block: @escaping WSBlock){
        jprint("-------------- Get Daily Darshan \(isShangar ? "Shangar" : "Shayan")------------------")
        let relPath = "?mode=getDailyDarshan"
        
        var param = [String: String]()
        param["date"] = date
        param["type"] = (isShangar == true) ? "Shangar" : "Shayan"


        _ = postRequest(relPath, param: param as NSDictionary?, block: block)
    }
    
    
     //MARK:- Photo Gallery
    func getPhotoYearList(_ block: @escaping WSBlock){
        jprint("-------------- Get Photo Year------------------")
        let relPath = "?mode=getPhotoYear"
        _ = getRequest(relPath, param: nil, block: block)
    }
    
    func getPhotoYearEventList(_ year: String, page: String, block: @escaping WSBlock){
        jprint("-------------- Get Photo Year Event List------------------")
        let relPath = "?mode=getPGByear"
        var param = [String: String]()
        param["year"] = year
        param["page"] = page
     _ = getRequest(relPath, param: param as NSDictionary?, block: block)
    }
    
    func getEventSubPhotosList(_ code: String, block: @escaping WSBlock){
        jprint("-------------- Get Event Sub Photos List------------------")
        let relPath = "?mode=getSubPG"
        var param = [String: String]()
        param["event_code"] = code
        _ = getRequest(relPath, param: param as NSDictionary?, block: block)
    }
    
    func getEventSubPhotosDetail(_ code: String, block: @escaping WSBlock){
        jprint("-------------- Get Event Sub Photos detail ------------------")
        let relPath = "?mode=getPhotos"
        var param = [String: String]()
        param["event_dt_code"] = code
        _ = getRequest(relPath, param: param as NSDictionary?, block: block)
    }
    
     //MARK:- Video Gallery
    func getVideoYearList(_ block: @escaping WSBlock){
        jprint("-------------- Get Video Year------------------")
        let relPath = "?mode=getVideoYear"
        _ = getRequest(relPath, param: nil, block: block)
    }
    
    func getVideoYearAlbumList(_ year: String, block: @escaping WSBlock){
        jprint("-------------- Get Video Year Album List------------------")
        let relPath = "?mode=getVByear"
        var param = [String: String]()
        param["year"] = year
        _ = getRequest(relPath, param: param as NSDictionary?, block: block)
    }
    
    func getVideoAlbumDetail(_ videoID: String, page: String, block: @escaping WSBlock){
        jprint("-------------- Get Video Album Detail------------------")
        let relPath = "?mode=getVideo"
        var param = [String: String]()
        param["video_id"] = videoID
        param["page"] = page
        _ = getRequest(relPath, param: param as NSDictionary?, block: block)
    }
    
     //MARK:- Audio Gallery
    
    func getAudioAlbumList(_ block: @escaping WSBlock){
        jprint("-------------- Get Audio Album List------------------")
        let relPath = "?mode=getAudioAlbum"
        _ = getRequest(relPath, param: nil, block: block)
    }
    
    func getAudioAlbumDetail(_ code: String, block: @escaping WSBlock){
        jprint("-------------- Get Audio Album Detail------------------")
        let relPath = "?mode=getAudio"
        var param = [String: String]()
        param["albam_code"] = code
        _ = getRequest(relPath, param: param as NSDictionary?, block: block)
    }
    
    //MARK:- Achary Photo Gallery
    func getAcharyPhotoYearList(_ block: @escaping WSBlock){
        jprint("-------------- Get Acharya Photo Year------------------")
        let relPath = "?mode=getacharyadeskYear"
        _ = getRequest(relPath, param: nil, block: block)
    }
    
    func getAcharyPhotoEventList(_ year: String, block: @escaping WSBlock){
        jprint("-------------- Get Acharya Photo Year Event List------------------")
        let relPath = "?mode=getacharyadeskgallery"
        var param = [String: String]()
        param["year"] = year
       _ =  getRequest(relPath, param: param as NSDictionary?, block: block)
    }
    
    func getAcharyEventPhotos(_ code: String, block: @escaping WSBlock){
        jprint("-------------- Get Acharya Event Photos detail ------------------")
        let relPath = "?mode=getacharyadeskphoto"
        var param = [String: String]()
        param["event_code"] = code
        _ = getRequest(relPath, param: param as NSDictionary?, block: block)
    }
    
    //MARK:- Achary Video Gallery
    func getAcharyVideoYearList(_ block: @escaping WSBlock){
        jprint("-------------- Get Acharya Video Year------------------")
        let relPath = "?mode=getacharyadeskvideoYear"
        _ = getRequest(relPath, param: nil, block: block)
    }
  
    func getAcharyVideoAlbumDetail(_ year: String, block: @escaping WSBlock){
        jprint("-------------- Get Acharya Video Album Detail------------------")
        let relPath = "?mode=getacharyadeskvideos"
        var param = [String: String]()
        param["year"] = year
        _ = getRequest(relPath, param: param as NSDictionary?, block: block)
    }

    
     //MARK:- Nirnay
    func getNirnayList(_ month: String, year: String, block: @escaping WSBlock){
        jprint("-------------- Get Nirnay List------------------")
        let relPath = "?mode=getnirnaycal"
        var param = [String: String]()
        param["month"] = month
        param["year"] = year
        param["language"] = _userSelectedLanguage.getWebAPIPrefix()

       _ =  getRequest(relPath, param: param as NSDictionary?, block: block)

    }
     //MARK:- Mahotsav
    func getMahotsavAlbumList(_ block: @escaping WSBlock){
        jprint("-------------- Get Mahotsav Album List------------------")
        let relPath = "?mode=getMahotsavAlbum"
        _ = getRequest(relPath, param: nil, block: block)
    }
    
    func getMahotsavAlbumDetail(_ code: String, block: @escaping WSBlock){
        jprint("-------------- Get Mahotsav Album Detail------------------")
        let relPath = "?mode=getMimg"
        var param = [String: String]()
        param["mahotsav_code"] = code

       _ =  getRequest(relPath, param: param as NSDictionary?, block: block)
    }
    
     //MARK:- Museum
    func getMuseumList(_ block: @escaping WSBlock){
        jprint("-------------- Get Museum List------------------")
        let relPath = "https://vadtalmandir.org/admin/services/museum_service"
       _ =  getRequest(relPath, param: nil, block: block)
    }
    
    //MARK:- E-Magazine
    func getMagazineYearList(_ block: @escaping WSBlock){
        jprint("-------------- Get E-Magazine Year------------------")
        let relPath = "?mode=getemagazineYear"
        _ = getRequest(relPath, param: nil, block: block)
    }

    func getMagazineYeaAlbumList(_ year: String, block: @escaping WSBlock){
        jprint("-------------- Get Magazine Year ALbum List------------------")
        let relPath = "?mode=getemagazine"
        var param = [String: String]()
        param["year"] = year
        _ = getRequest(relPath, param: param, block: block)
    }
    
    func downloadMagazine(_ url:URL,progress: ((Progress) -> ())? , block: @escaping WSFileBlock){
        jprint("-------------- Download Magazine------------------")
        _ = downloadFile(url, progress: progress, block: block)
    }
    
    //MARK:- E-Library
    func getLibraryList(_ block: @escaping WSBlock){
        jprint("-------------- Get E-Magazine Year------------------")
        let relPath = "?mode=getebook"
        var param = [String: String]()
        param["language"] = _userSelectedLanguage.getWebAPIPrefix(false)

        _ = getRequest(relPath, param: param, block: block)
    }

    func downloadLibrary(_ url:URL,progress: ((Progress) -> ())? , block: @escaping WSFileBlock){
        jprint("-------------- Download Library------------------")
        _ = downloadFile(url, progress: progress, block: block)
    }
    
    

}




