//
//  NSNumberExtension.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 13/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//


import UIKit


extension Double {
    init(string value: String) {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 1
        var separator = ""
        
        if let safeSeparator = formatter.decimalSeparator {
            separator = safeSeparator
        }
        var safeValue = value
        if separator == "," {
            safeValue =  value.replacingOccurrences(of: ".", with: ",", options: NSString.CompareOptions.literal, range: nil)
        } else {
            safeValue = value.replacingOccurrences(of: ",", with: ".", options: NSString.CompareOptions.literal, range: nil)
        }
        
        if safeValue.isEmpty {
            safeValue = "0"
        }
        
        let number = formatter.number(from: safeValue)
        if let double = number?.doubleValue {
            self.init(double)
        } else {
            self.init(0)
        }
    }
    
    func stringWithDoubleFormat(_ value : String,numberStyle :NumberFormatter.Style! = NumberFormatter.Style.none) -> Double {
        let formatter = NumberFormatter()
        formatter.numberStyle = numberStyle
        formatter.maximumFractionDigits = 16

        var separator = ""
        
        if let safeSeparator = formatter.decimalSeparator {
            separator = safeSeparator
        }
        var safeValue = value
        if separator == "," {
            safeValue =  value.replacingOccurrences(of: ".", with: ",", options: NSString.CompareOptions.literal, range: nil)
        } else {
            safeValue = value.replacingOccurrences(of: ",", with: ".", options: NSString.CompareOptions.literal, range: nil)
        }
        
        if safeValue.isEmpty {
            safeValue = "0"
        }
        
        let number = formatter.number(from: safeValue)
        if let double = number?.doubleValue {
            return double
        } else {
            return 0
        }
    }
    func stringWithStringFormat(_ value : String) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        var separator = ""
        
        if let safeSeparator = formatter.decimalSeparator {
            separator = safeSeparator
        }
        var safeValue = value
        if separator == "," {
            safeValue =  value.replacingOccurrences(of: ".", with: ",", options: NSString.CompareOptions.literal, range: nil)
        } else {
            safeValue = value.replacingOccurrences(of: ",", with: ".", options: NSString.CompareOptions.literal, range: nil)
        }
        
        if safeValue.isEmpty {
            return ""
        }
        
        let number = formatter.number(from: safeValue)
        if let str = formatter.string(from: number!) {
            return str
        } else {
            return ""
        }

    }
    func stringWithStringEngFormat(_ value : String) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 1
        var separator = ""
        
        if let safeSeparator = formatter.decimalSeparator {
            separator = safeSeparator
        }
        var safeValue = value
        if separator == "," {
            safeValue =  value.replacingOccurrences(of: ".", with: ",", options: NSString.CompareOptions.literal, range: nil)
        } else {
            safeValue = value.replacingOccurrences(of: ",", with: ".", options: NSString.CompareOptions.literal, range: nil)
        }
        
        if safeValue.isEmpty {
            return ""
        }
        
        let number = formatter.number(from: safeValue)
        if let double = number?.doubleValue {
            return String(format: "%.2f",double)
        } else {
            return ""
        }
        
    }
}
