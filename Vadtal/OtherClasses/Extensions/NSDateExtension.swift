//
//  NSDateExtension.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 13/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//


import Foundation
import UIKit

extension Date
{
    
    fileprivate struct Date {
        
        static let formatter1 :DateFormatter = {
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en")
            
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            formatter.dateFormat = "dd-MM-yyyy"
            return formatter
        }()
        
        static let formatter2 :DateFormatter = {
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en")
            
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            return formatter
        }()
        
        
        
        static let formatter3 :DateFormatter = {
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en")
            formatter.dateFormat = "MMMM"
            return formatter
        }()
        
        static let formatter4 :DateFormatter = {
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en")
            formatter.dateFormat = "MM"
            return formatter
        }()
        
        static let formatter5 :DateFormatter = {
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en")
            formatter.dateFormat = "yyyy"
            return formatter
        }()
        
        static let formatter6 :DateFormatter = {
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en")
            
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            formatter.dateFormat = "dd-MM-yyyy"
            return formatter
        }()
        
        static let formatter7 :DateFormatter = {
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en")
            formatter.dateFormat = "dd MMMM, yyyy"
            return formatter
        }()
        static let formatter1_1 :DateFormatter = {
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en")
            formatter.dateFormat = "dd-MM-yyyy"
            return formatter
        }()
    }
    
    var formatted1: String {
        return Date.formatter1.string(from: self)
    }
    var formatted1_1: String {
        return Date.formatter1_1.string(from: self)
    }
    var formatted2: String {
        return Date.formatter2.string(from: self)
    }
    var formatted3: String {
        return Date.formatter3.string(from: self)
    }
    
    var formatted4: String {
        return Date.formatter4.string(from: self)
    }
    var formatted5: String {
        return Date.formatter5.string(from: self)
    }
    
    var formatted6: String {
        return Date.formatter6.string(from: self)
    }
    var formatted7: String {
        return Date.formatter7.string(from: self)
    }
    static func dateFromFormatted1_String (_ dateString: String) -> Foundation.Date? {
        let dateFromString = Date.formatter1.date(from: dateString)
        return dateFromString
    }
    
    static func dateFromFormatted2_String (_ dateString: String) -> Foundation.Date? {
        let dateFromString = Date.formatter2.date(from: dateString)
        return dateFromString
    }
    
    
    func calculateAge () -> Int {
        return (Calendar.current as NSCalendar).components(NSCalendar.Unit.year, from: self, to: Foundation.Date(), options: []).year!
    }
    func dateWithStringFormat(_ format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en")
        return formatter.string(from: self)
    }
    func localDateWithStringFormat(_ format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = Locale(identifier: "en")
        formatter.timeZone = TimeZone.current
        
        return formatter.string(from: self)
    }
    func localDateWithServerTimeZoneFormat(_ format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en")
        return formatter.string(from: self)
    }
    func stringWithDateFormat(_ format: String, fromDate date: String) -> Foundation.Date {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        return formatter.date(from: date)!
    }
    
    func getOnlyIntervalSinceDate(_ date: String) -> Foundation.Date{
        let timeinterval : TimeInterval = (date as NSString).doubleValue // convert it in to NSTimeInteral
        let dateFromServer = Foundation.Date(timeIntervalSince1970:timeinterval)
        
        return dateFromServer
    }
    func changeDateFormateWithGivenDate(_ format: String, format1: String, fromDate date: String) -> String {
        if date.isEmpty{
            return ""
        }
        let formatter1 = DateFormatter()
        formatter1.dateFormat = format
        formatter1.timeZone = TimeZone(secondsFromGMT: 0)
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = format1
        formatter2.locale = Locale(identifier: "en")
        formatter2.timeZone = TimeZone.current
        
        return formatter2.string(from: formatter1.date(from: date)!)
    }
    func separateDayMonthYear(_ block : (_ day: String,_ month: String, _ year: String) -> () ){
        let day =  self.localDateWithStringFormat("dd") //"dd"
        let month =  self.localDateWithStringFormat("MM") //"MM"
        let year =  self.localDateWithStringFormat("yyyy") //"yyyy"
        block(day, month, year)
    }
    func befor1dayDate() -> Foundation.Date{
        let date = (Calendar.current as NSCalendar).date(
            byAdding: NSCalendar.Unit.day,
            value: -1,
            to: self,
            options: NSCalendar.Options.searchBackwards)
        return date!
    }
    func After1dayDate() -> Foundation.Date{
        let date = (Calendar.current as NSCalendar).date(
            byAdding: NSCalendar.Unit.day,
            value: 1,
            to: self,
            options: NSCalendar.Options.wrapComponents)
        return date!
    }
    func get1MonthPreviousDate() -> Foundation.Date{
        let date = (Calendar.current as NSCalendar).date(
            byAdding: NSCalendar.Unit.month,
            value: -1,
            to: self,
            options: NSCalendar.Options.wrapComponents)
        return date!
    }
    func get1MonthNextDate() -> Foundation.Date{
        let date = (Calendar.current as NSCalendar).date(
            byAdding: NSCalendar.Unit.month,
            value: 1,
            to: self,
            options: NSCalendar.Options.wrapComponents)
        return date!
    }
    
    func commpareWithCurrentDate() -> ComparisonResult{
        let date =  self.stringWithDateFormat("dd-MM-yyyy", fromDate: self.dateWithStringFormat("dd-MM-yyyy"))
        let date1 =  Foundation.Date().stringWithDateFormat("dd-MM-yyyy", fromDate: Foundation.Date().dateWithStringFormat("dd-MM-yyyy"))

        return date.compare(date1)
    }
}
