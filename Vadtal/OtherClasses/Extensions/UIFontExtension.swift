//
//  UIFontExtension.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 13/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//


import Foundation
import UIKit


enum FontBook: String {
    //MARK:- Avenir Font
    case Avenir_Book         = "Avenir-Book"
    case Avenir_Black        = "Avenir-Black"
    case Avenir_Medium       = "Avenir-Medium"
    case Avenir_Heavy        = "Avenir-Heavy"
    case Avenir_Light        = "Avenir-Light"
 
    //MARK:- AvenirNext Font
    case AvenirNext_Medium            = "AvenirNext-Medium"
    case AvenirNext_Heavy             = "AvenirNext-Heavy"
    case AvenirNext_UltraLight        = "AvenirNext-UltraLight"
    
    //MARK:- HelveticaNeue Font

    case HelveticaNeue_Light                    = "HelveticaNeue-Light"
    case HelveticaNeue_Regular                  = "HelveticaNeue"
    
    //MARK:- TrajanPro Font
    
    case TrajanPro_Regular                      = "TrajanPro-Regular"
    case TrajanPro_Bold                         = "TrajanPro-Bold"
    
    //MARK:- Gotham Font
    case Gotham_Regular                         = "Gotham-Black"
    case Gotham_Regular_Italic                  = "Gotham-BlackItalic"
    case Gotham_Bold                            = "Gotham-Bold"
    case Gotham_Bold_Italic                     = "Gotham-BoldItalic"
    case Gotham_Book                            = "Gotham-Book"
    case Gotham_Book_Italic                     = "Gotham-BookItalic"
    case Gotham_Light                           = "Gotham-Light"
    case Gotham_Light_Italic                    = "Gotham-LightItalic"
    case Gotham_Medium                          = "Gotham-Medium"
    case Gotham_Medium_Italic                   = "Gotham-MediumItalic"

    //MARK:- Method

    func of(_ size: CGFloat) -> UIFont {
        return UIFont(name: self.rawValue, size: size)!
    }
}

