//
//  ConversionExtensions.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 13/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//


import Foundation
import UIKit

// MARK: - Conversion
extension Double{
    func getFormattedValue(_ str: String)->String?{
        return String(format: "%.\(str)f", self)
    }
}

extension CGFloat{
    
    var intValue: NSInteger?{
        let val = Int(self)
        return val
    }
    
    func getFormattedValue(_ str: String)->String?{
        return String(format: "%.\(str)f", self)
    }
}
// MARK: - Conversion

extension String {
    var doubleValue: Double? {
        var safeValue = self
        
        if safeValue.isEmpty {
            safeValue = "0"
        }
        return Double(safeValue)
    }
    var floatValue: Float? {
        var safeValue = self
        
        if safeValue.isEmpty {
            safeValue = "0"
        }
        return Float(safeValue)
    }
    var integerValue: Int? {
        var safeValue = self
        
        if safeValue.isEmpty {
            safeValue = "0"
        }
        return Int(safeValue)
    }
}

