//
//  UIColorExtension.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 13/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//


import Foundation
import UIKit

extension UIColor {
    
    class func colorWithGray(_ gray: Int) -> UIColor {
        return UIColor(red: CGFloat(gray) / 255.0, green: CGFloat(gray) / 255.0, blue: CGFloat(gray) / 255.0, alpha: 1.0)
    }
    class func colorWithRGB(r: Int, g: Int, b: Int) -> UIColor {
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: 1.0)
    }
    
    // Color of Validation Fail/Success
    class func popupFailColor() -> UIColor {
        return UIColor(red: 248.0 / 255.0, green: 73.0 / 255.0, blue: 73.0 / 255.0, alpha: 1.0)
    }
    class func popupWarningColor() -> UIColor {
        return UIColor(red: 255.0 / 255.0, green: 165.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0)
    }
    class func popupSuccessColor() -> UIColor {
        return UIColor(red: 103.0 / 255.0, green: 193.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0)
    }
   
    class func randomColor() -> UIColor {
        let r = CGFloat.random()
        let g = CGFloat.random()
        let b = CGFloat.random()
        
        // If you wanted a random alpha, just create another
        // random number for that too.
        return UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
    
    // App
    class func inputBorderThemeColor() -> UIColor {
        return UIColor(red: 186.0 / 255.0, green: 198.0 / 255.0, blue: 198.0 / 255.0, alpha: 1.0)
    }
    class func inputBorderGaryThemeColor() -> UIColor {
        return UIColor(red: 211.0 / 255.0, green: 211.0 / 255.0, blue: 211.0 / 255.0, alpha: 1.0)
    }
    class func favoriteItemBorderColor() -> UIColor {
        return UIColor(red: 190.0 / 255.0, green: 185.0 / 255.0, blue: 164.0 / 255.0, alpha: 1.0)
    }
    
    class func redThemeColor() -> UIColor {
        return UIColor(red: 165.0 / 255.0, green: 43.0 / 255.0, blue: 47.0 / 255.0, alpha: 1.0)
    }
    
    class func yellowThemeColor() -> UIColor {
        return UIColor(red: 255.0 / 255.0, green: 199.0 / 255.0, blue: 17.0 / 255.0, alpha: 1.0)
    }
    class func orangeThemeColor() -> UIColor {
        return UIColor(red: 233.0 / 255.0, green: 113.0 / 255.0, blue: 17.0 / 255.0, alpha: 1.0)
    }
    
    
    class func calenderEkasdshiBGColor() -> UIColor {
        return UIColor(red: 56.0 / 255.0, green: 244.0 / 255.0, blue: 198.0 / 255.0, alpha: 1.0)
    }
    
    class func calenderPoonamBGColor() -> UIColor {
        return UIColor(red: 255.0 / 255.0, green: 51.0 / 255.0, blue: 17.0 / 54.0, alpha: 1.0)
    }
    
    
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}
