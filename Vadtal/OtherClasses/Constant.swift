//
//  Constant.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 13/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//


import Foundation
import UIKit


let _screenSize         = UIScreen.main.bounds.size
let _appName            = "Vadtaldham Darshan"

let _defaultCenter  = NotificationCenter.default
let _userDefault    = UserDefaults.standard
let _appDelegator   = UIApplication.shared.delegate! as! AppDelegate
let _application    = UIApplication.shared

//var _userInfo: User! = nil
var  _userSelectedLanguage = AppLanguage.english


let _heighRatio     = _screenSize.height/736
let _widthRatio     = _screenSize.width/414

let _PwdMinLimit = 6

let _mobileMinLimit  = 10
let _mobileMaxLimit  = 14

let _offline = -0

// MARK: Media Placehodler
let media_Placeholder =  UIImage(named: "media_placeholder")

// MARK: User Default keus
let kUserDeviceTokenKey            = "kUserDeviceTokenKey"

// typealias _comQulRow = 0.5 as Float
// Image compression quality while converting to data
let _compQul80P    = CGFloat(0.2)
let _compQul60P    = CGFloat(0.4)
let _compQul50P    = CGFloat(0.5)
let _compQul20p    = CGFloat(0.8)
let _compQulZeroP  = CGFloat(1)
let _compQul100P   = CGFloat(0)


let kActivityCenterImageName = "spinnerIcon_black"
let kActivityButtonImageName = "spinnerIcon_black"
let kActivityTableImageName  = "spinnerIcon_black"

let kActivityCenterBigImageName = "spinnerIcon_black"
let kActivityButtonBigImageName = "spinnerIcon_black"
let kActivityTableBigImageName  = "spinnerIcon_black"

let _dobFormat          = "dd-MM-yyyy"
let _serverDobFormat    = "yyyy-MM-dd"

// MARK: Important Enums
enum UIUserInterfaceIdiom : Int {
    case unspecified
    case phone
    case pad
}
// MARK: Global Functions
// Comment in release mode
func jprint(_ items: Any...) {
    for item in items {
        print(item)
    }
}

/**
 Convert any object to Json formate string if any catch then empty string return 
 */
func toJsonString(_ parm : AnyObject) -> String{
    do{
        let jsonData: Data = try JSONSerialization.data(withJSONObject: parm, options: JSONSerialization.WritingOptions.prettyPrinted)
        if let datastring = String(data: jsonData, encoding: String.Encoding.utf8){
            return datastring
        }
    } catch {
        return  ""
    }
    return  ""
}


