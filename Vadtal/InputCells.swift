//
//  InputCells.swift
//  AussieFood
//
//  Created by Yudiz Solutions Pvt.Ltd. on 07/09/16.
//  Copyright © 2016 Yudiz Solutions Pvt.Ltd. All rights reserved.
//

import Foundation
import UIKit

//MARK:- TextFiledInput Cell
class TextFiledInputCell: ConstrainedTableViewCell {
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var txtf: UITextField!
    @IBOutlet var button: UIButton!
    @IBOutlet var imgInputIcon: UIImageView!
    @IBOutlet var imgInputFocus: UIImageView!
   
    var fType = FormType.feedback

    weak var parentFeedback: FeedbackVC!

    var actionBlock : ((_ tapped: Tapped,_ textField:UITextField?,_ button:UIButton?) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
  
    func prepareUI(_ filed: UserField,type: FormType,tag : Int) {
       fType = type
        self.tag = tag
        if imgInputIcon != nil{
            imgInputIcon.image  = UIImage(named: filed.image)
        }
        if lblTitle != nil{
            lblTitle.text = filed.title
        }
        txtf.setAttributedPlaceholder(filed.placeHolder, font: txtf.font!, color: UIColor.white)
        txtf.text           = filed.str
        txtf.tag            = tag
        txtf.clearButtonMode = .whileEditing
        txtf.isSecureTextEntry = false
        txtf.keyboardType    = .default
        txtf.returnKeyType   = .next
        txtf.autocapitalizationType = .none
        switch type {
        case .feedback:
            if tag == 1 {
                txtf.keyboardType = .numberPad
            }else if tag == 2 {
                txtf.keyboardType = .emailAddress
            }
            break
            
        default:
            break
        }
        
    }
    // MARK: Method
    func handleBlockAction(_ block: @escaping (_ tapped: Tapped,_ textField:UITextField?,_ button:UIButton?) -> ()){
        actionBlock = block
    }
    
    // MARK: TextField Change Actions
    @IBAction func txtfDidChange(_ sender: UITextField!) {
        switch fType {
        case .feedback:
            parentFeedback.fieldData.allFileds[self.tag].str = sender.text!
            break

        default:
            break
        }

        actionBlock?(.valueChange, sender,nil)
    }
    
    @IBAction func txtfDidEndChange(_ sender: UITextField!) {
        switch fType {
        case .feedback:
            parentFeedback.fieldData.allFileds[self.tag].str = sender.text!
            break
            
        default:
            break
        }
        actionBlock?(.valueChange, sender,nil)
    }
    
    
    // MARK: Button Actions
    @IBAction func buttonTapped(_ sender: UIButton!) {
        actionBlock?(.action, nil,sender)
    }
    
    
}
//MARK:- TextViewInput Cell

class TextViewInputCell: ConstrainedTableViewCell {
    
    @IBOutlet var lblHolder: UILabel!
    @IBOutlet var txtv: UITextView!
    @IBOutlet var button: UIButton!
    @IBOutlet var imgInputIcon: UIImageView!

    
    var fType = FormType.feedback
    
    weak var parentFeedback: FeedbackVC!

    
    var actionBlock : ((_ tapped: Tapped,_ textView:UITextView?,_ button:UIButton?) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        _defaultCenter.addObserver(self, selector: #selector(TextViewInputCell.textViewTextDidBeginEditing(_:)), name: NSNotification.Name.UITextViewTextDidBeginEditing, object: nil)
        _defaultCenter.addObserver(self, selector: #selector(TextViewInputCell.textViewTextDidChange(_:)), name: NSNotification.Name.UITextViewTextDidChange, object: nil)
        _defaultCenter.addObserver(self, selector: #selector(TextViewInputCell.textViewTextDidEndChange(_:)), name: NSNotification.Name.UITextViewTextDidEndEditing, object: nil)
//        UITextViewTextDidBeginEditingNotification
    }
    
    deinit {
        _defaultCenter.removeObserver(self)
    }

    
    func prepareUI(_ filed: UserField,type: FormType,tag : Int) {
        fType = type
        if !filed.image.isEmpty{
            imgInputIcon.image  = UIImage(named: filed.image)
        }
        lblHolder.text      = filed.placeHolder
        txtv.text           = filed.str
        txtv.tag            = tag
        txtv.isSecureTextEntry = false
        txtv.keyboardType    = .default
        txtv.returnKeyType   = .default
        txtv.autocapitalizationType = .none
        
        lblHolder.isHidden = filed.str.characters.count > 0


    }
    // MARK: Method
    func handleBlockAction(_ block: @escaping (_ tapped: Tapped,_ textView:UITextView?,_ button:UIButton?) -> ()){
        actionBlock = block
    }
    
    // MARK: UITextView Change Notification
    func textViewTextDidBeginEditing(_ notification: Foundation.Notification!){
        if let textv = notification.object as? UITextView{
            switch fType {
            case .feedback:
                parentFeedback.fieldData.allFileds[textv.tag].str = textv.text!
                lblHolder.isHidden = textv.text!.characters.count > 0
                break
            default:
                break
            }
            
            actionBlock?(.valueChange, textv,nil)
        }

    }
    func textViewTextDidChange(_ notification: Foundation.Notification!) {
        if let textv = notification.object as? UITextView{
            switch fType {
            case .feedback:
                parentFeedback.fieldData.allFileds[textv.tag].str = textv.text!
                lblHolder.isHidden = textv.text!.characters.count > 0

                break
            default:
                break
            }
            
            actionBlock?(.valueChange, textv,nil)
        }
    }
    func textViewTextDidEndChange(_ notification: Foundation.Notification!) {
        if let textv = notification.object as? UITextView{
            switch fType {
            case .feedback:
                parentFeedback.fieldData.allFileds[textv.tag].str = textv.text!
                lblHolder.isHidden = textv.text!.characters.count > 0
                break
            default:
                break
            }
            actionBlock?(.valueChange, textv,nil)
            
        }
    }
  
    // MARK: Button Actions
    @IBAction func buttonTapped(_ sender: UIButton!) {
        actionBlock?(.action, nil,sender)
    }
    
    
}


class ButtonCell: ConstrainedTableViewCell {
    @IBOutlet var btns:  [UIButton]!
    @IBOutlet var viewBtnContainers: [UIView]!
    
    var actionBlock : ((_ tapped: Tapped, _ button:UIButton) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: Method
    func handleBlockAction(_ block: @escaping (_ tapped: Tapped,_ button:UIButton) -> ()){
        actionBlock = block
    }
    
    
    // MARK: Button Actions
    @IBAction func btnTapped(_ sender: UIButton!) {
        actionBlock?(Tapped.action, sender)
    }
}
