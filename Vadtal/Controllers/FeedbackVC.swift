//
//  FeedbackVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 15/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class FeedbackVC: ParentVC {
    
    //MARK:- Outlet
    let cellIdentifiers :[String] = ["TitleCell","TxtFCell","TxtFCell","TxtFCell","TxtVCell","BtnCell"]
    
    let fieldData = UserDataSpecifier(type: FormType.feedback)
    
    //MARK:- Variables
    
    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK:- Private
extension FeedbackVC{
    func prepareUI(){
        self.tableView.contentInset = UIEdgeInsetsMake(15, 0, 15, 0)
        self.tableView.rowHeight            = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight   = 40
        self.tableView.keyboardDismissMode  = .onDrag
        setKeyboardNotifications()
    }
}
//MARK:- TextField Delegate
extension FeedbackVC{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        fieldData.allFileds[textField.tag].preStype = .active
        
        switch textField.tag {
            
        case 1:
            toolBar.btnOK.setTitle(kNext, for: UIControlState())
            textField.inputAccessoryView = toolBar
            toolBar.handleTappedAction({ (tapped, alert) in
                if tapped == .ok{
//                    self.scrollToIndex(textField.tag + 2)
                    let cell = self.tableTextFiledInputCell(textField.tag + 2)
                    if let _ = cell {
                        cell!.txtf.becomeFirstResponder()
                    }else{
                        self.view.endEditing(true)
                    }
                }
            })
            break
        default:
            break
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        fieldData.allFileds[textField.tag].preStype = .normal
    }
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == " " && range.location == 0{
            return false
        }
        
        if string == " " && textField.tag == 2{
            return false
        }
        switch textField.tag {
        case 1:
            let currentText = textField.text ?? ""
            let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
            if prospectiveText.characters.count > _mobileMaxLimit {
                return false
            }
            let aSet = CharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
            
        default:
            return true
            
        }
        
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == UIReturnKeyType.next{
            if textField.tag == 2{
                let cellTv = tableTextViewInputCell(textField.tag + 2)
                if let _ = cellTv {
                    cellTv!.txtv.becomeFirstResponder()
                    return false
                }
            }else{
                let cell = tableTextFiledInputCell(textField.tag + 2)
                if let _ = cell {
//                    scrollToIndex(textField.tag + 2)
                    cell!.txtf.becomeFirstResponder()
                }
            }
            
        }else{
            textField.resignFirstResponder()
            
        }
        return true
    }
}
//MARK:- UITextView Delegate

extension FeedbackVC {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool{
        toolBar.btnOK.setTitle(kDone, for: UIControlState())
        textView.inputAccessoryView = toolBar
        toolBar.handleTappedAction({ (tapped, alert) in
            if tapped == .ok{
                self.view.endEditing(true)
            }
        })
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView){
        scrollToIndex(textView.tag + 1)
    }
    
    
}
// MARK: - TableViewDataSource & Delegate
extension FeedbackVC{
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellIdentifiers.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (indexPath as NSIndexPath).row {
        case 1,2,3:
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifiers[(indexPath as NSIndexPath).row], for: indexPath) as! TextFiledInputCell
            cell.parentFeedback = self
            cell.prepareUI(fieldData.allFileds[(indexPath as NSIndexPath).row - 1], type: .feedback, tag: (indexPath as NSIndexPath).row - 1)
            
            return cell
            
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifiers[(indexPath as NSIndexPath).row], for: indexPath) as! TextViewInputCell
            cell.parentFeedback = self
            cell.prepareUI(fieldData.allFileds[(indexPath as NSIndexPath).row - 1], type: .feedback, tag: (indexPath as NSIndexPath).row - 1)
            
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifiers[(indexPath as NSIndexPath).row], for: indexPath) as! ButtonCell
            
            cell.handleBlockAction({ (tapped, button) in
                if tapped == .action{
                    let validation = self.fieldData.isValidData()
                    if validation.valid {
                        self.view.endEditing(true)
                        self.addFeedback()
                    } else {
                        _ = ValidationToast.showBarMessage(validation.error)
                    }
                }
            })
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifiers[0], for: indexPath) as! GenericTableViewCell
            return cell
        }
    }
    
}

//MARK:- Web Operation
extension FeedbackVC{
    
    func addFeedback(){
        self.showCentralSpinner()
        
        WebService.wsCall.addFeedback(fieldData.paramDictionary()) { (json, flag) in
            self.hideCentralSpinner()
             // Internet connection goes down.
            if flag == _offline {
                _ = ValidationToast.showStatusMessage(kInternetDown)
                return
            }
            if flag == 200 {
                if let jsonResult = json as? NSDictionary {
                    let record = RawdataConverter.string(jsonResult["record"])
                    if record.characters.count > 0 {
                        _ = ValidationToast.showBarMessage(record)
                        return
                    }
                    _ = self.navigationController?.popViewController(animated: true)
                }
            }else if let jsonResult = json as? NSDictionary{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(jsonResult["message"]))
            }else{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(kSomethingWentWrong))
            }
        }
    }
}
