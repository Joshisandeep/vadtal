//
//  NirnayPopUpVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 25/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class NirnayPopUpVC: ParentVC {
    
    //MARK:- Outlet
    let menuItems = HomeMenuItem.allValues
    @IBOutlet var lblMsgTile : UILabel!
    @IBOutlet var lblMsgSubTitle : UILabel!

    //MARK:- Variables
    var objNirnay : Nirnay!
    // MARK: - iOS Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
//MARK:- Private
extension NirnayPopUpVC{
    func prepareUI(){
        if let obj = objNirnay{
            if let date = obj.date{
                lblTitle.text = date.formatted7
            }else {
               lblTitle.text =  Date().changeDateFormateWithGivenDate("dd-MM-yyyy", format1: "dd MMMM, yyyy", fromDate: obj.fulldate)
            }
            lblMsgTile.text = obj.getTithi()
            lblMsgSubTitle.text = obj.txt_detail

        }else{
            lblTitle.text = ""
            lblMsgTile.text = ""
            lblMsgSubTitle.text = ""
        }
        
    }
}
