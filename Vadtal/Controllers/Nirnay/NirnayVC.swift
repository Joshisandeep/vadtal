//
//  NirnayVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 15/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit
import FSCalendar

class NirnayVC: ParentVC {
    
    //MARK:- Outlet
    @IBOutlet var imgvCalenderBG: UIImageView!
    @IBOutlet var calenderView : UIView!
    @IBOutlet var calender : FSCalendar!
    @IBOutlet var lblSavant : UILabel!
    @IBOutlet var lblSane : UILabel!
    @IBOutlet var lblToday : UILabel!
    @IBOutlet var lblEkadashi : UILabel!
    @IBOutlet var lblPoonam : UILabel!
    @IBOutlet var lblMonth : UILabel!
    @IBOutlet var btnPrevMonth : UIButton!
    @IBOutlet var btnNextMonth : UIButton!
    
    //MARK:- Variables
    var arrWeek :[String] = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]
    var arrNirnay:[Nirnay] = []
    var arrEkadshiDates:[String] = []
    var arrPoonamDates:[String] = []
    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareCalenderUI()
        prepareToUpdate()
        getNirnayList(Date())
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  let row = sender as? Int , segue.identifier == "ToNirnayPopup"{
            let dest = segue.destination as! NirnayPopUpVC
            dest.objNirnay = arrNirnay[row]
        }
    }
}
// MARK: - Private
extension NirnayVC{
    func prepareUI(){
       
        lblToday.text = ""
        lblEkadashi.text = ""
        lblPoonam.text = ""
    }
    func prepareCalenderUI(){
        calender.delegate = self
        calender.dataSource = self
        calender.allowsMultipleSelection = false
        calender.backgroundColor = .clear
        calender.headerHeight = 0
        calender.weekdayHeight = 0
        calender.pagingEnabled = true
        calender.scrollEnabled = true
        calender.placeholderType = .none
    }
    
    func prepareToUpdate(_ isReload : Bool = false){
        arrEkadshiDates = []
        arrPoonamDates = []
        arrEkadshiDates = arrNirnay.filter { (nirnay) -> Bool in
            return nirnay.special_day == .ekadasi
            }.map {$0.fulldate}
        
        
        arrPoonamDates = arrNirnay.filter { (nirnay) -> Bool in
            return nirnay.special_day == .poonam
            }.map {$0.fulldate}
        
        updateMonthTitle()
        if isReload{
            calender.reloadData()
        }
    }
    func updateMonthTitle()
    {
        lblMonth.text = calender.currentPage.formatted3
        if arrNirnay.count > 0{
            lblSavant.text = "Savant" + " " + arrNirnay[0].savant
            lblSane.text = "Sane" + " "  + arrNirnay[0].sane
        }else{
            lblSavant.text = ""
            lblSane.text = ""
            
        }
    }
}
// MARK: - Actions
extension NirnayVC{
    @IBAction func btnPrevMonth(_ sender:UIButton ){
        calender.setCurrentPage(calender.currentPage.get1MonthPreviousDate(), animated: true)
    }
    
    @IBAction func btnNextMonthTapped(_ sender:UIButton!){
        calender.setCurrentPage(calender.currentPage.get1MonthNextDate(), animated: true)
        updateMonthTitle()
        
    }
    
}

//MARK:- UICollectionView DataSource & Delegate Method
extension NirnayVC{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrWeek.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! GenericCollectionViewCell
        cell.lblTitle.text = arrWeek[(indexPath as NSIndexPath).row]
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets{
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        let w = collectionView.bounds.size.width/7
        return CGSize(width: w ,height: 30)
    }
}
//FS Calendar DataSource & Delegate Method
extension NirnayVC : FSCalendarDataSource,FSCalendarDelegate{
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int
    {
        return 0
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date){
        jprint("date:\(date.formatted1_1)")
        let index = arrNirnay.index { (value) -> Bool in
            return value.fulldate == date.formatted1_1
        }
        if let row = index , row < arrNirnay.count{
            self.performSegue(withIdentifier: "ToNirnayPopup", sender: row)
        }
    }
    func calendarCurrentMonthDidChange(_ calendar: FSCalendar){
        updateMonthTitle()
        getNirnayList(calender.currentPage)
    }
}

//FS Calendar Delegate Appearance Method
extension NirnayVC : FSCalendarDelegateAppearance{
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderDefaultColorFor date: Date) -> UIColor?{
        
        if date.formatted1_1 == Date().formatted1_1 {
            return UIColor.white
        }
        return nil
    }
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderSelectionColorFor date: Date) -> UIColor?{
        if date.formatted1_1 == Date().formatted1_1 {
            return UIColor.white
        }
        
        return nil
    }
    
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor?{
        //For Ekadshi
        
        if !arrEkadshiDates.isEmpty{
            if  arrEkadshiDates.contains(date.formatted1_1){
                return UIColor.calenderEkasdshiBGColor()
                
            }
        }
        
        //For Poonam
        if !arrPoonamDates.isEmpty{
            if  arrPoonamDates.contains(date.formatted1_1){
                return UIColor.calenderPoonamBGColor()
                
            }
        }
        
        return nil
    }
    
    
    
}

//MARK:- Web Operation
extension NirnayVC{
    
    func getNirnayList(_ date:Date){
        self.showCentralSpinner()
        WebService.wsCall.getNirnayList(date.formatted4, year: date.formatted5) { (json, flag) in
            self.hideCentralSpinner()
             // Internet connection goes down.
            if flag == _offline {
                _ = ValidationToast.showStatusMessage(kInternetDown)
                return
            }
            if flag == 200 {
                if let jsonResult = json as? [NSDictionary] {
                    
                    
                    if let firstDict = jsonResult.first {
                        let record = RawdataConverter.string(firstDict["record"])
                        if record.characters.count > 0 {
                            _ =  ValidationToast.showBarMessage(record)
                            return
                        }
                    }
                    if jsonResult.isEmpty{
                        return
                    }
                    self.arrNirnay.removeAll()
                    for dict in jsonResult{
                        self.arrNirnay.append(Nirnay(dict: dict))
                    }
                    self.prepareToUpdate(true)
                }
            }
            else if let jsonResult = json as? NSDictionary{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(jsonResult["message"]))
            }
            else{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(kSomethingWentWrong))
            }
        }
    }
}

