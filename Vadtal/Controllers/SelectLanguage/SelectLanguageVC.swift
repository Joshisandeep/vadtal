//
//  SelectLanguageVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 13/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class SelectLanguageVC: ParentVC {
    
    //MARK:- Outlet
    let cellIdentifiers :[String] = ["TitleCell","BtnCell","BtnCell"]
    let imgName :[String] = ["ic_select_language","ic_eng_language","ic_guj_language"]
    
    //MARK:- Variables
    
    // MARK: - iOS Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK:- Private
extension SelectLanguageVC{
    func prepareUI(){
        self.tableView.sectionFooterHeight = CGFloat.leastNormalMagnitude
        self.tableView.keyboardDismissMode  = .onDrag
    }
    
    func navigateToHomeScreen(){
        let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(vc, animated: true)
//        HomeVC
    }
}


// MARK: - TableViewDataSource & Delegate
extension SelectLanguageVC{
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        return cellIdentifiers.count
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40 * _widthRatio
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        switch (indexPath as NSIndexPath).section {
        case 0:
            return 170 * _widthRatio
        case 1,2:
            return 100 * _widthRatio
            
        default:
            return CGFloat.leastNormalMagnitude
        }
        
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifiers[(indexPath as NSIndexPath).section], for: indexPath) as! SelectLangCell
        if (indexPath as NSIndexPath).section != 0{
            cell.btn.tag = (indexPath as NSIndexPath).section
            cell.btn.setImage(UIImage(named: imgName[(indexPath as NSIndexPath).section]), for: UIControlState())
        }else{
            cell.imgv.image = UIImage(named: imgName[(indexPath as NSIndexPath).section])
        }
        
        cell.handleBlockAction { (tapped, button) in
            if tapped == .action{
                
                //If tag is 2 then Gujarti language on tapped else english
                
                if button.tag == 2{
                    _userSelectedLanguage = .gujarati
                }else{
                    _userSelectedLanguage = .english
                }
                self.navigateToHomeScreen()
            }
        }
        return cell
    }
    
}
