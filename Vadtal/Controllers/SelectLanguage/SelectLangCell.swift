//
//  SelectLangCell.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 14/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit
// MARK: Enum
enum Tapped {
    case okay,done,yes,cancel,valueChange,action
}

class SelectLangCell: ConstrainedTableViewCell {
    @IBOutlet var imgv: UIImageView!
    @IBOutlet var btn: UIButton!
    
    var actionBlock : ((_ tapped: Tapped, _ button:UIButton) -> ())?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: Method
    func handleBlockAction(_ block: @escaping (_ tapped: Tapped,_ button:UIButton) -> ()){
        actionBlock = block
    }
    
    
    // MARK: Button Actions
    @IBAction func btnTapped(_ sender: UIButton!) {
        actionBlock?(Tapped.action, sender)
    }
    
    

}
