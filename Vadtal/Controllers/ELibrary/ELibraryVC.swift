//
//  ELibraryVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 15/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit
import QuickLook
import Kingfisher

class ELibraryVC: ParentVC {
    
    //MARK:- Outlet
    
    //MARK:- Variables
    var arrLibrary:[Library] = []
    var fileUrl :URL!
    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getLibraryList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK:- Private
extension ELibraryVC{
    
    func openPdfPreviewController(_ url:URL){
        fileUrl = url
        if QLPreviewController.canPreview(url as QLPreviewItem) {
            let quickLookController = QLPreviewController()
            quickLookController.dataSource = self
            quickLookController.delegate = self
            self.present(quickLookController, animated: true, completion: nil)
        }
    }
}
//MARK:- UICollectionView DataSource & Delegate Method
extension ELibraryVC{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrLibrary.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! GenericCollectionViewCell
        cell.imgv.kf.setImage(with: URL(string: arrLibrary[indexPath.row].cover_img_path), placeholder: media_Placeholder, options: nil, progressBlock: nil, completionHandler: nil)
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath){
        let library = arrLibrary[(indexPath as NSIndexPath).row]
        if let url = DocumentAccess.obj.documentForName(library.pdf_file){
            self.openPdfPreviewController(url)
        }else{
            self.downloadFile(URL(string: library.pdf_file_path)!)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets{
        let pending = (_screenSize.width - ((170 * _widthRatio) * 2))/3
        return UIEdgeInsetsMake(10, pending, 10, pending)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        return CGSize(width: (170 * _widthRatio) ,height: 135 * _widthRatio)
    }
}

//MARK:- Web Operation
extension ELibraryVC{
    
    func getLibraryList(){
        self.showCentralSpinner()
        WebService.wsCall.getLibraryList { (json, flag) in
            self.hideCentralSpinner()
             // Internet connection goes down.
            if flag == _offline {
                _ = ValidationToast.showStatusMessage(kInternetDown)
                return
            }
            if flag == 200 {
                if let jsonResult = json as? [NSDictionary] {
                    
                    
                    if let firstDict = jsonResult.first {
                        let record = RawdataConverter.string(firstDict["record"])
                        if record.characters.count > 0 {
                            _ = ValidationToast.showBarMessage(record)
                            return
                        }
                    }
                    if jsonResult.isEmpty{
                        return
                    }
                    self.arrLibrary.removeAll()
                    for dict in jsonResult{
                        self.arrLibrary.append(Library(dict: dict))
                    }
                    self.collectionView.reloadData()
                    
                }
            }
            else if let jsonResult = json as? NSDictionary{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(jsonResult["message"]))
            }
            else{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(kSomethingWentWrong))
            }
        }
    }
    
    func downloadFile(_ url:URL){
        self.showCentralSpinner()
        WebService.wsCall.downloadLibrary(url, progress: nil, block: { (path, success) in
            self.hideCentralSpinner()
            if success == true{
                self.openPdfPreviewController(path)
            }
        })
    }
}


//MARK:- QLPreviewControllerDataSource
extension ELibraryVC : QLPreviewControllerDataSource,QLPreviewControllerDelegate{
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        if fileUrl != nil{
            return 1
        }
        return 0
    }
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return fileUrl as QLPreviewItem
    }
    func previewControllerWillDismiss(_ controller: QLPreviewController) {
        print("The Preview Controller will be dismissed.")
    }
    
    func previewControllerDidDismiss(_ controller: QLPreviewController) {
        print("The Preview Controller has been dismissed.")
    }
    
    func previewController(_ controller: QLPreviewController, shouldOpen url: URL, for item: QLPreviewItem) -> Bool {
        if item as! URL == fileUrl!{
            return true
        }
        else {
            print("Will not open URL \(url.absoluteString)")
        }
        
        return false
    }
}
