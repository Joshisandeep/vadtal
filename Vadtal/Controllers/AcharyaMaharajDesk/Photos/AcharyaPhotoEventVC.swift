//
//  AcharyaPhotoEventVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 25/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class AcharyaPhotoEventVC: ParentVC {
    
    //MARK:- Outlet
    
    //MARK:- Variables
    var  events : [AcharyPhotoEvent] = []
    var photoYear : AcharyPhotoYear!
    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


//MARK:- Private
extension AcharyaPhotoEventVC{
    func prepareUI(){
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 15, 0)
        lblSubTitle.text = photoYear.year + " " + "EVENTS"
        getPhotoYearEventList()
    }
}
// MARK: - TableViewDataSource & Delegate
extension AcharyaPhotoEventVC{
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 152
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! GenericTableViewCell
        let event = events[(indexPath as NSIndexPath).row]
        cell.lblTitle.text = event.event_name
        cell.lblSubtitle.text = event.event_dt
        cell.imgv.kf.setImage(with: URL(string: event.gallery_cover_img_path), placeholder: media_Placeholder, options: nil, progressBlock: nil, completionHandler: nil)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath)
    {
        let event = events[(indexPath as NSIndexPath).row]
        let vc = UIStoryboard(name: "AcharyaMaharajDesk", bundle: nil).instantiateViewController(withIdentifier: "AcharyaPhotoVC") as! AcharyaPhotoVC
        vc.yearEvent = event
        self.navigationController?.pushViewController(vc, animated: true)

    }
}


//MARK:- Web Operation
extension AcharyaPhotoEventVC{
    
    func getPhotoYearEventList(){
        self.showCentralSpinner()
        
        WebService.wsCall.getAcharyPhotoEventList(photoYear.year) { (json, flag) in
            self.hideCentralSpinner()
             // Internet connection goes down.
            if flag == _offline {
                _ = ValidationToast.showStatusMessage(kInternetDown)
                return
            }
            if flag == 200 {
                if let jsonResult = json as? [NSDictionary] {
                    if let firstDict = jsonResult.first {
                        let record = RawdataConverter.string(firstDict["record"])
                        if record.characters.count > 0 {
                            _ = ValidationToast.showBarMessage(record)
                            return
                        }
                    }
                    if jsonResult.isEmpty{
                        return
                    }
                    self.events.removeAll()
                    for dict in jsonResult{
                        self.events.append(AcharyPhotoEvent(dict: dict))
                    }
                    self.tableView.reloadData()
                }
            }else if let jsonResult = json as? NSDictionary{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(jsonResult["message"]))
            }else{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(kSomethingWentWrong))
            }
        }
    }
    
}
