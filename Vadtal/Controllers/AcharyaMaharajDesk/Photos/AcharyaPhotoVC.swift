//
//  AcharyaPhotoVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 25/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class AcharyaPhotoVC: ParentVC {
    
    //MARK:- Outlet
    var  photos : [AcharyaEventPhoto] = []
    
    var yearEvent : AcharyPhotoEvent!
    //MARK:- Variables
    
    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK:- Private
extension AcharyaPhotoVC{
    func prepareUI(){
        if let event = yearEvent{
            lblSubTitle.text = event.event_name
            getPhotosList(event.event_code)
        }else{
            lblSubTitle.text = ""
        }
    }
    
}
//MARK:- UICollectionView DataSource & Delegate Method
extension AcharyaPhotoVC{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! GenericCollectionViewCell
        
        cell.imgv.kf.setImage(with: URL(string: photos[indexPath.item].photo_path), placeholder: media_Placeholder, options: nil, progressBlock: nil, completionHandler: nil)
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath){
        let vc = UIStoryboard(name: "Zoom", bundle: nil).instantiateViewController(withIdentifier: "FullScreenPhotoVC") as! FullScreenPhotoVC
        vc.initialViewIndex = (indexPath as NSIndexPath).row
        vc.imagePaths = photos.map{return $0.photo_path}
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets{
        return UIEdgeInsetsMake(10, 10, 10, 10)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        let w = (_screenSize.width - 45)/3
        return CGSize(width: w ,height: w)
    }
}


//MARK:- Web Operation
extension AcharyaPhotoVC{
    
    func getPhotosList(_ code: String){
        self.showCentralSpinner()
        WebService.wsCall.getAcharyEventPhotos(code) { (json, flag) in
            self.hideCentralSpinner()
             // Internet connection goes down.
            if flag == _offline {
                _ = ValidationToast.showStatusMessage(kInternetDown)
                return
            }
            if flag == 200 {
                if let jsonResult = json as? [NSDictionary] {
                    if let firstDict = jsonResult.first {
                        let record = RawdataConverter.string(firstDict["record"])
                        if record.characters.count > 0 {
                            _ =  ValidationToast.showBarMessage(record)
                            return
                        }
                    }
                    if jsonResult.isEmpty{
                        return
                    }
                    self.photos.removeAll()
                    for dict in jsonResult{
                        self.photos.append(AcharyaEventPhoto(dict: dict))
                    }
                    self.collectionView.reloadData()
                }
            }else if let jsonResult = json as? NSDictionary{
                _ =  ValidationToast.showBarMessage(RawdataConverter.string(jsonResult["message"]))
            }else{
                _ =  ValidationToast.showBarMessage(RawdataConverter.string(kSomethingWentWrong))
            }
        }
    }
    
}
