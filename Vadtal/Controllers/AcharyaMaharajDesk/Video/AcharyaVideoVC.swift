//
//  AcharyaVideoVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 25/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class AcharyaVideoVC: ParentVC {
    
    //MARK:- Outlet
    
    //MARK:- Variables
    
    var videos : [AcharyaVideoYearAlbum] = []
    var videoYear : AcharyaVideoYear!
    
    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK:- Private
extension AcharyaVideoVC{
    func prepareUI(){
        lblSubTitle.text = videoYear.year + " " + "EVENTS"
        getVideoAlbumDetail()
    }

    
}
//MARK:- UICollectionView DataSource & Delegate Method
extension AcharyaVideoVC{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! GenericCollectionViewCell
        
        let video = videos[(indexPath as NSIndexPath).item]
        
        cell.lblTitle.text = video.video_desc
        
        cell.imgv.kf.setImage(with: URL(string: video.video_url.extractYoutubeIdFromLink().fetchYoutubeVideoUrl()), placeholder: media_Placeholder, options: nil, progressBlock: nil, completionHandler: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath){
        let vc = UIStoryboard(name: "Player", bundle: nil).instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
        vc.videoID = videos[(indexPath as NSIndexPath).item].video_url.extractYoutubeIdFromLink()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets{
        return UIEdgeInsetsMake(10, 10, 10, 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        let w = (_screenSize.width - 35)/2
        return CGSize(width: w ,height: w + 30)
    }
}

//MARK:- Web Operation
extension AcharyaVideoVC{
    
    func getVideoAlbumDetail(){
        self.showCentralSpinner()
        WebService.wsCall.getAcharyVideoAlbumDetail(videoYear.year) { (json, flag) in
            self.hideCentralSpinner()
             // Internet connection goes down.
            if flag == _offline {
                _ = ValidationToast.showStatusMessage(kInternetDown)
                return
            }
            if flag == 200 {
                if let jsonResult = json as? [NSDictionary] {
                    
                    
                    if let firstDict = jsonResult.first {
                        let record = RawdataConverter.string(firstDict["record"])
                        if record.characters.count > 0 {
                            _ =  ValidationToast.showBarMessage(record)
                            return
                        }
                    }
                    if jsonResult.isEmpty{
                        return
                    }
                    self.videos.removeAll()
                    for dict in jsonResult{
                        self.videos.append(AcharyaVideoYearAlbum(dict: dict))
                    }
                    self.collectionView.reloadData()
                    
                }
            }
            else if let jsonResult = json as? NSDictionary{
               _ = ValidationToast.showBarMessage(RawdataConverter.string(jsonResult["message"]))
            }
            else{
                _ =  ValidationToast.showBarMessage(RawdataConverter.string(kSomethingWentWrong))
            }
        }
    }
    
    
}
