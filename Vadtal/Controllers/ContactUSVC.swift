//
//  ContactUSVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 15/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class ContactUSVC: ParentVC {
    
    //MARK:- Outlet
    
    //MARK:- Variables
    
    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK:- Private
extension ContactUSVC{
    func prepareUI(){
        self.tableView.contentInset = UIEdgeInsetsMake(15, 0, 15, 0)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 50
    }
}
// MARK: - TableViewDataSource & Delegate
extension ContactUSVC{
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell\((indexPath as NSIndexPath).row)", for: indexPath) as! GenericTableViewCell
        return cell
    }
    
}

