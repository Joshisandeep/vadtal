//
//  AudioPlayer.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 18/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class AudioPlayerVC: ParentVC {
    // MARK: - Outlets
    @IBOutlet var imgv : UIImageView!
    @IBOutlet var lblAudioTitle : UILabel!
    @IBOutlet var slider : UISlider!

    @IBOutlet var viewControl : UIView!
    @IBOutlet var btnNext : UIButton!
    @IBOutlet var btnPervious : UIButton!
    @IBOutlet var btnPlay : UIButton!


    // MARK: - Varible
    var player: AudioPlayer! = AudioPlayer.sharedInstance
    var album :AudioAlbum!
    var songs :[AlbumSong] = []
    var index : Int = 0
    
    //Struc for player specific
    var playerSpecific = AudioPlayerSpecific()
    struct AudioPlayerSpecific {
        var secondCounter = 0
        var countDownTimer: Timer!
        
    }
    // MARK: - iOS Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

//MARK:- Private
extension AudioPlayerVC{
    func prepareUI(){
        imgv.kf.setImage(with: URL(string: album.cover_img_path), placeholder: media_Placeholder, options: nil, progressBlock: nil, completionHandler: nil)

        self.prepareResetUI()
        prepareUpdateData(true)
    }
   
    func prepareUpdateData(_ isToPlay:Bool = false){
        if index < songs.count{
            lblAudioTitle.text = songs[index].song_title
            if isToPlay{
                if let path = URL(string: songs[index].song_path){
                    prepareForPlaying(path)
                }
            }
        }else{
            lblAudioTitle.text = ""
        }
       
    }
    
    func prepareForPlaying(_ url :URL){
        self.showCentralSpinner(true)
        player.prepareForPlayingAudio(withUrl: url, block: { (state) -> Void in
            self.hideCentralSpinner()
            if state == PlayerState.readyToPlay {
                self.player.currentIndex = 0
                self.player.starPlayingAudio()
                self.prepareTimerScheduled()
            }else if state == PlayerState.complete{
                self.prepareResetUI()
            }else{
                _ = ValidationToast.showBarMessage(kSomethingWentWrong)
                self.prepareResetUI()
            }
        })

        
    }
    func prepareResetUI(){
        btnPlay.isSelected = false
        slider.value = 0.0
        if self.playerSpecific.countDownTimer != nil{
            self.playerSpecific.countDownTimer.invalidate()
        }
        playerSpecific = AudioPlayerSpecific()
//        lblMinTimer.text = "0:00"
//        lblMaxTimer.text = String(format: "0:%.2d",Int(player.getAudioDuration()))
    }
    func progressViewAppearance(){
//        lblMinTimer.text = "0:00"
//        lblMaxTimer.text = String(format: "0:%.2d",Int(player.getAudioDuration()))
        
        slider.value = 0.0
    }
    
    func playTimer(_ timer: Timer){
        jprint("SliderRecording Value \(slider.value)")
        if slider.maximumValue == 1 {
            btnPlay.isSelected = false
            timer.invalidate()
        }
        else{
            playerSpecific.secondCounter += 1
            
            slider.value =  self.player.getCurrentAudioTime() / self.player.getAudioDuration()
//            lblMinTimer.text = String(format: "0:%.2d",(playerSpecific.secondCounter)/10)
            //            progressView.progress = Float(playerSpecific.secondCounter)/Float(audioLength * 10)
            jprint("\n\ngetCurrentAudioTime:\(self.player.getCurrentAudioTime()) ,\(self.player.getAudioDuration())")
            
        }
    }
    
    func prepareTimerScheduled(){
        playerSpecific.countDownTimer = Timer.scheduledTimer(timeInterval: TimeInterval(0.1), target: self, selector:  #selector(AudioPlayerVC.playTimer(_:)), userInfo: nil, repeats: true)
    }
    
    func prepareForPlayOrPasue(){
        if btnPlay.isSelected {
            btnPlay.isSelected = false
            if playerSpecific.countDownTimer != nil{
                player.pausePlayingAudio()
                playerSpecific.countDownTimer.invalidate()
            }
        }else{
            btnPlay.isSelected = true
            //invalidate past counert
            if playerSpecific.countDownTimer != nil{
                playerSpecific.countDownTimer.invalidate()
            }
            if player.currentIndex == 0{
                
                self.player.starPlayingAudio()
                self.prepareTimerScheduled()
            }else{
                if !songs.isEmpty &&  index < songs.count {
                    
                    if let path = URL(string: songs[index].song_path){
                        prepareForPlaying(path)
                    }
                }
                
                
            }
        }
    }

}

//MARK:- Actions
extension AudioPlayerVC{
    @IBAction override func parentBackAction(_ sender:UIButton ){
        if playerSpecific.countDownTimer != nil{
            player.pausePlayingAudio()
            playerSpecific.countDownTimer.invalidate()
        }
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnPerviousTapped(_ sender:UIButton ){
        if index == 0 {
            return
        }
        index -= 1
        prepareUpdateData(true)
    }
    
    @IBAction func btnNextTapped(_ sender:UIButton ){
        if songs.isEmpty{
            return
        }
        if index == (songs.count - 1) {
            return
        }
        index += 1
        prepareUpdateData(true)
    }
    
    @IBAction func btnPlayTapped(_ sender:UIButton ){
//        prepareForPlayOrPasue()
        if let path = URL(string: songs[index].song_path){
            let player = AVPlayer(url: path)
            
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
            
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = self.view.bounds
            self.view.layer.addSublayer(playerLayer)

        
        }

        
    }
    
    @IBAction func sliderValueChanged(_ sender:UISlider ){
    }
    
}
