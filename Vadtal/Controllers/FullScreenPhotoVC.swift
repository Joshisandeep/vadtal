//
//  FullSizeImagesVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 21/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit
import Photos

class FullScreenPhotoVC: ParentVC {
    
    //MARK:- Outlet
    @IBOutlet var carouselView : MVCarouselCollectionView!;
    
    //MARK:- Variables
    var imagePaths : [String] = []
    var initialViewIndex : NSInteger = 0
    
    var imageViewLoadCached : ((_ imageView: UIImageView, _ imagePath : String, _ completion: (_ newImage: Bool) -> ()) -> ()) = {
        (imageView: UIImageView, imagePath : String, completion: (_ newImage: Bool) -> ()) in
        
        imageView.image = UIImage(named:imagePath)
        completion(imageView.image != nil)
    }
    
    var imageViewLoadFromPath: ((_ imageView: UIImageView, _ imagePath : String, _ completion: @escaping (_ newImage: Bool) -> ()) -> ()) = {
        (imageView: UIImageView, imagePath : String, block:  @escaping (_ newImage: Bool) -> ()) in
        
        var url = URL(string: imagePath)
        
        imageView.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil) { (image, error, cacheType, imageURL) in
            block(image != nil)
        }
    }
    
    
    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //self.collectionView.reloadData()
        // Apparently on iOS 7 scrollToInitialIndex doesn't have an effect here. Using performSelector fixes it
        Timer.scheduledTimer(timeInterval: 0.0, target: self, selector: #selector(FullScreenPhotoVC.scrollToInitialIndex), userInfo: nil, repeats: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}



//MARK:- Private
extension FullScreenPhotoVC{
    func prepareUI(){
        self.carouselView.selectDelegate = self
        self.carouselView.commonImageLoader = self.imageViewLoadFromPath
        self.carouselView.imagePaths = self.imagePaths
        self.carouselView.maximumZoom = 4.0
        self.carouselView.reloadData()
        prepareSubTitleUI(initialViewIndex)
        
    }
    func prepareSubTitleUI(_ index:Int){
        let count1  =    JPUtility.shared.sjCount(Double(index + 1))
        let count2  =    JPUtility.shared.sjCount(Double(imagePaths.count))
        
        self.lblSubTitle.text = count1 + " of " + count2
    }
    func scrollToInitialIndex() {
        let indexPath = IndexPath(row:self.initialViewIndex, section: 0)
        self.carouselView.scrollToItem(at: indexPath, at:UICollectionViewScrollPosition.centeredHorizontally, animated: false)
    }
}
// MARK: - Actions
extension FullScreenPhotoVC{
    @IBAction func btnShareAction(_ sender:UIButton ){
        let indexPath = IndexPath(row:self.initialViewIndex, section: 0)
        
        if let cell = carouselView.cellForItem(at: indexPath) as? MVCarouselCell {
            if let image  = cell.scrollView.imageView.image{
                ShareController.shared.openShareKit([image], toController: self, compilation: { (success) in
                    
                })
                
            }else{
                _ = ValidationToast.showStatusMessage(kImageUnloaded)
            }
        }
        
        
    }
    
    @IBAction func btnSaveAction(_ sender:UIButton ){
        let indexPath = IndexPath(row:self.initialViewIndex, section: 0)
        
        if let cell = carouselView.cellForItem(at: indexPath) as? MVCarouselCell {
            if let image  = cell.scrollView.imageView.image {
                
                PHPhotoLibrary.requestAuthorization { status in
                    switch status {
                    case .authorized:
                        UIImageWriteToSavedPhotosAlbum(image, self, #selector(FullScreenPhotoVC.image(_:didFinishSavingWithError:contextInfo:)), nil)
                        break
                    case .restricted:
                        Popups.instance.ShowPopup(self, title: kPhotosAccessTitle, message: kPhotosAccessMsg)
                        break
                    case .denied:
                         Popups.instance.ShowPopup(self, title: kPhotosAccessTitle, message: kPhotosAccessMsg)
                        break
                    default:
                        // place for .notDetermined - in this callback status is already determined so should never get here
                        break
                    }
                }
            }else{
                _ = ValidationToast.showStatusMessage(kImageUnloaded)

            }
        }
        
    }
    func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            _ = ValidationToast.showStatusMessage(error.localizedDescription)
        } else {
             _ = ValidationToast.showStatusMessage(kImageSave, inView: nil, withColor: UIColor.popupSuccessColor())
        }
    }
}

//MARK:- MVCarousel CollectionView Delegate
extension FullScreenPhotoVC : MVCarouselCollectionViewDelegate{
    func carousel(_ carousel: MVCarouselCollectionView, didSelectCellAtIndexPath indexPath: IndexPath) {
    }
    
    func carousel(_ carousel: MVCarouselCollectionView, didScrollToCellAtIndex cellIndex : NSInteger) {
        initialViewIndex = cellIndex
        prepareSubTitleUI(cellIndex)
        
    }
}
