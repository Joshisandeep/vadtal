//
//  VideoGalleryVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 16/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class VideoGalleryVC: ParentVC {
    
    //MARK:- Outlet
    
    //MARK:- Variables
    
    var videos : [VideoAlbum] = []
    var album : VideoYearAlbum!
    var loader = LoadMoreSpecific()
    
    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK:- Private
extension VideoGalleryVC{
    func prepareUI(){
        loader.totalRecord = album.tot_page
        lblSubTitle.text = album.video_title
        
        getVideoAlbumDetail()
    }
    func pagingMangemnet(_ indexPath:IndexPath){
        // Paging managment, It will load next page as it reached to last cell.
        if (indexPath as NSIndexPath).item == self.videos.count - 1 &&
            !self.loader.isLoading   &&  self.videos.count < self.loader.totalRecord {
            self.callForMore()
        }
    }
    
}
//MARK:- UICollectionView DataSource & Delegate Method
extension VideoGalleryVC{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! GenericCollectionViewCell
        
        let video = videos[(indexPath as NSIndexPath).item]
        
        cell.lblTitle.text = video.video_desc
        
        cell.imgv.kf.setImage(with: URL(string: video.video_url.extractYoutubeIdFromLink().fetchYoutubeVideoUrl()), placeholder: media_Placeholder, options: nil, progressBlock: nil, completionHandler: nil)

        self.pagingMangemnet(indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath){
        let vc = UIStoryboard(name: "Player", bundle: nil).instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
        vc.videoID = videos[(indexPath as NSIndexPath).item].video_url.extractYoutubeIdFromLink()
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets{
        return UIEdgeInsetsMake(10, 10, 10, 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        let w = (_screenSize.width - 30)/2
        return CGSize(width: w ,height: w + 30)
    }
}

//MARK:- Web Operation
extension VideoGalleryVC{
    
    func getVideoAlbumDetail(){
        self.showCentralSpinner()
        WebService.wsCall.getVideoAlbumDetail(album.video_id, page: loader.page) { (json, flag) in
            self.hideCentralSpinner()
             // Internet connection goes down.
            if flag == _offline {
                _ = ValidationToast.showStatusMessage(kInternetDown)
                return
            }
            if flag == 200 {
                if let jsonResult = json as? [NSDictionary] {
                    
                    
                    if let firstDict = jsonResult.first {
                        let record = RawdataConverter.string(firstDict["record"])
                        if record.characters.count > 0 {
                            _ = ValidationToast.showBarMessage(record)
                            return
                        }
                    }
                    if jsonResult.isEmpty{
                        return
                    }
                    self.videos.removeAll()
                    for dict in jsonResult{
                        self.videos.append(VideoAlbum(dict: dict))
                    }
                    self.collectionView.reloadData()
                    
                }
            }
            else if let jsonResult = json as? NSDictionary{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(jsonResult["message"]))
            }
            else{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(kSomethingWentWrong))
            }
        }
    }
    
    func callForMore(){
        self.loader.isLoading = true
        
        WebService.wsCall.getVideoAlbumDetail(album.video_id, page: loader.page) { (json, flag) in
            self.loader.isLoading = false
             // Internet connection goes down.
            if flag == _offline {
                _ = ValidationToast.showStatusMessage(kInternetDown)
                return
            }
            if flag == 200 {
                if let jsonResult = json as? [NSDictionary] {
                    
                    if jsonResult.isEmpty{
                        return
                    }
                    self.videos.removeAll()
                    for dict in jsonResult{
                        self.videos.append(VideoAlbum(dict: dict))
                    }
                    self.collectionView.reloadData()
                    
                }
            }
            else if let jsonResult = json as? NSDictionary{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(jsonResult["message"]))
            }
            else{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(kSomethingWentWrong))
            }
        }
    }
    
}

