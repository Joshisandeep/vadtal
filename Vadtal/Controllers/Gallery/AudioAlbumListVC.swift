//
//  AudioAlbumListVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 16/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class AudioAlbumListVC: ParentVC {
    
    //MARK:- Outlet
    
    //MARK:- Variables
    var songs :[AlbumSong] = []
    var album :AudioAlbum!
    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


//MARK:- Private
extension AudioAlbumListVC{
    func prepareUI(){
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 15, 0)
        self.getAudioAlbumDetail()
    }
    
}
// MARK: - TableViewDataSource & Delegate
extension AudioAlbumListVC{
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 70
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! GenericTableViewCell
        let song = songs[(indexPath as NSIndexPath).row]
        cell.lblTitle.text = song.albam_name
        cell.lblSubtitle.text = song.song_title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath)
    {
        
        let player = AVPlayer(url: URL(string: songs[(indexPath as NSIndexPath).row].song_path)!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
}


//MARK:- Web Operation
extension AudioAlbumListVC{
    
    func getAudioAlbumDetail(){
        self.showCentralSpinner()
        WebService.wsCall.getAudioAlbumDetail(album.albam_code) { (json, flag) in
            self.hideCentralSpinner()
             // Internet connection goes down.
            if flag == _offline {
                _ = ValidationToast.showStatusMessage(kInternetDown)
                return
            }
            if flag == 200 {
                if let jsonResult = json as? [NSDictionary] {
                    
                    
                    if let firstDict = jsonResult.first {
                        let record = RawdataConverter.string(firstDict["record"])
                        if record.characters.count > 0 {
                            _ = ValidationToast.showBarMessage(record)
                            return
                        }
                    }
                    if jsonResult.isEmpty{
                        return
                    }
                    self.songs.removeAll()
                    for dict in jsonResult{
                        self.songs.append(AlbumSong(dict: dict))
                    }
                    self.tableView.reloadData()
                    
                }
            }
            else if let jsonResult = json as? NSDictionary{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(jsonResult["message"]))
            }
            else{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(kSomethingWentWrong))
            }
        }
    }
}

