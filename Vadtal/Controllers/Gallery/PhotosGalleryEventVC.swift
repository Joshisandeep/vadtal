//
//  PhotosGalleryEventVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 16/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class PhotosGalleryEventVC: ParentVC {
    
    //MARK:- Outlet
    
    //MARK:- Variables
    var  events : [PhotoYearEvent] = []
    var photoYear : PhotoYear!
    var loader = LoadMoreSpecific()
    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


//MARK:- Private
extension PhotosGalleryEventVC{
    func prepareUI(){
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 15, 0)
        loader.totalRecord = photoYear.tot_page
        lblSubTitle.text = photoYear.year + " " + "EVENTS"
        getPhotoYearEventList()
    }
    func pagingMangemnet(_ indexPath:IndexPath){
        // Paging managment, It will load next page as it reached to last cell.
        if (indexPath as NSIndexPath).row == self.events.count - 1 &&
            !self.loader.isLoading   &&  self.events.count < self.loader.totalRecord {
            self.callForMore()
        }
    }
}
// MARK: - TableViewDataSource & Delegate
extension PhotosGalleryEventVC{
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 152
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! GenericTableViewCell
        let event = events[(indexPath as NSIndexPath).row]
        cell.lblTitle.text = event.event_name
        cell.lblSubtitle.text = event.date
        cell.imgv.kf.setImage(with: URL(string: event.event_cover_img), placeholder: media_Placeholder, options: nil, progressBlock: nil, completionHandler: nil)

        self.pagingMangemnet(indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath)
    {
        let event = events[(indexPath as NSIndexPath).row]
        // tot_gallery = 1 that means the event have subevent
        if event.tot_gallery == 1 {
            let vc = UIStoryboard(name: "Gallery", bundle: nil).instantiateViewController(withIdentifier: "PhotosGallerySubEventVC") as! PhotosGallerySubEventVC
            vc.yearEvent = event
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = UIStoryboard(name: "Gallery", bundle: nil).instantiateViewController(withIdentifier: "PhotosGalleryVC") as! PhotosGalleryVC
            vc.yearEvent = event
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
}


//MARK:- Web Operation
extension PhotosGalleryEventVC{
    
    func getPhotoYearEventList(){
        self.showCentralSpinner()
        
        WebService.wsCall.getPhotoYearEventList(photoYear.year, page: loader.page) { (json, flag) in
            self.hideCentralSpinner()
             // Internet connection goes down.
            if flag == _offline {
                _ = ValidationToast.showStatusMessage(kInternetDown)
                return
            }
            if flag == 200 {
                if let jsonResult = json as? [NSDictionary] {
                    if let firstDict = jsonResult.first {
                        let record = RawdataConverter.string(firstDict["record"])
                        if record.characters.count > 0 {
                            _ =  ValidationToast.showBarMessage(record)
                            return
                        }
                    }
                    if jsonResult.isEmpty{
                        return
                    }
                    self.events.removeAll()
                    for dict in jsonResult{
                        self.events.append(PhotoYearEvent(dict: dict))
                    }
                    self.loader.loadingPage += 1
                    self.tableView.reloadData()
                }
            }else if let jsonResult = json as? NSDictionary{
               _ = ValidationToast.showBarMessage(RawdataConverter.string(jsonResult["message"]))
            }else{
                _ =  ValidationToast.showBarMessage(RawdataConverter.string(kSomethingWentWrong))
            }
        }
    }
    
    func callForMore(){
        self.loader.isLoading = true
        
        WebService.wsCall.getPhotoYearEventList(photoYear.year, page: loader.page) { (json, flag) in
            self.loader.isLoading = false
             // Internet connection goes down.
            if flag == _offline {
                _ = ValidationToast.showStatusMessage(kInternetDown)
                return
            }
            if flag == 200 {
                if let jsonResult = json as? [NSDictionary] {
                    
                    if jsonResult.isEmpty{
                        return
                    }
                    for dict in jsonResult{
                        self.events.append(PhotoYearEvent(dict: dict))
                    }
                    self.loader.loadingPage += 1
                    self.tableView.reloadData()
                }
            }else if let jsonResult = json as? NSDictionary{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(jsonResult["message"]))
            }else{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(kSomethingWentWrong))
            }
        }
    }
}
