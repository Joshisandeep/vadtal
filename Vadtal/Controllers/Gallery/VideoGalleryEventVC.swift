//
//  VideoGalleryEventVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 16/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class VideoGalleryEventVC: ParentVC {
    
    //MARK:- Outlet
    
    //MARK:- Variables
    var albums : [VideoYearAlbum] = []
    var videoYear : VideoYear!
    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


//MARK:- Private
extension VideoGalleryEventVC{
    func prepareUI(){
        lblSubTitle.text = videoYear.year + " " + "EVENTS"

        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 15, 0)
        getVideoYearAlbumList()
    }
    
}
// MARK: - TableViewDataSource & Delegate
extension VideoGalleryEventVC{
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 152
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! GenericTableViewCell
        let album = albums[(indexPath as NSIndexPath).row]
        cell.lblTitle.text = album.video_title
        cell.lblSubtitle.text = ""
        cell.imgv.kf.setImage(with: URL(string: album.cover_img), placeholder: media_Placeholder, options: nil, progressBlock: nil, completionHandler: nil)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath)
    {
        let vc = UIStoryboard(name: "Gallery", bundle: nil).instantiateViewController(withIdentifier: "VideoGalleryVC") as! VideoGalleryVC
        vc.album = albums[(indexPath as NSIndexPath).row]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

//MARK:- Web Operation
extension VideoGalleryEventVC{
    
    func getVideoYearAlbumList(){
        self.showCentralSpinner()
        WebService.wsCall.getVideoYearAlbumList(videoYear.year) { (json, flag) in
            
            self.hideCentralSpinner()
             // Internet connection goes down.
            if flag == _offline {
                _ = ValidationToast.showStatusMessage(kInternetDown)
                return
            }
            if flag == 200 {
                if let jsonResult = json as? [NSDictionary] {
                    if let firstDict = jsonResult.first {
                        let record = RawdataConverter.string(firstDict["record"])
                        if record.characters.count > 0 {
                         _ =    _ = ValidationToast.showBarMessage(record)
                            return
                        }
                    }
                    if jsonResult.isEmpty{
                        return
                    }
                    self.albums.removeAll()
                    for dict in jsonResult{
                        self.albums.append(VideoYearAlbum(dict: dict))
                    }
                    self.tableView.reloadData()
                }
            }else if let jsonResult = json as? NSDictionary{
                _ =  ValidationToast.showBarMessage(RawdataConverter.string(jsonResult["message"]))
            }else{
                _ =  ValidationToast.showBarMessage(RawdataConverter.string(kSomethingWentWrong))
            }
        }
    }
    
}
