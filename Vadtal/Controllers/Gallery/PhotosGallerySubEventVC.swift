//
//  PhotosGallerySubEventVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 24/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class PhotosGallerySubEventVC: ParentVC {
    
    //MARK:- Outlet
    var  subEvents : [PhotoYearSubEvent] = []
    
    var yearEvent : PhotoYearEvent!
    //MARK:- Variables
    
    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK:- Private
extension PhotosGallerySubEventVC{
    func prepareUI(){
        lblSubTitle.text = yearEvent.event_name
        getEventSubPhotosList()
    }
    
}
// MARK: - TableViewDataSource & Delegate
extension PhotosGallerySubEventVC{
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subEvents.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 152
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! GenericTableViewCell
        let event = subEvents[(indexPath as NSIndexPath).row]
        cell.lblTitle.text = event.gallery_name
        cell.lblSubtitle.text = event.date
        cell.imgv.kf.setImage(with: URL(string: event.gallery_cover_img), placeholder: media_Placeholder, options: nil, progressBlock: nil, completionHandler: nil)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath)
    {
        let vc = UIStoryboard(name: "Gallery", bundle: nil).instantiateViewController(withIdentifier: "PhotosGalleryVC") as! PhotosGalleryVC
        vc.subEvent = subEvents[(indexPath as NSIndexPath).row]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

//MARK:- Web Operation
extension PhotosGallerySubEventVC{
    
    func getEventSubPhotosList(){
        self.showCentralSpinner()
        WebService.wsCall.getEventSubPhotosList(yearEvent.event_code) { (json, flag) in
            self.hideCentralSpinner()
             // Internet connection goes down.
            if flag == _offline {
                _ = ValidationToast.showStatusMessage(kInternetDown)
                return
            }
            if flag == 200 {
                if let jsonResult = json as? [NSDictionary] {
                    if let firstDict = jsonResult.first {
                        let record = RawdataConverter.string(firstDict["record"])
                        if record.characters.count > 0 {
                            _ = ValidationToast.showBarMessage(record)
                            return
                        }
                    }
                    if jsonResult.isEmpty{
                        return
                    }
                    self.subEvents.removeAll()
                    for dict in jsonResult{
                        self.subEvents.append(PhotoYearSubEvent(dict: dict))
                    }
                    self.tableView.reloadData()
                }
            }else if let jsonResult = json as? NSDictionary{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(jsonResult["message"]))
            }else{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(kSomethingWentWrong))
            }
        }
    }
    
}

