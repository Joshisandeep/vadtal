//
//  AudioAlbumGalleryVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 16/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit
import Kingfisher

class AudioAlbumGalleryVC: ParentVC {
    
    //MARK:- Outlet
    
    //MARK:- Variables
    var albums :[AudioAlbum] = []
    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK:- Private
extension AudioAlbumGalleryVC{
    func prepareUI(){
        getAudioAlbumList()
    }
    
}
//MARK:- UICollectionView DataSource & Delegate Method
extension AudioAlbumGalleryVC{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return albums.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! GenericCollectionViewCell
        cell.imgv.kf.setImage(with: URL(string: albums[indexPath.item].cover_img_path), placeholder: media_Placeholder, options: nil, progressBlock: nil, completionHandler: nil)


        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath){
        let vc = UIStoryboard(name: "Gallery", bundle: nil).instantiateViewController(withIdentifier: "AudioAlbumListVC") as! AudioAlbumListVC
        vc.album = albums[(indexPath as NSIndexPath).item]
        self.navigationController?.pushViewController(vc, animated: true)

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets{
        return UIEdgeInsetsMake(10, 10, 10, 10)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        let w = (_screenSize.width - 30)/2
        return CGSize(width: w ,height: w)
    }
}

//MARK:- Web Operation
extension AudioAlbumGalleryVC{
    
    func getAudioAlbumList(){
        self.showCentralSpinner()
        WebService.wsCall.getAudioAlbumList { (json, flag) in
            self.hideCentralSpinner()
             // Internet connection goes down.
            if flag == _offline {
                _ = ValidationToast.showStatusMessage(kInternetDown)
                return
            }
            if flag == 200 {
                if let jsonResult = json as? [NSDictionary] {
                    
                    
                    if let firstDict = jsonResult.first {
                        let record = RawdataConverter.string(firstDict["record"])
                        if record.characters.count > 0 {
                            _ =  ValidationToast.showBarMessage(record)
                            return
                        }
                    }
                    if jsonResult.isEmpty{
                        return
                    }
                    self.albums.removeAll()
                    for dict in jsonResult{
                        self.albums.append(AudioAlbum(dict: dict))
                    }
                    self.collectionView.reloadData()
                    
                }
            }
            else if let jsonResult = json as? NSDictionary{
                _ =  ValidationToast.showBarMessage(RawdataConverter.string(jsonResult["message"]))
            }
            else{
                _ =  ValidationToast.showBarMessage(RawdataConverter.string(kSomethingWentWrong))
            }
        }
    }
}

