//
//  GalleryVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 15/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class GalleryVC: ParentVC {
    
    //MARK:- Outlet
    
    //MARK:- Variables
    let imgName :[String] = ["ic_\(_userSelectedLanguage.getAppPrefix())_btn_photo","ic_\(_userSelectedLanguage.getAppPrefix())_btn_video","ic_\(_userSelectedLanguage.getAppPrefix())_btn_audio"]
    
    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK:- Private
extension GalleryVC{
    func prepareUI(){
        let h = (_screenSize.height - 135 - (80 * 3))/2
        self.tableView.isScrollEnabled = false

        self.tableView.contentInset = UIEdgeInsetsMake(h, 0, h, 0)
    }
    
}
// MARK: - TableViewDataSource & Delegate
extension GalleryVC{
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imgName.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 80
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! GenericTableViewCell
        cell.imgv.image = UIImage(named: imgName[(indexPath as NSIndexPath).row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath)
    {
        switch (indexPath as NSIndexPath).row {
        case 0:
            let vc = UIStoryboard(name: "Gallery", bundle: nil).instantiateViewController(withIdentifier: "PhotosGalleryYearVC") as! PhotosGalleryYearVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case 1:
            let vc = UIStoryboard(name: "Gallery", bundle: nil).instantiateViewController(withIdentifier: "VideoGalleryYearVC") as! VideoGalleryYearVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case 2:
            let vc = UIStoryboard(name: "Gallery", bundle: nil).instantiateViewController(withIdentifier: "AudioAlbumGalleryVC") as! AudioAlbumGalleryVC
            self.navigationController?.pushViewController(vc, animated: true)

            break

        default:
            break
        }
    }
}
