//
//  UpCommingMahostavVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 15/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class UpCommingMahostavVC: ParentVC {
    
    //MARK:- Outlet
    
    //MARK:- Variables
    var arrAlbum :[MahotsavAlbum] = []

    // MARK: - iOS Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 }



//MARK:- Private
extension UpCommingMahostavVC{
    func prepareUI(){
        self.tableView.contentInset = UIEdgeInsetsMake(15, 0, 15, 0)
        getMahotsavAlbumList()
    }
    
}
// MARK: - TableViewDataSource & Delegate
extension UpCommingMahostavVC{
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAlbum.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 130
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! GenericTableViewCell
        
        cell.imgv.kf.setImage(with: URL(string: arrAlbum[indexPath.row].cover_img_path), placeholder: media_Placeholder, options: nil, progressBlock: nil, completionHandler: nil)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath)
    {
        let vc = UIStoryboard(name: "UpCommingMahotsav", bundle: nil).instantiateViewController(withIdentifier: "UpCommingMahostavDetailVC") as! UpCommingMahostavDetailVC
        vc.album = arrAlbum[(indexPath as NSIndexPath).row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- Web Operation
extension UpCommingMahostavVC{
    
    func getMahotsavAlbumList(){
        self.showCentralSpinner()
        WebService.wsCall.getMahotsavAlbumList { (json, flag) in
            self.hideCentralSpinner()
             // Internet connection goes down.
            if flag == _offline {
                _ = ValidationToast.showStatusMessage(kInternetDown)
                return
            }
            if flag == 200 {
                if let jsonResult = json as? [NSDictionary] {
                    
                    
                    if let firstDict = jsonResult.first {
                        let record = RawdataConverter.string(firstDict["record"])
                        if record.characters.count > 0 {
                            _ = ValidationToast.showBarMessage(record)
                            return
                        }
                    }
                    if jsonResult.isEmpty{
                        return
                    }
                    self.arrAlbum.removeAll()
                    for dict in jsonResult{
                        self.arrAlbum.append(MahotsavAlbum(dict: dict))
                    }
                    self.tableView.reloadData()
                    
                }
            }
            else if let jsonResult = json as? NSDictionary{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(jsonResult["message"]))
            }
            else{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(kSomethingWentWrong))
            }
        }
    }
}
