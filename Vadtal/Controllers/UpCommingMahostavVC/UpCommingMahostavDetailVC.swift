//
//  UpCommingMahostavDetailVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 21/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class UpCommingMahostavDetailVC: ParentVC {
    
    //MARK:- Outlet
    
    //MARK:- Variables
    var album :MahotsavAlbum!
    var images :[MahotsavImage] = []

    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
}

}

//MARK:- Private
extension UpCommingMahostavDetailVC{
    func prepareUI(){
        getMahotsavAlbumDetail()
    }
    
}
//MARK:- UICollectionView DataSource & Delegate Method
extension UpCommingMahostavDetailVC{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! GenericCollectionViewCell
        
        cell.imgv.kf.setImage(with: URL(string: images[indexPath.item].photopath), placeholder: media_Placeholder, options: nil, progressBlock: nil, completionHandler: nil)

        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath){
        let vc = UIStoryboard(name: "Zoom", bundle: nil).instantiateViewController(withIdentifier: "FullScreenPhotoVC") as! FullScreenPhotoVC
        vc.initialViewIndex = (indexPath as NSIndexPath).row
        vc.imagePaths = images.map{return $0.photopath}
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets{
        return UIEdgeInsetsMake(10, 10, 10, 10)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        let w = (_screenSize.width - 30)/2
        return CGSize(width: w ,height: w)
    }
}

//MARK:- Web Operation
extension UpCommingMahostavDetailVC{
    
    func getMahotsavAlbumDetail(){
        self.showCentralSpinner()
        WebService.wsCall.getMahotsavAlbumDetail(album.mahotsav_code) { (json, flag) in
            self.hideCentralSpinner()
             // Internet connection goes down.
            if flag == _offline {
                _ = ValidationToast.showStatusMessage(kInternetDown)
                return
            }
            if flag == 200 {
                if let jsonResult = json as? [NSDictionary] {
                    
                    
                    if let firstDict = jsonResult.first {
                        let record = RawdataConverter.string(firstDict["record"])
                        if record.characters.count > 0 {
                           _ = ValidationToast.showBarMessage(record)
                            return
                        }
                    }
                    if jsonResult.isEmpty{
                        return
                    }
                    self.images.removeAll()
                    for dict in jsonResult{
                        self.images.append(MahotsavImage(dict: dict))
                    }
                    self.collectionView.reloadData()
                    
                }
            }
            else if let jsonResult = json as? NSDictionary{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(jsonResult["message"]))
            }
            else{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(kSomethingWentWrong))
            }
        }
    }
}
