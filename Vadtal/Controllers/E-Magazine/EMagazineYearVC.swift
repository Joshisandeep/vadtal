//
//  EMagazineYearVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 22/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class EMagazineYearVC: ParentVC {
    
    //MARK:- Outlet
    
    //MARK:- Variables
    var years :[MagazineYear] = []
    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getMagazineYearList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

//MARK:- UICollectionView DataSource & Delegate Method
extension EMagazineYearVC{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return years.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! GenericCollectionViewCell
        cell.lblTitle.text = years[(indexPath as NSIndexPath).item].year
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath){
        let vc = UIStoryboard(name: "EMagazine", bundle: nil).instantiateViewController(withIdentifier: "EMagazineVC") as! EMagazineVC
        vc.magazineYear = years[(indexPath as NSIndexPath).item]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets{
        let pending = (_screenSize.width - (150 * 2))/3
        return UIEdgeInsetsMake(10, pending, 10, pending)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        return CGSize(width: 150 ,height: 90)
    }
}

//MARK:- Web Operation
extension EMagazineYearVC{
    
    func getMagazineYearList(){
        self.showCentralSpinner()
        WebService.wsCall.getMagazineYearList { (json, flag) in
            self.hideCentralSpinner()
             // Internet connection goes down.
            if flag == _offline {
                _ = ValidationToast.showStatusMessage(kInternetDown)
                return
            }
            if flag == 200 {
                if let jsonResult = json as? [NSDictionary] {
                    
                    
                    if let firstDict = jsonResult.first {
                        let record = RawdataConverter.string(firstDict["record"])
                        if record.characters.count > 0 {
                            _ = ValidationToast.showBarMessage(record)
                            return
                        }
                    }
                    if jsonResult.isEmpty{
                        return
                    }
                    self.years.removeAll()
                    for dict in jsonResult{
                        self.years.append(MagazineYear(dict: dict))
                    }
                    self.collectionView.reloadData()
                    
                }
            }
            else if let jsonResult = json as? NSDictionary{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(jsonResult["message"]))
            }
            else{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(kSomethingWentWrong))
            }
        }
    }
}
