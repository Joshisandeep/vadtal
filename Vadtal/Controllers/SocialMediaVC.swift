//
//  SocialMediaVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 15/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit
import SafariServices

class SocialMediaVC: ParentVC {
    
    //MARK:- Outlet
    
    //MARK:- Variables
    
    // MARK: - iOS Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

//MARK:- Private
extension SocialMediaVC{
    func prepareUI(){
        self.tableView.contentInset = UIEdgeInsetsMake(15, 0, 15, 0)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 50

    }
    
    func openSocialPage(_ url:String){
        if #available(iOS 9.0, *) {
            let vc = SFSafariViewController(url: URL(string: url)!, entersReaderIfAvailable: true)
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)

        } else {
            // Fallback on earlier versions
            if _application.canOpenURL(URL(string: url)!)
            {
                _application.openURL(URL(string: url)!);
            }
        }
    }
}
// MARK: - TableViewDataSource & Delegate
extension SocialMediaVC{
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
        
    }
  
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell\((indexPath as NSIndexPath).row)", for: indexPath) as! GenericTableViewCell
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath)
    {
        switch (indexPath as NSIndexPath).row {
        
        case 1:
            self.openSocialPage(Facebook_Page_URL)
            break
        case 2:
            self.openSocialPage(Instagram_Page_URL)

         break
        case 3:
            self.openSocialPage(Youtube_Page_URL)
            break
            
        default:
            break
        }
    }
}

//MARK:- SFSafariViewController Delegate
@available(iOS 9.0, *)
extension SocialMediaVC : SFSafariViewControllerDelegate{
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        self.dismiss(animated: true, completion: nil)
    }

}
