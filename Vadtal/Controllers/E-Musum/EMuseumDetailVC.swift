//
//  EMusumDetailVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 16/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit
import Kingfisher

class EMuseumDetailVC: ParentVC {
    
    //MARK:- Outlet
    
    //MARK:- Variables
    var musium :Musium!

    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


//MARK:- Private
extension EMuseumDetailVC{
    func prepareUI(){
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 15, 0)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 150
        lblTitle.text = musium.title()
    }
    
}
// MARK: - TableViewDataSource & Delegate
extension EMuseumDetailVC{
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
        
    }
   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! GenericTableViewCell
        cell.lblTitle.text = musium.desc()
        cell.imgv.kf.setImage(with: musium.getImagePathURL(), placeholder: media_Placeholder, options: nil, progressBlock: nil, completionHandler: nil)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath)
    {
        
    }
}
