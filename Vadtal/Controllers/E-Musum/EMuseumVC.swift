//
//  EMusumVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 16/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class EMuseumVC: ParentVC {
    
    //MARK:- Outlet
    
    //MARK:- Variables
    var arrMusium :[Musium] = []
    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


//MARK:- Private
extension EMuseumVC{
    func prepareUI(){
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 15, 0)
        getMuseumList()
    }
    
}
// MARK: - TableViewDataSource & Delegate
extension EMuseumVC{
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMusium.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 80
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! GenericTableViewCell
        let musium = arrMusium[(indexPath as NSIndexPath).row]
        cell.lblTitle.text = musium.title()
        cell.imgv.kf.setImage(with: musium.getImagePathURL(), placeholder: media_Placeholder, options: nil, progressBlock: nil, completionHandler: nil)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath)
    {
        let vc = UIStoryboard(name: "EMuseum", bundle: nil).instantiateViewController(withIdentifier: "EMuseumDetailVC") as! EMuseumDetailVC
        vc.musium = arrMusium[(indexPath as NSIndexPath).row]
        self.navigationController?.pushViewController(vc, animated: true)

    }
}

//MARK:- Web Operation
extension EMuseumVC{
    
    func getMuseumList(){
        self.showCentralSpinner()
        WebService.wsCall.getMuseumList { (json, flag) in
            self.hideCentralSpinner()
             // Internet connection goes down.
            if flag == _offline {
                _ = ValidationToast.showStatusMessage(kInternetDown)
                return
            }
            if flag == 200 {
                if let jsonResult = json as? NSDictionary {
                    if RawdataConverter.string(jsonResult["return"]) != "true" {
                        return
                    }
                    
                    if let arrData = jsonResult["data"] as? [NSDictionary] , !arrData.isEmpty{
                        self.arrMusium.removeAll()
                        for dict in arrData{
                            self.arrMusium.append(Musium(dict: dict))
                        }
                    }
                    self.tableView.reloadData()
                }
            }
            else if let jsonResult = json as? NSDictionary{
                _ =  ValidationToast.showBarMessage(RawdataConverter.string(jsonResult["message"]))
            }
            else{
                _ =  ValidationToast.showBarMessage(RawdataConverter.string(kSomethingWentWrong))
            }
        }
    }
}
