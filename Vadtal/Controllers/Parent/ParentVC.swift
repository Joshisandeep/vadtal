//
//  KPParentViewController.swift
//  VoteMe
//
//  Created by Yudiz Solutions Pvt.Ltd. on 30/03/16.
//  Copyright © 2016 Yudiz Solutions Pvt.Ltd. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ParentVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet var tableView :UITableView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblSubTitle : UILabel!

    @IBOutlet var imgvHeader : UIImageView?

    @IBOutlet var viewHeader : UIView?
    @IBOutlet var viewBottom : UIView?
    
    @IBOutlet var horizontalConstraints: [NSLayoutConstraint]?
    @IBOutlet var verticalConstraints: [NSLayoutConstraint]?
    
    // MARK: Variables
    var toolBar : ToolBarView!
    let referesh = UIRefreshControl()


    // MARK: - iOS Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        constraintUpdate()
        setDefaultUI()
        jprint("Allocated: \(self.classForCoder)")
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        _ = super.preferredStatusBarStyle
        return .lightContent
    }
    
    override var prefersStatusBarHidden : Bool{
        _ = super.prefersStatusBarHidden
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
   
  
       deinit {
        jprint("Deallocated: \(self.classForCoder)")
       _defaultCenter.removeObserver(self)
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: - Lazy Variables
    lazy internal var activityIndicator : CustomActivityIndicatorView = {
        let image : UIImage = UIImage(named: kActivityButtonImageName)!
        return CustomActivityIndicatorView(image: image)
    }()
    
    lazy internal var centralActivityIndicator : CustomActivityIndicatorView = {
        let image : UIImage = UIImage(named: kActivityCenterBigImageName)!
        return CustomActivityIndicatorView(image: image)
    }()
    
    
}
//MARK: - UI Releated Method
extension ParentVC {
    // This will update constaints and shrunk it as device screen goes lower.
    func constraintUpdate() {
        if let hConts = horizontalConstraints {
            for const in hConts {
                let v1 = const.constant
                let v2 = v1 * _widthRatio
                const.constant = v2
            }
        }
        if let vConst = verticalConstraints {
            for const in vConst {
                let v1 = const.constant
                let v2 = v1 * _heighRatio
                const.constant = v2
            }
        }
    }
    
    // Set tableView
    func setDefaultUI() {
        tableView?.scrollsToTop = true;
        toolBar = ToolBarView.instantiateViewFromNib()

    }
 
    func setHeaderShadow(){
        self.viewHeader?.layer.shadowColor = UIColor.black.cgColor
        self.viewHeader?.layer.shadowOpacity = 0.4
        self.viewHeader?.layer.shadowOffset = CGSize(width: 0, height: 2)
        //self.lblTitle?.font = UIFont.muAvenirMedium(18.0)
    }
    
    func setButtomShadow(){
        self.viewBottom?.layer.shadowColor = UIColor.black.cgColor
        self.viewBottom?.layer.shadowOpacity = 0.4
        self.viewBottom?.layer.shadowOffset = CGSize(width: 0, height: 0)
    }
    
    
    // MARK: - TableView
    func tableViewCell(_ indexPath: IndexPath) -> UITableViewCell?  {
        let cell = tableView.cellForRow(at: indexPath)
        return cell
    }
    func tableTextFiledInputCell(_ index: Int,section: Int = 0) -> TextFiledInputCell? {
        return tableView.cellForRow(at: IndexPath(row: index, section: section)) as? TextFiledInputCell
    }
    func tableTextViewInputCell(_ index: Int,section: Int = 0) -> TextViewInputCell? {
        return tableView.cellForRow(at: IndexPath(row: index, section: section)) as? TextViewInputCell
    }
    func scrollToIndex(_ index: Int,section: Int = 0, animate: Bool = false){
        let indexPath = IndexPath(item: index, section: section)
        tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: animate)
    }
    
    func scrollToTop(_ animate: Bool = false){
        if tableView?.numberOfRows(inSection: 0) > 0{
            tableView?.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: animate)
        }else{
            tableView?.contentOffset = CGPoint.zero
        }
    }
    
    func scrollToTopCollection(_ animate: Bool = false){
        if collectionView?.numberOfItems(inSection: 0) > 0{
            collectionView?.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: animate)
        }else{
            collectionView?.contentOffset = CGPoint.zero
        }
    }
    
    func scrollToBottom(_ animate: Bool = false){
        if tableView?.numberOfRows(inSection: 0) > 0{
            tableView?.scrollToRow(at: IndexPath(row: tableView.numberOfRows(inSection: 0) - 1, section: 0), at: .top, animated: animate)
        }
    }
    
    // MARK: - Spinner
    
    // This will show and hide spinner. In middle of container View
    // You can pass any view here, Spinner will be placed there runtime and removed on hide.
    func showSpinnerIn(_ container: UIView, control: UIButton, isCenter: Bool = true) {
        container.addSubview(activityIndicator)
        let xPoint: CGFloat!
        if isCenter {
            xPoint = -10
        }else{
            control.isSelected = true
            let str = control.title(for: .selected)
            control.contentEdgeInsets = UIEdgeInsetsMake(0, -30, 0, 0)
            xPoint = (str!.WidthWithNoConstrainedHeight((control.titleLabel?.font)!)/2) - 5
        }
        
        let xConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: container, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: xPoint)
        let yConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: container, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: -10)
        
        let wConstraint = NSLayoutConstraint(item: centralActivityIndicator, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 22)
        let hConstraint = NSLayoutConstraint(item: centralActivityIndicator, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 22)
        
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([xConstraint, yConstraint,wConstraint,hConstraint])
        self.view.layoutIfNeeded()
        
        activityIndicator.alpha = 0.0
        self.view.isUserInteractionEnabled = false
        activityIndicator.startAnimating()
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.activityIndicator.alpha = 1.0
            if isCenter{
                control.alpha = 0.0
            }
        }) 
    }
    
    func hideSpinnerIn(_ container: UIView, control: UIButton) {
        self.view.isUserInteractionEnabled = true
        activityIndicator.stopAnimating()
        control.isSelected = false
        control.contentEdgeInsets = UIEdgeInsets.zero
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.activityIndicator.alpha = 0.0
            control.alpha = 1.0
        }) 
    }
    
    func showCentralSpinner(_ userInteractionEnabled : Bool! = false) {
        self.view.addSubview(centralActivityIndicator)
        
        let xConstraint = NSLayoutConstraint(item: centralActivityIndicator, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
        let yConstraint = NSLayoutConstraint(item: centralActivityIndicator, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0)
        let wConstraint = NSLayoutConstraint(item: centralActivityIndicator, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 40)
        let hConstraint = NSLayoutConstraint(item: centralActivityIndicator, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 40)
        
        centralActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([xConstraint, yConstraint,wConstraint, hConstraint])
        self.view.layoutIfNeeded()
        centralActivityIndicator.alpha = 0.0
        self.view.isUserInteractionEnabled = userInteractionEnabled
        centralActivityIndicator.startAnimating()
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.centralActivityIndicator.alpha = 1.0
        }) 
    }
    
    func hideCentralSpinner() {
        self.view.isUserInteractionEnabled = true
        centralActivityIndicator.stopAnimating()
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.centralActivityIndicator.alpha = 0.0
        }) 
    }
}
//MARK:- Localization releated 

extension String{
    func localizationString() -> String{
        if _userSelectedLanguage == .gujarati{
            switch self {
            case "Ekadasi":
                return "એકાદશી"
                
            case "Poonam":
                return "પૂનમ"
                            
            case "Tithi":
                return "એકાદશી"
                
            case "Chandra":
                return "એકાદશી"
                
            default:
                return self
            }
        }
        
        return self
     }
}
// MARK: - Keyboard Functions
extension ParentVC {
    func setKeyboardNotifications() {
        _defaultCenter.addObserver(self, selector: #selector(ParentVC.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        _defaultCenter.addObserver(self, selector: #selector(ParentVC.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(_ notification: Foundation.Notification) {
        if let kbSize = ((notification as NSNotification).userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: kbSize.height, right: 0)
        }
    }
    
    func keyboardWillHide(_ notification: Foundation.Notification) {
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
//MARK: - UITextField Delegate
extension ParentVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if string == " " && range.location == 0{
            return false
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//MARK: - UITextView Delegate
extension ParentVC: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        textView.scrollRangeToVisible(range)
        if (text == " " || text == "\n") && range.location == 0{
            return false
        }
        return true
    }
    
}
// MARK: - TableViewDataSource & Delegate
extension ParentVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        return cell
    }
    
}
// MARK: - Actions
extension ParentVC{
    // Navigate to Previous View Controller with navigation popview method
    @IBAction func parentBackAction(_ sender:UIButton ){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // Navigate to Previous View Controller with dismiss view method
    @IBAction func parentDismissAction(_ sender:UIButton!){
        self.dismiss(animated: true, completion: nil)
    }
    
    // Navigate to Root view controller
    @IBAction func parentBackToRootViewController(_ sender: UIButton!){
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
}

