//
//  HomeVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 14/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

enum HomeMenuItem: String {
    case Daily_Darshan                          = "DAILY DARSHAN"
    case Acharya_Mahraj_Desk                    = "ACHARYA MAHRAJ’S DESK"
    case E_Museum                               = "E-MUSEUM"
    case Nirnay                                 = "NIRNAY"
    case Gallery                                = "GALLERY"
    case Upcoming_Mahotsav                      = "UPCOMING MAHOTSAV"
    case Mandir_Parisar                         = "MANDIR PARISAR"
    case E_Magazine                             = "E-MAGAZINE"
    case E_Library                              = "E-LIBRARY"
    case More                                   = "MORE"
    
    
    static let allValues : [[HomeMenuItem]] = [[Daily_Darshan, Acharya_Mahraj_Desk], [ E_Museum, Nirnay], [Gallery, Upcoming_Mahotsav], [E_Magazine, E_Library], [More]]
    
    func getImageName()-> String{
        let imgName = "ic_\(_userSelectedLanguage.getAppPrefix())_"
        switch self {
        case .Daily_Darshan:
            return imgName + "daily_darshan"
            
        case .Acharya_Mahraj_Desk:
            return imgName + "acharya"
            
        case .E_Museum:
            return imgName + "museum"
            
        case .Nirnay:
            return imgName + "nirnay"
            
        case .Gallery:
            return imgName + "gallery"
            
        case .Upcoming_Mahotsav:
            return imgName + "upcoming_mahotsav"
            
        case .Mandir_Parisar:
            return imgName + "marndir"
            
        case .E_Magazine:
            return imgName + "emagazine"
            
        case .E_Library:
            return imgName + "elib"
            
        case .More:
            return imgName + "more"
        }
       
    }

}


class HomeVC: ParentVC {
    
    //MARK:- Outlet
    let menuItems = HomeMenuItem.allValues

    //MARK:- Variables
    
    // MARK: - iOS Life Cycle   
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
//MARK:- Private
extension HomeVC{
    func prepareUI(){
        self.tableView.contentInset = UIEdgeInsetsMake(15, 0, 15, 0)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 70
    }
    
    func prepareToNavigate(_ button :UIButton){
        let indexpath = IndexPath().indexPathForCellContainingView(button, inTableView: tableView)
        guard let index = indexpath else {
            return
        }
        switch (index as NSIndexPath).section {
        case 0:
            if button.tag == 1{
                let vc = UIStoryboard(name:"AcharyaMaharajDesk", bundle: nil).instantiateViewController(withIdentifier: "AcharyaMaharajDeskVC") as! AcharyaMaharajDeskVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let vc = UIStoryboard(name:"DailyDarshan", bundle: nil).instantiateViewController(withIdentifier: "DailyDarshanVC") as! DailyDarshanVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            break
            
        case 1:
            if button.tag == 1{
                
                let vc = UIStoryboard(name: "Nirnay", bundle: nil).instantiateViewController(withIdentifier: "NirnayVC") as! NirnayVC
                self.navigationController?.pushViewController(vc, animated: true)

            }else{
                let vc = UIStoryboard(name: "EMuseum", bundle: nil).instantiateViewController(withIdentifier: "EMuseumVC") as! EMuseumVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            break
            
        case 2:
            if button.tag == 1{
                let vc = UIStoryboard(name: "UpCommingMahotsav", bundle: nil).instantiateViewController(withIdentifier: "UpCommingMahostavVC")
                self.navigationController?.pushViewController(vc, animated: true)

            }else{
                let vc = UIStoryboard(name: "Gallery", bundle: nil).instantiateViewController(withIdentifier: "GalleryVC")
                self.navigationController?.pushViewController(vc, animated: true)
            }
            break
        case 3:
            if button.tag == 0{
                let vc = UIStoryboard(name: "EMagazine", bundle: nil).instantiateViewController(withIdentifier: "EMagazineYearVC") as! EMagazineYearVC
                self.navigationController?.pushViewController(vc, animated: true)

            }else{
                let vc = UIStoryboard(name: "ELibrary", bundle: nil).instantiateViewController(withIdentifier: "ELibraryVC") as! ELibraryVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            break
            
        case 4:
//            if button.tag == 1{
                let vc = UIStoryboard(name: "More", bundle: nil).instantiateViewController(withIdentifier: "MoreVC")
                self.navigationController?.pushViewController(vc, animated: true)
//            }else{
//                let vc = UIStoryboard(name: "ELibrary", bundle: nil).instantiateViewController(withIdentifier: "ELibraryVC") as! ELibraryVC
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
            break
        default:
            break
        }
    }

}
// MARK: - TableViewDataSource & Delegate
extension HomeVC{
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        return menuItems.count
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
        
    }
   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! HomeCell
        cell.prepareFillData(menuItems[(indexPath as NSIndexPath).section])
        
        cell.handleBlockAction { (tapped, button) in
            if tapped == .action{
               self.prepareToNavigate(button)
            }
        }
        return cell
    }
    
}

