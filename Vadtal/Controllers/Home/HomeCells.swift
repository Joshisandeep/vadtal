//
//  HomeCells.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 14/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class HomeCell:  ConstrainedTableViewCell {
    @IBOutlet var btns: [UIButton]!
    
    var actionBlock : ((_ tapped: Tapped, _ button:UIButton) -> ())?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func prepareFillData(_ images:[HomeMenuItem]){
        btns[0].setImage(UIImage(named: images[0].getImageName()), for: UIControlState())
        if images.count > 1{
            btns[1].setImage(UIImage(named: images[1].getImageName()), for: UIControlState())
        }
        btns[1].isHidden = images.count == 1
        btns[0].tag = 0
        btns[1].tag = 1

    }
    
    // MARK: Method
    func handleBlockAction(_ block: @escaping (_ tapped: Tapped,_ button:UIButton) -> ()){
        actionBlock = block
    }
    
    
    // MARK: Button Actions
    @IBAction func btnTapped(_ sender: UIButton!) {
        actionBlock?(Tapped.action, sender)
    }
    
}
