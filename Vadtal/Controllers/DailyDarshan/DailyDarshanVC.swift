//
//  DailyDarshanVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 15/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class DailyDarshanVC: ParentVC {
    
    //MARK:- Outlet
    @IBOutlet var btnShayan : UIButton!
    @IBOutlet var btnLiveDarshan : UIButton!
    @IBOutlet var btnPrevMonth : UIButton!
    @IBOutlet var btnNextMonth : UIButton!

    //MARK:- Variables
    var arrDarshan :[Darshan] = []
    var nirnay : Nirnay!
    var darshanDate : Date = Date()
    // MARK: - iOS Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
        getDailyDarshan()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

//MARK:- Private
extension DailyDarshanVC{
    func prepareUI(){
        btnShayan.setImage(UIImage(named: "ic_\(_userSelectedLanguage.getAppPrefix())_btn_shayan"), for: UIControlState())
        btnLiveDarshan.setImage(UIImage(named: "ic_\(_userSelectedLanguage.getAppPrefix())_btn_live_darshan"), for: UIControlState())
        updateData()
    }
    
    func updateData(){
        if let obj = nirnay{
            lblSubTitle.text = obj.getDisplayTitle()
        }else{
            lblSubTitle.text = ""
        }
        self.collectionView.reloadData()
    }
    
}
// MARK: - Actions
extension DailyDarshanVC{
    @IBAction func btnShayanAction(_ sender:UIButton ){
        let vc = UIStoryboard(name: "DailyDarshan", bundle: nil).instantiateViewController(withIdentifier: "DailyDarshanShayanVC") as! DailyDarshanShayanVC
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func btnLiveDarshanAction(_ sender:UIButton ){
        let vc = UIStoryboard(name: "DailyDarshan", bundle: nil).instantiateViewController(withIdentifier: "DailyDarshanLiveVC") as! DailyDarshanLiveVC
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func btnPrevMonthTapped(_ sender:UIButton ){
        darshanDate = darshanDate.befor1dayDate()
        self.getDailyDarshan()

    }
    
    @IBAction func btnNextMonthTapped(_ sender:UIButton!){
        
        let result  = darshanDate.commpareWithCurrentDate()

        switch result {
        case .orderedSame, .orderedDescending:
            return
        case .orderedAscending:
            darshanDate = darshanDate.After1dayDate()
            break
        }
        self.getDailyDarshan()
    }
}


//MARK:- UICollectionView DataSource & Delegate Method
extension DailyDarshanVC{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrDarshan.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! GenericCollectionViewCell
        cell.imgv.kf.setImage(with: URL(string: arrDarshan[indexPath.item].photopath), placeholder: media_Placeholder, options: nil, progressBlock: nil, completionHandler: nil)

        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath){
        let vc = UIStoryboard(name: "Zoom", bundle: nil).instantiateViewController(withIdentifier: "FullScreenPhotoVC") as! FullScreenPhotoVC
        vc.initialViewIndex = (indexPath as NSIndexPath).row
        vc.imagePaths = arrDarshan.map{return $0.photopath}
        self.navigationController?.pushViewController(vc, animated: true)

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets{
        return UIEdgeInsetsMake(10, 10, 10, 10)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        let w = (_screenSize.width - 45)/3
        return CGSize(width: w ,height: w)
    }
}

//MARK:- Web Operation
extension DailyDarshanVC{
    
    func getDailyDarshan(){
        self.showCentralSpinner()

        WebService.wsCall.getDailyDarshan(darshanDate.dateWithStringFormat("dd-MM-yyyy")) { (json, flag) in
            self.hideCentralSpinner()
             // Internet connection goes down.
            if flag == _offline {
                _ = ValidationToast.showStatusMessage(kInternetDown)
                return
            }
            if flag == 200 {
                if let jsonResult = json as? NSDictionary {
                    self.arrDarshan.removeAll()
                    self.nirnay = nil
                    if let firstDict = jsonResult["0"] as? NSDictionary {
                        let record = RawdataConverter.string(firstDict["record"])
                        if record.characters.count > 0 {
                            _ = ValidationToast.showBarMessage(record)
                            self.updateData()
                            return
                        }
                    }
                    if let arrPhotos = jsonResult["p"] as? [NSDictionary]  {
                        for dict in arrPhotos{
                            self.arrDarshan.append(Darshan(dict: dict))
                        }

                    }
                    if let dict = jsonResult["d"] as? NSDictionary {
                        self.nirnay = Nirnay(dict: dict)
                    }
                    self.updateData()
                    
                }
            }
            else if let jsonResult = json as? NSDictionary{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(jsonResult["message"]))
            }
            else{
                _ = ValidationToast.showBarMessage(RawdataConverter.string(kSomethingWentWrong))
            }
        }
    }
}
