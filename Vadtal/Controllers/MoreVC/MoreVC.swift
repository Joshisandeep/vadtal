//
//  MoreVC.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 15/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class MoreVC: ParentVC {
    
    //MARK:- Outlet
    
    //MARK:- Variables
    let imgName :[String] = ["ic_\(_userSelectedLanguage.getAppPrefix())_btn_social_media","ic_\(_userSelectedLanguage.getAppPrefix())_btn_contact","ic_\(_userSelectedLanguage.getAppPrefix())_btn_feedback","ic_\(_userSelectedLanguage.getAppPrefix())_btn_share_app"]
    
    // MARK: - iOS Life Cycle
    
    override func viewDidLoad() {
        prepareUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK:- Private
extension MoreVC{
    func prepareUI(){
        let h = (_screenSize.height - 135 - (80 * 4))/2
        self.tableView.isScrollEnabled = false
        self.tableView.contentInset = UIEdgeInsetsMake(h, 0, h, 0)
    }
    
}
// MARK: - TableViewDataSource & Delegate
extension MoreVC{
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imgName.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 80
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! GenericTableViewCell
        cell.imgv.image = UIImage(named: imgName[(indexPath as NSIndexPath).row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath)
    {
        switch (indexPath as NSIndexPath).row {
        case 0:
            let vc = UIStoryboard(name: "More", bundle: nil).instantiateViewController(withIdentifier: "SocialMediaVC") as! SocialMediaVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case 1:
            
            let vc = UIStoryboard(name: "More", bundle: nil).instantiateViewController(withIdentifier: "ContactUSVC") as! ContactUSVC
            self.navigationController?.pushViewController(vc, animated: true)
            
            break
        case 2:
            
            let vc = UIStoryboard(name: "More", bundle: nil).instantiateViewController(withIdentifier: "FeedbackVC") as! FeedbackVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case 3:
//            ShareController.shared.openShareKit(kShare_APP_Message, toController: self, compilation: { (success) in
//                
//            })
            ShareController.shared.openShareKit([kShare_APP_Message], toController: self, compilation: { (success) in
                
            })
            break
            
        default:
            break
        }
    }
}

