//
//  VideoPlayer.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 18/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import UIKit

class VideoPlayerVC: ParentVC {
    // MARK: - Outlets
    
    // MARK: - Varible
    var videoID :String!
    // MARK: - iOS Life Cycle 
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

// MARK: - Private
extension VideoPlayerVC{
    fileprivate func preparePlayer(){
    }
}
// MARK: - TableViewDataSource & Delegate
extension VideoPlayerVC{
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        if videoID == nil{
            return 0
        }
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return tableView.bounds.size.height
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PlayerCell
        cell.prepareToPlay(videoID)
        
        return cell
    }
    
}
