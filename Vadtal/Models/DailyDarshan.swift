//
//  DailyDarshan.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 19/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import Foundation

// MARK:- Darshan Class
class Darshan: NSObject {
    
    var photopath           : String!
    
    init(dict: NSDictionary) {
        photopath   = RawdataConverter.string(dict["photopath"])
    }
    
}


// MARK:- MahotsavAlbum Class
class MahotsavAlbum: NSObject {
    
    var mahotsav_code           : String!
    var mahotsav_name           : String!
    var mahotsav_desc           : String!
    var cover_img               : String!
    var cover_img_path          : String!
    var mahotsav_dt             : String!
    var year                    : String!
    var month                   : String!
    var date                    : Date?
    
    init(dict: NSDictionary) {
        mahotsav_code = RawdataConverter.string(dict["mahotsav_code"])
        mahotsav_name = RawdataConverter.string(dict["mahotsav_name"])
        mahotsav_desc = RawdataConverter.string(dict["mahotsav_desc"])
        cover_img = RawdataConverter.string(dict["cover_img"])
        cover_img_path = RawdataConverter.string(dict["cover_img_path"])
        mahotsav_dt = RawdataConverter.string(dict["mahotsav_dt"])
        year = RawdataConverter.string(dict["year"])
        month = RawdataConverter.string(dict["month"])
        let dt = RawdataConverter.string(dict["mahotsav_dt"])
        
        if dt != "00-00-0000" && !dt.isEmpty  {
            date = Date.dateFromFormatted1_String(dt)
        }
    }
    
}


// MARK:- MahotsavImage Class
class MahotsavImage: NSObject {
    
    var mahotsav_code           : String!
    var mahotsav_dt_code        : String!
    var photo                   : String!
    var photopath               : String!
    
    init(dict: NSDictionary) {
        mahotsav_code = RawdataConverter.string(dict["mahotsav_code"])
        mahotsav_dt_code = RawdataConverter.string(dict["mahotsav_dt_code"])
        photo = RawdataConverter.string(dict["photo"])
        photopath = RawdataConverter.string(dict["photopath"])
        
    }
}



// MARK:- Musium Class
class Musium: NSObject {
    
    var mid                               : String!
    var eng_title                         : String!
    var eng_description                   : String!
    var guj_title                         : String!
    var guj_description                   : String!
    var image_path                        : String!
    var c_date                            : String!
    var update_date                       : String!
    var status                            : String!
    
    var tCreateDate                    : Date?
    var tUpdateDate                    : Date?
    
    init(dict: NSDictionary) {
        mid = RawdataConverter.string(dict["mid"])
        eng_title = RawdataConverter.string(dict["eng_title"])
        eng_description = RawdataConverter.string(dict["eng_description"])
        guj_title = RawdataConverter.string(dict["guj_title"])
        guj_description = RawdataConverter.string(dict["guj_description"])
        image_path  = RawdataConverter.string(dict["image_path"])
        c_date      = RawdataConverter.string(dict["c_date"])
        update_date = RawdataConverter.string(dict["update_date"])
        status      = RawdataConverter.string(dict["status"])
        
        let tCreate = RawdataConverter.string(dict["c_date"])
        let tUpdate = RawdataConverter.string(dict["update_date"])
        
        if tCreate != "00-00-0000 00:00:00" && !tCreate.isEmpty  {
            tCreateDate = Date.dateFromFormatted1_String(tCreate)
        }
        if tUpdate != "00-00-0000 00:00:00" && !tUpdate.isEmpty  {
            tUpdateDate = Date.dateFromFormatted1_String(tUpdate)
        }
        
    }
    
    func title() -> String{
        if _userSelectedLanguage == .english{
            return eng_title
        }
        return guj_title
    }
    
    func desc() -> String{
        if _userSelectedLanguage == .english{
            return eng_description
        }
        return guj_description
    }
    
    func isStatus() -> Bool{
        if status == "Active"{
            return true
        }
        return false
    }
    
    
    func getImagePathURL() -> URL?{
        let path = "https://vadtalmandir.org/admin/upload/images/" + self.image_path
        return URL(string: path)
    }
}



// MARK:- Notification Class
class Notification: NSObject {
    
    var event_code              : String!
    var event_name              : String!
    var event_desc              : String!
    var event_dt                : String!
    var year                    : String!
    var month                   : String!
    var create_date             : String!
    
    var tCreateDate             : Date?
    var tEventDate             : Date?

    init(dict: NSDictionary) {
        event_code  = RawdataConverter.string(dict["event_code"])
        event_name  = RawdataConverter.string(dict["event_name"])
        event_desc  = RawdataConverter.string(dict["event_desc"])
        event_dt    = RawdataConverter.string(dict["event_dt"])
        year        = RawdataConverter.string(dict["year"])
        month       = RawdataConverter.string(dict["month"])
        create_date = RawdataConverter.string(dict["create_date"])
        
        let tCreate = RawdataConverter.string(dict["create_date"])
        let tEvent = RawdataConverter.string(dict["event_dt"])
        
        if tCreate != "00-00-0000 00:00:00" && !tCreate.isEmpty  {
            tCreateDate = Date.dateFromFormatted1_String(tCreate)
        }
        if tEvent != "00-00-0000 00:00:00" && !tEvent.isEmpty  {
            tEventDate = Date.dateFromFormatted1_String(tEvent)
        }
    }
}
