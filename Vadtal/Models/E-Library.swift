//
//  E-Library.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 22/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import Foundation




// MARK:- Library Class
class Library: NSObject {
    
    var id                  : String!
    var lng            : String!
    var title               : String!
    var cover_img           : String!
    var cover_img_path      : String!
    var pdf_file            : String!
    var pdf_file_path       : String!
    var date                : String!
    var tdate               : Date?
    
    init(dict: NSDictionary) {
        id              = RawdataConverter.string(dict["id"])
        lng             = RawdataConverter.string(dict["language"])
        title           = RawdataConverter.string(dict["title"])
        cover_img       = RawdataConverter.string(dict["cover_img"])
        cover_img_path  = RawdataConverter.string(dict["cover_img_path"])
        pdf_file        = RawdataConverter.string(dict["pdf_file"])
        pdf_file_path   = RawdataConverter.string(dict["pdf_file_path"])
        date            = RawdataConverter.string(dict["date"])
        let dt = RawdataConverter.string(dict["date"])
        
        if dt != "00-00-0000" && !dt.isEmpty  {
            tdate = Date.dateFromFormatted1_String(dt)
        }
    }
    
}
