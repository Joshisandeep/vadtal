//
//  AudioAlbum.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 19/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import Foundation



// MARK:- AudioAlbum Class
class AudioAlbum: NSObject {
    
    var albam_code          : String!
    var albam_name          : String!
    var albam_desc          : String!
    var cover_img           : String!
    var cover_img_path      : String!
    var create_date         : String!
    
    var date                : Date?
    
    init(dict: NSDictionary) {
        
        albam_code = RawdataConverter.string(dict["albam_code"])
        albam_name = RawdataConverter.string(dict["albam_name"])
        albam_desc = RawdataConverter.string(dict["albam_desc"])
        cover_img = RawdataConverter.string(dict["cover_img"])
        cover_img_path = RawdataConverter.string(dict["cover_img_path"])
        let cdate = RawdataConverter.string(dict["create_date"])
        create_date = cdate
        
        if cdate != "00-00-0000" && !cdate.isEmpty  {
            date = Date.dateFromFormatted1_String(cdate)
        }
    }
    
}

// MARK:- AlbumSong Class
class AlbumSong: NSObject {
    
    var song_code           : String!
    var song_name           : String!
    var song_path           : String!
    var song_title          : String!
    var albam_name          : String!
    
    init(dict: NSDictionary) {
        
        song_code   = RawdataConverter.string(dict["song_code"])
        song_name   = RawdataConverter.string(dict["song_name"])
        song_path   = RawdataConverter.string(dict["song_path"])
        song_title  = RawdataConverter.string(dict["song_title"])
        albam_name  = RawdataConverter.string(dict["albam_name"])
    }
    
}
