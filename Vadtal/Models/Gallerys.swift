//
//  Gallerys.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 19/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import Foundation


// MARK:- PhotoYear Class
class PhotoYear: NSObject {
    
    var year          : String!
    var tot_page          : Int!
    var tot_row          : Int!
    
    init(dict: NSDictionary) {
        year        = RawdataConverter.string(dict["year"])
        tot_page    = RawdataConverter.integer(dict["tot_page"])
        tot_row     = RawdataConverter.integer(dict["tot_row"])
    }
    
}

// MARK:- PhotoYearEvent Class
class PhotoYearEvent: NSObject {
    
    var event_code           : String!
    var event_cover_img      : String!
    var tot_gallery          : Int!
    var event_name           : String!
    var event_desc           : String!
    var date                 : String!
    var year                 : String!
    var event_dt_code        : String!
    
    var tDate                : Date?
    
    init(dict: NSDictionary) {
        event_code          = RawdataConverter.string(dict["event_code"])
        event_cover_img     = RawdataConverter.string(dict["event_cover_img"])
        tot_gallery         = RawdataConverter.integer(dict["tot_gallery"])
        event_name          = RawdataConverter.string(dict["event_name"])
        event_desc          = RawdataConverter.string(dict["event_desc"])
        date                = RawdataConverter.string(dict["date"])
        year                = RawdataConverter.string(dict["year"])
        event_dt_code       = RawdataConverter.string(dict["event_dt_code"])
        
        let  dt             = RawdataConverter.string(dict["date"])
        if dt != "00-00-0000" && !dt.isEmpty  {
            tDate = Date.dateFromFormatted1_String(dt)
        }
        
    }
    
}

// MARK:- PhotoYearSubEvent Class
class PhotoYearSubEvent: NSObject {
    
    var event_dt_code           : String!
    var event_name              : String!
    var gallery_cover_img       : String!
    var gallery_name            : String!
    var date                    : String!
    var event_date              : String!
    
    var tDate                   : Date?
    var tEventDate              : Date?
    
    init(dict: NSDictionary) {
        event_dt_code          = RawdataConverter.string(dict["event_dt_code"])
        event_name     = RawdataConverter.string(dict["event_name"])
        gallery_cover_img         = RawdataConverter.string(dict["gallery_cover_img"])
        gallery_name          = RawdataConverter.string(dict["gallery_name"])
        date          = RawdataConverter.string(dict["date"])
        event_date                = RawdataConverter.string(dict["event_date"])
        
        let  dt             = RawdataConverter.string(dict["date"])
        if dt != "00-00-0000" && !dt.isEmpty  {
            tDate = Date.dateFromFormatted1_String(dt)
        }
        
        let  dEvent             = RawdataConverter.string(dict["event_date"])
        if dEvent != "00-00-0000" && !dEvent.isEmpty  {
            tEventDate = Date.dateFromFormatted1_String(dt)
        }
        
        
    }
    
}


// MARK:- EventPhoto Class
class EventPhoto: NSObject {
    
    var photopath           : String!
    
    init(dict: NSDictionary) {
        photopath   = RawdataConverter.string(dict["photopath"])
    }
    
}

// MARK:- VideoYear Class
class VideoYear: NSObject {
    
    var year          : String!
    
    init(dict: NSDictionary) {
        year        = RawdataConverter.string(dict["year"])
    }
    
}

// MARK:- VideoYearAlbum Class
class VideoYearAlbum: NSObject {
    
    var video_id           : String!
    var cover_img      : String!
    var video_title          : String!
    var tot_page           : Int!
    
    init(dict: NSDictionary) {
        video_id            = RawdataConverter.string(dict["video_id"])
        cover_img           = RawdataConverter.string(dict["cover_img"])
        video_title         = RawdataConverter.string(dict["video_title"])
        tot_page            = RawdataConverter.integer(dict["tot_page"])
    }
    
}


// MARK:- VideoAlbum Class
class VideoAlbum: NSObject {
    
    var video_code         : String!
    var video_desc         : String!
    var video_url          : String!
    var video_dt           : String!
    
    var tVideoDate                   : Date?
    
    init(dict: NSDictionary) {
        video_code          = RawdataConverter.string(dict["video_code"])
        video_desc     = RawdataConverter.string(dict["video_desc"])
        video_url         = RawdataConverter.string(dict["video_url"])
        video_dt          = RawdataConverter.string(dict["video_dt"])
        
        let  dt             = RawdataConverter.string(dict["video_dt"])
        if dt != "00-00-0000" && !dt.isEmpty  {
            tVideoDate = Date.dateFromFormatted1_String(dt)
        }
        
    }
    
}
