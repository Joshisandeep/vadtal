//
//  E-Magazine .swift
//  Vadtal
//
//  Created by Sandeep Joshi on 22/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import Foundation



// MARK:- MagazineYear Class
class MagazineYear: NSObject {
    
    var year                  : String!
    
    init(dict: NSDictionary) {
        year              = RawdataConverter.string(dict["year"])
    }
    
}
// MARK:- Magazine Class
class Magazine: NSObject {
    
    var magazine_code       : String!
    var magazine_name       : String!
    var year                : String!
    var month               : String!
    var cover_img           : String!
    var cover_img_path      : String!
    var file_path           : String!
    var file_path_path      : String!
    var date                : String!
    var tdate               : Date?
    
    init(dict: NSDictionary) {
        magazine_code       = RawdataConverter.string(dict["magazine_code"])
        magazine_name       = RawdataConverter.string(dict["magazine_name"])
        year                = RawdataConverter.string(dict["year"])
        month               = RawdataConverter.string(dict["month"])
        cover_img           = RawdataConverter.string(dict["cover_img"])
        cover_img_path      = RawdataConverter.string(dict["cover_img_path"])
        file_path           = RawdataConverter.string(dict["file_path"])
        file_path_path      = RawdataConverter.string(dict["file_path_path"])
        date                = RawdataConverter.string(dict["date"])
        let dt = RawdataConverter.string(dict["date"])
        
        if dt != "00-00-0000" && !dt.isEmpty  {
            tdate = Date.dateFromFormatted1_String(dt)
        }
    }
    
}
