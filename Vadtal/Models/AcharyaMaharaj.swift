//
//  AcharyaMaharaj .swift
//  Vadtal
//
//  Created by Sandeep Joshi on 25/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import Foundation


// MARK:- AcharyPhotoYear Class
class AcharyPhotoYear: NSObject {
    
    var year          : String!
    
    init(dict: NSDictionary) {
        year        = RawdataConverter.string(dict["year"])
    }
    
}

// MARK:- AcharyPhotoEvent Class
class AcharyPhotoEvent: NSObject {
    
    var event_code              : String!
    var event_name              : String!
    var event_desc              : String!
    var event_dt                : String!
    var year                    : String!
    var month                   : String!
    var gallery_cover_img       : String!
    var gallery_cover_img_path  : String!

    var tDate                : Date?
    
    init(dict: NSDictionary) {
        event_code          = RawdataConverter.string(dict["event_code"])
        event_name          = RawdataConverter.string(dict["event_name"])
        event_desc          = RawdataConverter.string(dict["event_desc"])
        event_dt            = RawdataConverter.string(dict["event_dt"])
        year                = RawdataConverter.string(dict["year"])
        month               = RawdataConverter.string(dict["month"])
        gallery_cover_img                = RawdataConverter.string(dict["gallery_cover_img"])
        gallery_cover_img_path       = RawdataConverter.string(dict["gallery_cover_img_path"])
        
        let  dt             = RawdataConverter.string(dict["event_dt"])
        if dt != "00-00-0000" && !dt.isEmpty  {
            tDate = Date.dateFromFormatted1_String(dt)
        }
        
    }
    
}


// MARK:- AcharyaEventPhoto Class
class AcharyaEventPhoto: NSObject {
    
    var photo_path           : String!
    var photo               : String!
    
    init(dict: NSDictionary) {
        photo       = RawdataConverter.string(dict["photo"])
        photo_path   = RawdataConverter.string(dict["photo_path"])
    }
    
}

// MARK:- AcharyaVideoYear Class
class AcharyaVideoYear: NSObject {
    
    var year          : String!
    
    init(dict: NSDictionary) {
        year        = RawdataConverter.string(dict["year"])
    }
    
}

// MARK:- AcharyaVideoYearAlbum     Class
class AcharyaVideoYearAlbum: NSObject {
    
    var video_code          : String!
    var video_name          : String!
    var video_desc          : String!
    var video_url           : String!
    var video_dt            : String!
    var year                : String!
    var month               : String!
    
    var tDate               : Date?

    init(dict: NSDictionary) {
        video_code            = RawdataConverter.string(dict["video_code"])
        video_name           = RawdataConverter.string(dict["video_name"])
        video_desc         = RawdataConverter.string(dict["video_desc"])
        video_url         = RawdataConverter.string(dict["video_url"])
        video_dt         = RawdataConverter.string(dict["video_dt"])
        year         = RawdataConverter.string(dict["year"])
        month        = RawdataConverter.string(dict["month"])
        let  dt             = RawdataConverter.string(dict["video_dt"])
        if dt != "00-00-0000" && !dt.isEmpty  {
            tDate = Date.dateFromFormatted1_String(dt)
        }
    }
    
}
