//
//  Nirnay.swift
//  Vadtal
//
//  Created by Sandeep Joshi on 19/10/16.
//  Copyright © 2016 Sandeep Joshi. All rights reserved.
//

import Foundation

/**
 *This Enum Represents relationship user selected language at any time in application. It is bound with
 @English:
 @Gujarti:
 */
enum AppLanguage: Int {
    case english = 0
    case gujarati = 1
    
    func getAppPrefix()->String{
        if self == .gujarati{
            return "guj"
        }else{
            return "eng"
        }
    }
    
    func getWebAPIPrefix(_ fName:Bool = true)->String{
        if self == .gujarati{
            return fName ? "Gujarati" : "Guj"
        }else{
            return fName ? "English" : "Eng"
        }
    }
    
    
   
    
    
}


// MARK: Paging Structure
struct LoadMoreSpecific {
    var loadingPage: Int = 1
    
    var totalRecord: Int = 0
    var isLoading: Bool = false
    var page: String {
        get {
            return "\(loadingPage)"
        }
        
    }
    
}

enum SpecialDay: Int {
    case ekadasi = 0
    case poonam  = 1
    case other   = 2
    
    func getSpecialDayType(_ type:String) ->SpecialDay{
        switch type {
        case "Ekadasi","એકાદશી":
            return .ekadasi
        case "Poonam","પૂનમ":
            return .poonam
        default:
            return .other
            
        }
    }
   
}


// MARK:- Nirnay Class
class Nirnay: NSObject {
    
    var lng             : AppLanguage!
    var fulldate        : String!
    var month           : String!
    var year            : String!
    var day             : String!
    var day_word        : String!
    var tithi           : String!
    var naksatra        : String!
    var chandra         : String!
    var txt_detail      : String!
    var special_day     : SpecialDay!
    var savant          : String!
    var sane            : String!
    
    var date            : Date?
    
    
    init(dict: NSDictionary) {
        lng = (RawdataConverter.string(dict["language"]) == "Gujarati") ? .gujarati : AppLanguage.english
        
        let fdate = RawdataConverter.string(dict["fulldate"]) //"01-09-2016"
        fulldate = fdate
        month = RawdataConverter.string(dict["month"])
        year = RawdataConverter.string(dict["year"])
        day = RawdataConverter.string(dict["day"])
        day_word = RawdataConverter.string(dict["day_word"])
        tithi = RawdataConverter.string(dict["tithi"])
        naksatra = RawdataConverter.string(dict["naksatra"])
        chandra = RawdataConverter.string(dict["chandra"])
        txt_detail = RawdataConverter.string(dict["txt_detail"])
        special_day = SpecialDay.other.getSpecialDayType(RawdataConverter.string(dict["special_day"]))
        savant = RawdataConverter.string(dict["savant"])
        sane = RawdataConverter.string(dict["sane"])
        
        if fdate != "00-00-0000" && !fdate.isEmpty  {
            date = Date.dateFromFormatted1_String(fdate)
        }
    }
    
    
    func getDisplayTitle() -> String{
        var strTitle = ""
        if !fulldate.isEmpty{
            strTitle = "Date: " + fulldate
            if !day_word.isEmpty{
                strTitle += "," + day_word
            }
        }
//        if !tithi.isEmpty{
            if !strTitle.isEmpty{
                strTitle += " | "
            }
            strTitle += "Tithi - " + tithi
//        }
//        if !chandra.isEmpty{
            if !strTitle.isEmpty{
                strTitle += " | "
            }
            strTitle += "Chandra - " + chandra
//        }
//        if !naksatra.isEmpty{
            if !strTitle.isEmpty{
                strTitle += " | "
            }
            strTitle += "Naksatra - " + naksatra
//        }
        return strTitle
    }
    
  
    func getTithi() -> String{
        var strTitle = ""
//        if !tithi.isEmpty{
            strTitle = "Tithi - " + tithi
//        }
        return strTitle
    }
}
