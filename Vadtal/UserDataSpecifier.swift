//
//  UserDataSpecifier.swift
//  AussieFood
//
//  Created by Yudiz Solutions Pvt.Ltd. on 06/09/16.
//  Copyright © 2016 Yudiz Solutions Pvt.Ltd. All rights reserved.
//

import Foundation

// MARK:- PassValidation Enum

enum PassValidation {
    case valid
    case small
    case big
    case empty
    case notMatch
    case notSame
}

// MARK:- PresentationStyle  Enum
enum PresentationStyle {
    case normal
    case active
    case error
}

// MARK:- FormType  Enum
enum FormType {
    case feedback
    case contactUs
    case rating
}

// MARK:- UserField Structure
struct UserField {
    var image : String!
    var placeHolder: String!
    var keyName: String!
    var str : String = ""
    var title : String = ""
    var preStype = PresentationStyle.normal
    var selected : Bool = false
}

// MARK:- UserDataSpecifier Class
class UserDataSpecifier: NSObject {
    
    var allFileds: [UserField] = []
    var formType = FormType.feedback
    
    // MARK:- initialize
    
    init(type: FormType,isAccpted:Bool = false) {
        super.init()
        formType = type
        switch type {
        case .feedback:
            self.prepareDataForFeedback()
            break
            
        default:
            break
        }
    }
    
    
    // MARK:- Feedback
    
    func prepareDataForFeedback() {
        
        var tf1 = UserField()
        tf1.image       = ""
        tf1.placeHolder = "Name"
        tf1.keyName     = "name"
        
        var tf2 = UserField()
        tf2.image       = ""
        tf2.placeHolder = "CONTACT NO."
        tf2.keyName     = "mobile"
        
        var tf3 = UserField()
        tf3.image       = ""
        tf3.placeHolder = "EMAIL"
        tf3.keyName     = "email"

        
        var tf4 = UserField()
        tf4.image       = ""
        tf4.placeHolder = "MESSAGE"
        tf4.keyName     = "feedback"

        allFileds.append(tf1)
        allFileds.append(tf2)
        allFileds.append(tf3)
        allFileds.append(tf4)
    }
    

    // MARK:- Param Dictionary
    
    func paramDictionary() -> [String: AnyObject]{
        var dict = [String: AnyObject]()
        for obj in allFileds {
            dict[obj.keyName] = obj.str.trimmedString() as AnyObject?
        }
        return dict //as NSDictionary as! [String: AnyObject]
    }
    
    
    // MARK:- Validate
    
    func isValidData() -> (valid: Bool, error: String) {
        
        var result = (valid: true, error: "")
        switch formType {
  
        case .feedback:
            if String.isValidString(allFileds[0].str){
                result.valid = false
                result.error = kEnterName
                return result
            }
            if  !allFileds[1].str.isValidContact(){
                result.valid = false
                result.error = kValidMobile
                return result
            }
            if String.isValidString(allFileds[2].str) {
                result.valid = false
                result.error = kEnterEmail
                
                return result
            } else if !allFileds[2].str.isValidEmailAddress() {
                result.valid = false
                result.error = kValidEmail
                
                return result
            }
            if String.isValidString(allFileds[3].str){
                result.valid = false
                result.error = kEnterMessage
                return result
            }
            
            break
           
        default:
            
            break
        }
        return result
    }
    
}
